<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Admin\Traits\Auditorias;

use App\Menu;
use App\MenuRol;

class Menu extends Model
{
    use Auditorias;
    protected $table = 't_menus';
    //protected $dateFormat = 'Y-d-m H:i:s';

    protected $fillable =[
        'id_padre',
        'titulo',
        'icono',
        'link',
        'descripcion',
        'orden',
        'estado'                
    ];

    public static function menuxRol($rol){
        $menus = Menu::select(['id','id_padre','titulo','icono','link','orden'])->where("estado",1);       
        //$usuarios = Usuario::with('persona');       
        $menus->whereHas('xRol', function ($query) use ($rol) {
            $query->where('id_rol',$rol);
        });
        return $menus->get();    
       
    }

    public function padre()
    {
        return $this->belongsTo('App\Menu', 'id_padre');
    }

    public function hijos()
    {
        return $this->hasMany('App\Menu', 'id_padre','id');
    }

    public function xRol()
    {
        return $this->hasMany('App\MenuRol', 'id_menu');
    }
   
    

    

}
