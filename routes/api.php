<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login','AuthController@authenticate');
    Route::post('logout','AuthController@logout');
    Route::post('check','AuthController@check');
    Route::post('register','AuthController@register');
    Route::get('activate/{token}','AuthController@activate');
    Route::post('password','AuthController@password');
    Route::post('validate-password-reset','AuthController@validatePasswordReset');
    Route::post('reset','AuthController@reset');
    Route::post('social/token','SocialAuthController@getToken');
    Route::post('registrar_free','AuthController@registrarCuentaFree');
});

Route::group(['prefix' => 'consulta'], function () {
  Route::post('info_cpe','ConsultaController@buscarCPE');

});

Route::group(['middleware' => ['jwt.auth']], function () {
  Route::get('auth/user','AuthController@getAuthUser');
  Route::post('task','TaskController@store');
  Route::get('task','TaskController@index');
  Route::delete('task/{id}','TaskController@destroy');
  Route::get('task/{id}','TaskController@show');
  Route::patch('task/{id}','TaskController@update');
  Route::post('task/status','TaskController@toggleStatus');

    Route::get('/configuration/fetch','ConfigurationController@index');
    Route::post('/configuration','ConfigurationController@store');

    Route::get('user','UserController@index');
    Route::post('user/change-password','AuthController@changePassword');
    Route::post('user/update-profile','UserController@updateProfile');
    Route::post('user/update-avatar','UserController@updateAvatar');
    Route::post('user/remove-avatar','UserController@removeAvatar');
    Route::delete('user/{id}','UserController@destroy');
    Route::get('user/dashboard','UserController@dashboard');

    Route::post('todo','TodoController@store');
    Route::get('/todo','TodoController@index');
    Route::delete('/todo/{id}','TodoController@destroy');
    Route::post('/todo/status','TodoController@toggleStatus');


   
    //Catalogos Select //select
    Route::group(['prefix' => 'select'], function () {      
      Route::get('sedes_regionales','SelectGeneralController@llenarSedesRegionales');  
      Route::get('sedes_provinciales/{id_reg}','SelectGeneralController@llenarSedesProvinciales');  
      Route::get('sedes_distritales/{id_reg}/{id_prov}','SelectGeneralController@llenarSedesDistritales');  
      Route::get('documentos','SelectGeneralController@llenarDocumentos');  
      Route::get('cargos','SelectGeneralController@llenarCargos');  
   
      
    });  
    
    //personas

    Route::group(['prefix' => 'persona'], function () { 
      Route::post('registrar','PersonaController@crear');
      Route::put('inactivar/{id}','PersonaController@inactivar');  
      Route::put('activar/{id}','PersonaController@activar');    
      Route::put('editar/{id}','PersonaController@modificar');
      Route::get('listar','PersonaController@listar');   
      Route::get('buscar_personas/{descripcion}','PersonaController@llenarSelectMultiple');  
    
      
    });  

    //documentos

    Route::group(['prefix' => 'cant_documentos'], function () {  
      Route::post('guardar','CantidadDocumentosController@guardar');
      Route::put('actualizar/{id}','CantidadDocumentosController@actualizar');
      Route::get('obtener','CantidadDocumentosController@obtener'); 
      //reportes
      Route::get('avance_nacional','CantidadDocumentosController@reporteAvanceNacional'); 
      Route::get('avance_documento','CantidadDocumentosController@reporteAvanceDocumento'); 
      Route::get('avance_documento_sede_regional','CantidadDocumentosController@reporteAvanceDocumentoSedeRegional');
      Route::get('avance_documento_sede_provincial','CantidadDocumentosController@reporteAvanceDocumentoSedeProvincialDistrital');
      Route::get('avance_sede','CantidadDocumentosController@reporteAvance'); 
      Route::get('avance_productiviadad','CantidadDocumentosController@reporteProductividad'); 
      Route::get('avance_diario','CantidadDocumentosController@reporteDiario'); 
      Route::get('avance_ranking_persona','CantidadDocumentosController@reporteRankingPersona'); 
       
      

      
      
      
    });  


   
    //usuarios


       
    
   

    //administrador


     //Menu
     Route::group(['prefix' => 'menu'], function () {
      Route::get('listar','MenuController@listar');
      Route::get('select','MenuController@llenarSelect'); 
      Route::post('registrar','MenuController@crear');
      Route::put('inactivar/{id}','MenuController@inactivar');
      Route::put('activar/{id}','MenuController@activar');   
      Route::put('editar/{id}','MenuController@modificar');
      Route::get('mostrar_sidebar','MenuController@mostrarSideBar');     
      Route::get('menu_x_rol/{id}','MenuController@MenuRol'); 
      
       
    });

    //Roles
    Route::group(['prefix' => 'rol'], function () {
      Route::get('mostrar','RolController@mostrar');
      Route::get('listar','RolController@listar');
      Route::post('registrar','RolController@crear');
      Route::put('inactivar/{id}','RolController@inactivar');  
      Route::put('activar/{id}','RolController@activar');    
      Route::put('editar/{id}','RolController@modificar');  
      Route::get('select','RolController@llenarSelect');
      Route::put('registrar_menus/{id}','RolController@registrarMenuRol');
    }); 

    //usuarios
    Route::group(['prefix' => 'usuario'], function () {
      Route::get('mostrar','UsuarioController@mostrar');
      Route::get('listar','UsuarioController@listar');
      Route::post('registrar','UsuarioController@crear');
      Route::put('inactivar/{id}','UsuarioController@inactivar');  
      Route::put('activar/{id}','UsuarioController@activar');    
      Route::put('editar/{id}','UsuarioController@modificar');  
    });
    

});
