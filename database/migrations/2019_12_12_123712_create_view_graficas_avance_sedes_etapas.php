<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewGraficasAvanceSedesEtapas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
            CREATE VIEW grafica_avance_sedes_operativo1_etapa1 AS 
            SELECT row_number() over (order by A.id) AS row_num, 
                cod_sede_regional, sede_regional, operativo, operativo_nombre, 
                p0_01_marco, 
                        p1_01, p1_01_avance, p1_02, p1_02_avance 
                        FROM (
                SELECT  t1.id,
                        t1.cod_sede_regional,
                t1.sede_regional,
                t0.operativo_marco as operativo,
                CASE 
                WHEN t0.operativo_marco=1 THEN 'ECE' 
                WHEN t0.operativo_marco=2 THEN 'EM' 
                ELSE NULL 
                END
                AS operativo_nombre,
                SUM(IFNULL(t0.p0_01_tot,0)) AS p0_01_marco,
                SUM(IFNULL(t2.p1_01,0)) AS p1_01,
                FORMAT(if(IFNULL(SUM(t2.p1_01),0)=0,0,(SUM(t2.p1_01)/SUM(t0.p0_01_tot)))*100.0,2) AS p1_01_avance,
                SUM(IFNULL(t2.p1_02,0)) AS p1_02,
                        FORMAT(if(IFNULL(SUM(t2.p1_02),0)=0,0,(SUM(t2.p1_02)/SUM(t0.p0_01_tot)))*100.0,2) AS p1_02_avance
                from t_cod_sede t1 
                left join resum_marco t0 on t1.id=t0.sede_id 
                left join resum_sede_operativo_etapa1 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
                        WHERE t0.operativo_marco = 1 
                        GROUP BY t1.cod_sede_regional, t0.operativo_marco order by id) A;

            CREATE VIEW grafica_avance_sedes_operativo1_etapa2 AS 
            SELECT row_number() over (order by A.id) AS row_num, 
                cod_sede_regional, sede_regional, operativo, operativo_nombre, 
                p0_01_marco, 
                        p2_01, p2_01_avance, p2_02, p2_02_avance 
                        FROM (
                SELECT  t1.id,
                        t1.cod_sede_regional,
                t1.sede_regional,
                t0.operativo_marco as operativo,
                CASE 
                WHEN t0.operativo_marco=1 THEN 'ECE' 
                WHEN t0.operativo_marco=2 THEN 'EM' 
                ELSE NULL 
                END
                AS operativo_nombre,
                SUM(IFNULL(t0.p0_01_tot,0)) AS p0_01_marco,
                SUM(IFNULL(t2.p2_01,0)) AS p2_01,
                FORMAT(if(IFNULL(SUM(t2.p2_01),0)=0,0,(SUM(t2.p2_01)/SUM(t0.p0_01_tot)))*100.0,2) AS p2_01_avance,
                SUM(IFNULL(t2.p2_02,0)) AS p2_02,
                        FORMAT(if(IFNULL(SUM(t2.p2_02),0)=0,0,(SUM(t2.p2_02)/SUM(t0.p0_01_tot)))*100.0,2) AS p2_02_avance
                from t_cod_sede t1 
                left join resum_marco t0 on t1.id=t0.sede_id 
                left join resum_sede_operativo_etapa2 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
                        WHERE t0.operativo_marco = 1 
                        GROUP BY t1.cod_sede_regional, t0.operativo_marco order by id) A;

            CREATE VIEW grafica_avance_sedes_operativo1_etapa3 AS 
            SELECT row_number() over (order by A.id) AS row_num, 
                cod_sede_regional, sede_regional, operativo, operativo_nombre, 
                p0_01_marco, 
                        p3_01, p3_01_avance, p3_02, p3_02_avance 
                        FROM (
                SELECT  t1.id,
                        t1.cod_sede_regional,
                t1.sede_regional,
                t0.operativo_marco as operativo,
                CASE 
                WHEN t0.operativo_marco=1 THEN 'ECE' 
                WHEN t0.operativo_marco=2 THEN 'EM' 
                ELSE NULL 
                END
                AS operativo_nombre,
                SUM(IFNULL(t0.p0_01_tot,0)) AS p0_01_marco,
                SUM(IFNULL(t2.p3_01,0)) AS p3_01,
                FORMAT(if(IFNULL(SUM(t2.p3_01),0)=0,0,(SUM(t2.p3_01)/SUM(t0.p0_01_tot)))*100.0,2) AS p3_01_avance,
                SUM(IFNULL(t2.p3_02,0)) AS p3_02,
                        FORMAT(if(IFNULL(SUM(t2.p3_02),0)=0,0,(SUM(t2.p3_02)/SUM(t0.p0_01_tot)))*100.0,2) AS p3_02_avance
                from t_cod_sede t1 
                left join resum_marco t0 on t1.id=t0.sede_id 
                left join resum_sede_operativo_etapa3 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
                        WHERE t0.operativo_marco = 1 
                        GROUP BY t1.cod_sede_regional, t0.operativo_marco order by id) A;
                        
            CREATE VIEW grafica_avance_sedes_operativo1_etapa4 AS 
            SELECT row_number() over (order by A.id) AS row_num, 
                cod_sede_regional, sede_regional, operativo, operativo_nombre, 
                p0_01_marco, 
                        p4_01, p4_01_avance, p4_02, p4_02_avance 
                        FROM (
                SELECT  t1.id,
                        t1.cod_sede_regional,
                t1.sede_regional,
                t0.operativo_marco as operativo,
                CASE 
                WHEN t0.operativo_marco=1 THEN 'ECE' 
                WHEN t0.operativo_marco=2 THEN 'EM' 
                ELSE NULL 
                END
                AS operativo_nombre,
                SUM(IFNULL(t0.p0_01_tot,0)) AS p0_01_marco,
                SUM(IFNULL(t2.p4_01,0)) AS p4_01,
                FORMAT(if(IFNULL(SUM(t2.p4_01),0)=0,0,(SUM(t2.p4_01)/SUM(t0.p0_01_tot)))*100.0,2) AS p4_01_avance,
                SUM(IFNULL(t2.p4_02,0)) AS p4_02,
                        FORMAT(if(IFNULL(SUM(t2.p4_02),0)=0,0,(SUM(t2.p4_02)/SUM(t0.p0_01_tot)))*100.0,2) AS p4_02_avance
                from t_cod_sede t1 
                left join resum_marco t0 on t1.id=t0.sede_id 
                left join resum_sede_operativo_etapa4 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
                        WHERE t0.operativo_marco = 1 
                        GROUP BY t1.cod_sede_regional, t0.operativo_marco order by id) A;

            CREATE VIEW grafica_avance_sedes_operativo2_etapa1 AS 
            SELECT row_number() over (order by A.id) AS row_num, 
                cod_sede_regional, sede_regional, operativo, operativo_nombre, 
                p0_01_marco, 
                        p1_01, p1_01_avance, p1_02, p1_02_avance 
                        FROM (
                SELECT  t1.id,
                        t1.cod_sede_regional,
                t1.sede_regional,
                t0.operativo_marco as operativo,
                CASE 
                WHEN t0.operativo_marco=1 THEN 'ECE' 
                WHEN t0.operativo_marco=2 THEN 'EM' 
                ELSE NULL 
                END
                AS operativo_nombre,
                SUM(IFNULL(t0.p0_01_tot,0)) AS p0_01_marco,
                SUM(IFNULL(t2.p1_01,0)) AS p1_01,
                FORMAT(if(IFNULL(SUM(t2.p1_01),0)=0,0,(SUM(t2.p1_01)/SUM(t0.p0_01_tot)))*100.0,2) AS p1_01_avance,
                SUM(IFNULL(t2.p1_02,0)) AS p1_02,
                        FORMAT(if(IFNULL(SUM(t2.p1_02),0)=0,0,(SUM(t2.p1_02)/SUM(t0.p0_01_tot)))*100.0,2) AS p1_02_avance
                from t_cod_sede t1 
                left join resum_marco t0 on t1.id=t0.sede_id 
                left join resum_sede_operativo_etapa1 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
                        WHERE t0.operativo_marco = 2 
                        GROUP BY t1.cod_sede_regional, t0.operativo_marco order by id) A;

            CREATE VIEW grafica_avance_sedes_operativo2_etapa2 AS 
            SELECT row_number() over (order by A.id) AS row_num, 
                cod_sede_regional, sede_regional, operativo, operativo_nombre, 
                p0_01_marco, 
                        p2_01, p2_01_avance, p2_02, p2_02_avance 
                        FROM (
                SELECT  t1.id,
                        t1.cod_sede_regional,
                t1.sede_regional,
                t0.operativo_marco as operativo,
                CASE 
                WHEN t0.operativo_marco=1 THEN 'ECE' 
                WHEN t0.operativo_marco=2 THEN 'EM' 
                ELSE NULL 
                END
                AS operativo_nombre,
                SUM(IFNULL(t0.p0_01_tot,0)) AS p0_01_marco,
                SUM(IFNULL(t2.p2_01,0)) AS p2_01,
                FORMAT(if(IFNULL(SUM(t2.p2_01),0)=0,0,(SUM(t2.p2_01)/SUM(t0.p0_01_tot)))*100.0,2) AS p2_01_avance,
                SUM(IFNULL(t2.p2_02,0)) AS p2_02,
                        FORMAT(if(IFNULL(SUM(t2.p2_02),0)=0,0,(SUM(t2.p2_02)/SUM(t0.p0_01_tot)))*100.0,2) AS p2_02_avance
                from t_cod_sede t1 
                left join resum_marco t0 on t1.id=t0.sede_id 
                left join resum_sede_operativo_etapa2 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
                        WHERE t0.operativo_marco = 2 
                        GROUP BY t1.cod_sede_regional, t0.operativo_marco order by id) A;


            CREATE VIEW grafica_avance_sedes_operativo2_etapa3 AS 
            SELECT row_number() over (order by A.id) AS row_num, 
                cod_sede_regional, sede_regional, operativo, operativo_nombre, 
                p0_01_marco, 
                        p3_01, p3_01_avance, p3_02, p3_02_avance 
                        FROM (
                SELECT  t1.id,
                        t1.cod_sede_regional,
                t1.sede_regional,
                t0.operativo_marco as operativo,
                CASE 
                WHEN t0.operativo_marco=1 THEN 'ECE' 
                WHEN t0.operativo_marco=2 THEN 'EM' 
                ELSE NULL 
                END
                AS operativo_nombre,
                SUM(IFNULL(t0.p0_01_tot,0)) AS p0_01_marco,
                SUM(IFNULL(t2.p3_01,0)) AS p3_01,
                FORMAT(if(IFNULL(SUM(t2.p3_01),0)=0,0,(SUM(t2.p3_01)/SUM(t0.p0_01_tot)))*100.0,2) AS p3_01_avance,
                SUM(IFNULL(t2.p3_02,0)) AS p3_02,
                        FORMAT(if(IFNULL(SUM(t2.p3_02),0)=0,0,(SUM(t2.p3_02)/SUM(t0.p0_01_tot)))*100.0,2) AS p3_02_avance
                from t_cod_sede t1 
                left join resum_marco t0 on t1.id=t0.sede_id 
                left join resum_sede_operativo_etapa3 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
                        WHERE t0.operativo_marco = 2 
                        GROUP BY t1.cod_sede_regional, t0.operativo_marco order by id) A;
                        
            CREATE VIEW grafica_avance_sedes_operativo2_etapa4 AS 
            SELECT row_number() over (order by A.id) AS row_num, 
                cod_sede_regional, sede_regional, operativo, operativo_nombre, 
                p0_01_marco, 
                        p4_01, p4_01_avance, p4_02, p4_02_avance 
                        FROM (
                SELECT  t1.id,
                        t1.cod_sede_regional,
                t1.sede_regional,
                t0.operativo_marco as operativo,
                CASE 
                WHEN t0.operativo_marco=1 THEN 'ECE' 
                WHEN t0.operativo_marco=2 THEN 'EM' 
                ELSE NULL 
                END
                AS operativo_nombre,
                SUM(IFNULL(t0.p0_01_tot,0)) AS p0_01_marco,
                SUM(IFNULL(t2.p4_01,0)) AS p4_01,
                FORMAT(if(IFNULL(SUM(t2.p4_01),0)=0,0,(SUM(t2.p4_01)/SUM(t0.p0_01_tot)))*100.0,2) AS p4_01_avance,
                SUM(IFNULL(t2.p4_02,0)) AS p4_02,
                        FORMAT(if(IFNULL(SUM(t2.p4_02),0)=0,0,(SUM(t2.p4_02)/SUM(t0.p0_01_tot)))*100.0,2) AS p4_02_avance
                from t_cod_sede t1 
                left join resum_marco t0 on t1.id=t0.sede_id 
                left join resum_sede_operativo_etapa4 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
                        WHERE t0.operativo_marco = 2 
                        GROUP BY t1.cod_sede_regional, t0.operativo_marco order by id) A;
            ");
        }else{
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("
        DROP VIEW grafica_avance_sedes_operativo2_etapa4;
        DROP VIEW grafica_avance_sedes_operativo2_etapa3;
        DROP VIEW grafica_avance_sedes_operativo2_etapa2;
        DROP VIEW grafica_avance_sedes_operativo2_etapa1;
        DROP VIEW grafica_avance_sedes_operativo1_etapa4;
        DROP VIEW grafica_avance_sedes_operativo1_etapa3;
        DROP VIEW grafica_avance_sedes_operativo1_etapa2;
        DROP VIEW grafica_avance_sedes_operativo1_etapa1;
        ");
    }
}
