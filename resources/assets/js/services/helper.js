import CryptoJS from "crypto-js";

var key = 'Lu15.v1ll3g@$.Hu4t@d0';
export default {
    logout(){
        return axios.post('api/auth/logout').then(response =>  {
            localStorage.removeItem('auth_token');
            axios.defaults.headers.common['Authorization'] = null;
            toastr['success'](response.data.message);
        }).catch(error => {
            console.log(error);
        });
    },

    authUser(){
        return axios.get('api/auth/user').then(response =>  {            
            return response.data;
        }).catch(error => {
            return error.response.data;
        });
    },

    check(){
        return axios.post('api/auth/check').then(response =>  {
            return !!response.data.authenticated;
        }).catch(error =>{
            return response.data.authenticated;
        });
    },

    getFilterURL(data){
        let url = '';
        $.each(data, function(key,value) {
            url += (value) ? '&'+key+'='+encodeURI(value) : '';
        });
        return url;
    },

    formAssign(form, data){
        for (let key of Object.keys(form)) {
            if(key !== "originalData" && key !== "errors" && key !== "autoReset"){
                form[key] = data[key];
            }
        }
        return form;
    },   

    taskColor(value){
        let classes = ['progress-bar','progress-bar-striped'];
        if(value < 20)
            classes.push('bg-danger');
        else if(value < 50)
            classes.push('bg-warning');
        else if(value < 80)
            classes.push('bg-info');
        else
            classes.push('bg-success');
        return classes;
    },

    formatDates(date){
        if(!date)
            return;

        return moment(date).format('MMMM Do YYYY');
    },

    formatDateTime(date){
        if(!date)
            return;

        return moment(date).format('MMMM Do YYYY h:mm a');
    },

    formatDate(date){
        if(!date)
            return;
        
        return moment(date).format('YYYY-MM-DD');
    },
    formatTime(time){ 
        if(time)     
            return time.slice(0,8);
    },
    ucword(value){
        if(!value)
            return;

        return value.toLowerCase().replace(/\b[a-z]/g, function(value) {
            return value.toUpperCase();
        });
    },

    soloMayusculas($event,_this,objeto,prop) {        
        const start = $event.target.selectionStart;
        $event.target.value = $event.target.value.toUpperCase();
        _this.$set(objeto,prop, $event.target.value);
        $event.target.setSelectionRange(start, start);
    },

    soloDecimalDos($event,_this,objeto,prop) {        
        const start = $event.target.selectionStart;
        //console.log($event.target.value);
        $event.target.value = $event.target.value;
        _this.$set(objeto,prop, Number($event.target.value).toFixed(2));
        $event.target.setSelectionRange(start, start);
    }, 

    encrypt(valor){            
        return (CryptoJS.AES.encrypt(valor.toString(), key)).toString();   
    },

    decrypt(valor){     
        var bytes  = CryptoJS.AES.decrypt(valor,key);
        return bytes.toString(CryptoJS.enc.Utf8);         
    },

    soloNumeros($event){       
        let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
        if ((keyCode < 48 || keyCode > 57)) { // 46 is dot
            $event.preventDefault();            
        }
    },
    soloDecimales($event,numero){       
        let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
         

        if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
            $event.preventDefault();            
        }      

        let numDec = (''+numero).split("."); 
       
        if(numDec.length > 1){
            if ( keyCode == 46) { // 46 is dot
                $event.preventDefault();            
            } 
            if(numDec[1].toString().length >=2){
                $event.preventDefault(); 
            }          
        }       
        
        if(numDec[0].toString().length >=18){
            $event.preventDefault(); 
        } 
        
    },
    soloNumerosCantidad($event){       
        let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
        if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
            $event.preventDefault();            
        }
    },
    soloNumerosPrecio($event){   
            
        let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
        var p = new RegExp(/^(\d{1}\.)?(\d+\.?)+(,\d{2})?$/);
        
        var regex = new RegExp(/^\d{1,2}(\.\d{1,2})?$/); ;
        if (!p.test(keyCode))
            $event.preventDefault(); 
              
    },
     compararFechas(fecha_min,fecha_max){        
        if(new Date(fecha_min).getTime() > new Date(fecha_max).getTime()){
            return true;
        }   
        return false;     
     },
     deshabilitarBoton(_this){
        _this.desabilitado = true;
       
    },
    habilitarBoton(_this){
        setTimeout(function(){ _this.desabilitado = false;}, 1500);
    },
    
}
