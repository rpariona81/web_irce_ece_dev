import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);
import createPersistedState from 'vuex-persistedstate'
import * as Cookies from 'js-cookie'

const store = new Vuex.Store({
	state: {
		auth: {
			rol:'',
			email: '',
			avatar: '',
			nombres:'',
			cargo:'',
		},	
	},
	mutations: {
		setAuthUserDetail (state, auth) {
        	for (let key of Object.keys(auth)) {
                state.auth[key] = auth[key];
            }
            if ('avatar' in auth)
            	state.auth.avatar = auth.avatar !== null ? auth.avatar : 'avatar.png';
		},
		setEmpresaDetail (state, empresa) {
        	for (let key of Object.keys(empresa)) {
                state.empresa[key] = empresa[key];
            }           
		},
		setProfileDetail (state, profile) {
        	for (let key of Object.keys(profile)) {
                state.profile[key] = profile[key];
            }           
		},
		resetAuthUserDetail (state) {
        	for (let key of Object.keys(state.auth)) {
                state.auth[key] = '';
            }
		},
		setConfig (state, config) {
        	for (let key of Object.keys(config)) {
                state.config[key] = config[key];
            }
		}
	},
	actions: {
		setAuthUserDetail ({ commit }, auth) {
     		commit('setAuthUserDetail',auth);
		 },
		setEmpresaDetail ({ commit }, empresa) {
			commit('setEmpresaDetail',empresa);
		},
		setProfileDetail ({ commit }, profile) {
			commit('setProfileDetail',profile);
		},
     	resetAuthUserDetail ({commit}){
     		commit('resetAuthUserDetail');
     	},
		setConfig ({ commit }, data) {
     		commit('setConfig',data);
     	}
	},
	getters: {
		getAuthUser: (state) => (name) => {			
		    return state.auth[name];
		},		
		getAuthUserFullName: (state) => {
			//return state.auth['nombres'];
			
		},
		getUser: (state) => {	
		    return state.profile;
		},
		getConfig: (state) => (name) => {
		    return state.config[name];
		},
		getRol: (state) => {	
		    return state.auth['rol'];
		},		
		getEmpresa: (state) => {
		    return state.empresa['ruc'] +' - '+ state.empresa['rz'];
		},
		getEmpresaFull: (state) => {
		    return state.empresa;
		},
		getTipoPrecio: (state) => {
		    return state.empresa['precio_con_igv'];
		},	
	},
	plugins: [
		createPersistedState({ storage: window.sessionStorage })
	]
});

export default store;