<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 't_roles';

    protected $fillable =[
        'nombre',
        'descripcion',
        'estado',
        'created_by',
        'updated_by',
        'deleted_at',
        'deleted_by',
    ];

    public function usuarios(){
        return $this->hasMany('App\Usuario','id_rol');
    }
}
