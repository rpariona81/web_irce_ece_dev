<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servidor;

class ExtraController extends Controller
{
    public function imprimirPDF($identificador,$codigo){
        $base = Servidor::find($identificador);
        return view('impresion',array('base'=>$base->dominio,'codigo'=>$codigo));
    }
}
