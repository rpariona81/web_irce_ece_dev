<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CantidadDocumentosDetalle extends Model
{
    protected $table = 't_det_dig_documento';
    //protected $dateFormat = 'Y-d-m H:i:s';

    protected $fillable =[
        'cabecera_id',     
        'documento_id',     
        'p_01',     
        'p_02',     
        'p_obs',

    ];

    
}
