<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Rol;
use App\MenuRol;

class RolController extends Controller
{
    //llenar un combo con los roles acivos
    public function llenarSelect(){
        $rol = Rol::select(['id','nombre'])->where('estado',1)->get();
        return $rol;
    }


    //funciones generales de mantenimiento
    public function listar(){
        //return request();
        $rol = Rol::select(['id','nombre','descripcion','estado','created_at']);

		if(request()->has('nombre'))
            $rol->where('nombre','like','%'.request('nombre').'%');
        if(request()->has('descripcion'))
			$rol->where('descripcion','like','%'.request('descripcion').'%');

        if(request()->has('estado'))
            $rol->where('estado',request('estado'));

        $rol->orderBy(request('ordenarPor'),request('order'));

        return $rol->paginate(request('pageLength'));
    }

    public function crear(Request $request){
        $nuevo = new Rol($request->all());        
        $nuevo->save();
        return response()->json(['message' => 'El Rol se creó correctamente']);
    }

    public function modificar(Request $request,$id){
        $editado = Rol::findOrFail($request->id);        
        $editado->update($request->all()); 
        return response()->json(['message' => 'El Rol se modificó correctamente']);
    }

    public function eliminar(){
        $eliminado = Rol::findOrFail($request->id);
        $eliminado->estado = 9 ;
        $eliminado->save(); 
    }

    public function inactivar($id){
        $inactivado = Rol::findOrFail($id);
        $inactivado->estado = 2 ;
        $inactivado->save(); 
        return response()->json(['message' => 'El Rol se inactivó correctamente']);
    }

    public function activar($id){
        $activado = Rol::findOrFail($id);
        $activado->estado = 1;
        $activado->save(); 
        return response()->json(['message' => 'El Rol se activó correctamente']);
    }
    //fin funciones generales de mantenimiento

    //menu rol

    public function registrarMenuRol(Request $request,$id){        
        MenuRol::where('id_rol',$id)->delete();
        $menus_x_rol = $request->menus;
        
        foreach($menus_x_rol as $menus){
            $menu_rol = new MenuRol;
            $menu_rol->id_rol = $id;
            $menu_rol->id_menu = $menus['id'];
            $menu_rol->save();
        }
        
        return response()->json(['message' => 'Se asignó correctamente los menus al Rol']);
    }



     //web Service

     public function mostrar(){
        $roles = Rol::select(['id','nombre','descripcion','created_at'])->get();
        return response()->json(['roles' => $roles]);        
    }
}
