<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sedes;
use App\Documentos;
use App\Cargo;


class SelectGeneralController extends Controller
{
    public function llenarSedesRegionales(){        
        $lista = Sedes::select('cod_sede_regional','sede_regional')->distinct()->get();
        return $lista;
    }

    public function llenarSedesProvinciales($id_regional){        
        $lista = Sedes::select('cod_sede_provincial','sede_provincial')->where('cod_sede_regional',$id_regional)->distinct()->get();
        return $lista;
    }

    public function llenarSedesDistritales($id_regional,$id_provincial){        
        $lista = Sedes::select('id','cod_sede_distrital','sede_distrital')->where('cod_sede_regional',$id_regional)->where('cod_sede_provincial',$id_provincial)->distinct()->get();
        return $lista;
    }
    public function llenarDocumentos(){  
        
        $entregables = Documentos::select('id','tipo','subtipo','orden','etiqueta')->where('subtipo',1)->get();
        $no_aplicacion = Documentos::select('id','tipo','subtipo','orden','etiqueta')->where('subtipo',2)->get();
        $inei = Documentos::select('id','tipo','subtipo','orden','etiqueta')->where('subtipo',3)->get();
        return compact('entregables','no_aplicacion','inei');
    }

    public function llenarCargos(){          
        $data = Cargo::select('id','cargo')->get();     
        return response()->json(['select'=>$data]);
    }
    
    
      
    

    
}
