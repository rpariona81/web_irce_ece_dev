<?php

namespace App;

use App\Admin\Traits\Auditorias;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use Auditorias;
    protected $table = 't_dig_persona';
   
    protected $fillable =[
        'cargo_id',
        'nombres',
        'doc_identidad',
        'nro_documento',
        'estado',
    ];


}
