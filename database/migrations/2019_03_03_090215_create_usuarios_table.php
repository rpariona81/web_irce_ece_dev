<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_usuarios', function (Blueprint $table) { 
            $table->increments('id');
            $table->integer('id_rol')->unsigned();           
            $table->string('email',100)->unique();
            $table->string('password',255);
            $table->string('nombres',200)->nullable();
            $table->string('cargo',200)->nullable();
            $table->integer('estado')->default(1);         
            $table->string('avatar')->nullable();
          
            $table->rememberToken();           
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();            
            $table->integer('deleted_by')->nullable();  
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('id_rol')->references('id')->on('t_roles');        
        });
        
        DB::table('t_usuarios')->insert(array('id_rol'=>'1','email'=>'john.doe@example.com','password'=>'$2y$10$ga0HxfpNEF7kRpxciepzC.l4scufJDPs9S7Qk000Zmt/itbtJmz.O'));
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_usuarios');
    }
}
