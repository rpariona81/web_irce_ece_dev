<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('exportar_excel_comprobantes','ComprobanteController@exportarExcel');
Route::get('exportar_excel_gastos','GastoController@exportarExcel');

Route::get('downloads/{model}/{type}/{external_id}', 'DownloadController@downloadExternal')->name('download.external_id');

Route::get('impresion_pdf/{identificador}/{codigo}', 'ExtraController@imprimirPDF');

Route::get('/auth/social/{provider}', 'SocialAuthController@providerRedirect');
Route::get('/auth/{provider}/callback', 'SocialAuthController@providerRedirectCallback');

 Route::get('{vue?}', function () {
    return view('home');
})->where('vue', '[\/\w\.-]*')->name('home');
 
//Route::get('/{any}', 'SpaController@index')->where('any', '.*');