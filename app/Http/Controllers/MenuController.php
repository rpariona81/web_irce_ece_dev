<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Util\Service;

use App\Menu;

class MenuController extends Controller
{
    //llenar el combo
    public function llenarSelect(){
        $select = Menu::select(['id','icono','titulo','link'])->where('estado',1)->get();
        return $select;
    }

    //mostarmos en el sidebar del menu
    public function mostrarSideBar(){
        //return Auth::user();
        
       // return Service::makeMenu('');
        return Service::prepararMenuJson();
        
    }

    //obtenemos los menús de un determinado rol
    public function MenuRol($rol){      
          
        if($rol > 1 ){            
            $menus = Menu::menuxRol($rol);
        }
        else {
            $menus = Menu::select(['id','id_padre','titulo','icono','link','orden'])->where('estado',1)->get();
        }
        
        return $menus;
    }
    
    //funciones generales de mantenimiento
    public function listar(){
        $menus = Menu::select(['id','id_padre','titulo','icono','link','descripcion','orden','estado','created_at']);
        $menus->with(['padre:id,titulo,icono,link']);

		if(request()->has('titulo'))
            $menus->where('titulo','like','%'.request('titulo').'%');
        if(request()->has('estado'))
            $menus->where('estado',request('estado'));

        $menus->orderBy(request('ordenarPor'),request('order'));

        return $menus->paginate(request('pageLength'));
    }

    public function crear(Request $request){
        $nuevo = new Menu($request->all());        
        $nuevo->save();
        return response()->json(['message' => 'El Menú se guardó correctamente']);
    }

    public function modificar(Request $request,$id){
        $editado = Menu::findOrFail($request->id);        
        $editado->update($request->all()); 
        return response()->json(['message' => 'El Menú se modificó correctamente']);
    }

    public function eliminar($id){
        $eliminado = Menu::findOrFail($id);
        $eliminado->estado = 9 ;
        $eliminado->save(); 
    }

    public function inactivar($id){
        $inactivado = Menu::findOrFail($id);
        $inactivado->estado = 2 ;
        $inactivado->save(); 
        return response()->json(['message' => 'El Menú se inactivó correctamente']);
    }

    public function activar($id){
        $activado = Menu::findOrFail($id);
        $activado->estado = 1;
        $activado->save(); 
        return response()->json(['message' => 'El Menú se activó correctamente']);
    }
    //fin funciones generales de mantenimiento
}
