<?php

namespace App\Admin\Traits;

use App\Usuario;
use JWTAuth;

trait Auditorias
{
    public static function boot()
    {      
        parent::boot();        
        self::auditarUsuarios();        
    }

    public static function auditarUsuarios()
    {       
        static::creating(function ($model) {              
            $id_user = JWTAuth::parseToken()->authenticate()->id;            
            $model->attributes['created_by'] = $id_user;
            $model->attributes['updated_by'] = $id_user;
        });
        static::updating(function ($model) {  
            $id_user = JWTAuth::parseToken()->authenticate()->id;           
            $model->attributes['updated_by'] = 5;
            //return var_dump("modifico ".JWTAuth::parseToken()->authenticate()->id);
        });
        static::deleting(function ($model) {
            $id_user = JWTAuth::parseToken()->authenticate()->id;
            $model->attributes['deleted_by'] = $id_user;
        });       
    }

    public function getCreatedByAttribute($value)
    {
        return $value ? Usuario::find($value)->id_persona : 'Desconocido';
    }

    public function getUpdatedByAttribute($value)
    {
        return $value ? Usuario::find($value)->id_persona : 'Desconocido';
    }

    public function getDeletedByAttribute($value)
    {
        return $value ? Usuario::find($value)->id_persona : 'Desconocido';
    }
}