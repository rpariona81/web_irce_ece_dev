<?php

namespace App;

use App\Admin\Traits\Auditorias;
use Illuminate\Database\Eloquent\Model;

class CantidadDocumentos extends Model
{
    use Auditorias;
    protected $table = 't_dig_documento';
    //protected $dateFormat = 'Y-d-m H:i:s';

    protected $fillable =[
        'persona_id',     
        'sede_id',     
        'etapa',     
        'operativo',     
        'fecha_registro',     
        'observacion_etapa',
        'created_by',
        'updated_by',

    ];
}
