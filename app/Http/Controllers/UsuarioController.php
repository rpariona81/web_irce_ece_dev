<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Persona;
use App\Usuario;
use App\Rol;

class UsuarioController extends Controller
{

    //funciones generales de mantenimiento
    public function listar(){
        
        $usuarios = Usuario::select('id','id_rol','email','nombres','cargo','estado','created_at')->with(['rol:id,nombre']);        
        //$usuarios = Usuario::with('persona');        
        

        if(request()->has('estado'))
            $usuarios->where('estado',request('estado'));      
                        
        if(request()->has('nombres'))
            $usuarios->where('nombres', 'like', request('nombres').'%'); 
       
        $usuarios->orderBy(request('ordenarPor'),request('order'));
       
        return $usuarios->paginate(request('pageLength'));
    }

    public function crear(Request $request){
        //corregir esto: jape corregido
        try {
            $usuario = new Usuario($request->all());
            $usuario->password = Hash::make($request->password);
            $usuario->save();
            return response()->json(['message' => 'El Usuario ha sido creado correctamente']);
        } catch (Exception $e) {
            return response()->json(['message' => 'Tuvimos un problema inténtelo denuevo mas tarde']);
        }        
    }

    public function modificar(Request $request){
        try {
            $usuario = Usuario::findOrFail($request->id);
            if(!empty($request->password)){$usuario->password = bcrypt($request->password);}
            $usuario->update($request->all());
            return response()->json(['message' => 'El Usuario ha sido modificado correctamente']);
        } catch (Exception $e) {
            return response()->json(['message' => 'Tuvimos un problema inténtelo denuevo mas tarde']);
        }
        
    }

    public function eliminar(){
        $eliminado = Usuario::findOrFail($request->id);
        $eliminado->estado = 9 ;
        $eliminado->save(); 
    }

    public function inactivar($id){
        $inactivado = Usuario::findOrFail($id);
        $inactivado->estado = 2 ;
        $inactivado->save(); 
        return response()->json(['message' => 'El Usuario ha sido inactivado correctamente']);
    }

    public function activar($id){
        $activado = Usuario::findOrFail($id);
        $activado->estado = 1 ;
        $activado->save();
        return response()->json(['message' => 'El Usuario ha sido activado correctamente']);
    }
    //fin funciones generales de mantenimiento


    //web Service

    public function mostrar(){
        $usuarios = Usuario::with(['rol:id,nombre','persona:id,nombres,tipo_doc,num_doc,direccion,telefono,email,estado,created_at'])->get();
        return response()->json(['usuarios' => $usuarios]);        
    }

}
