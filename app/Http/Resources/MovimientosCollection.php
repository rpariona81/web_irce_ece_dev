<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MovimientosCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key)
        {
            return [
                'id' => $row->id,
                'orden' => $key+1,              
                'tipo_movimiento' => $row->t_movimiento,
                'nombre_movimiento' => $row->documento->description,
                'id_almacen_origen' => $row->id_almacen_origen ,
                'almacen_origen' => ($row->id_almacen_origen == null) ? null : $row->almacen_origen->nombre . ' - '. $row->almacen_origen->direccion ,
                'id_almacen_destino' => $row->id_almacen_destino ,
                'almacen_destino' => ($row->id_almacen_destino == null) ? null : $row->almacen_destino->nombre . ' - '. $row->almacen_destino->direccion   ,
                'id_proveedor' => $row->id_proveedor_destinatario ,
                'proveedor' => ($row->proveedor == null) ? null : $row->proveedor->nro_doc_id . '-' . $row->proveedor->rz ,
                'orden_compra' => $row->orden_compra,
                'fecha_movimiento' => $row->fecha_movimiento,
                'serie' => $row->serie,
                'numero' => $row->numero,
                'serie_numero' => $row->serie  . " - " . $row->numero,
                'guias' => $row->guias,
                'external_id' => $row->external_id,
                'op_gravada' => $row->op_gravada,
                'op_exonerada' => $row->op_exonerada,
                'op_inafecta' => $row->op_inafecta,
                'op_gratuita' => $row->op_gratuita,
                'igv' => $row->igv,
                'total' => $row->total,          
                'estado' => $row->estado,       
                'estado_descripcion' => $row->state_type->description,            
                'observacion' => $row->observacion,
                'download_external_xml' => $row->download_external_xml,
                'download_external_pdf' => $row->download_external_pdf,
                'download_external_cdr' => $row->download_external_cdr,
                'usuario' => $row->created_by,
                'fecha_registro' => $row->created_at->format('Y-m-d H:i:s'),
                'fecha_modificacion' => $row->updated_at->format('Y-m-d H:i:s'),
            ];
        });
    }
    
}
