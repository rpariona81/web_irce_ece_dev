<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sedes extends Model
{
    protected $table = 't_cod_sede';

    protected $fillable =[
        'cod_sede_regional	',
        'sede_regional	',
        'cod_sede_provincial',
        'sede_provincial',
        'cod_sede_distrital',
        'sede_distrital',
        'codigo_sede',      
    ];

  
}
