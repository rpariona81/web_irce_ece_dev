<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTCargo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_cod_cargo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cargo');
            $table->timestamps();
        });
        DB::table('t_cod_cargo')->insert(array('cargo' => 'METODOLOGO(A)'));
        DB::table('t_cod_cargo')->insert(array('cargo' => 'SUPERVISOR(A)'));
        DB::table('t_cod_cargo')->insert(array('cargo' => 'ANALISTA CONSISTENCIA'));
        DB::table('t_cod_cargo')->insert(array('cargo' => 'OPERADOR(A)'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_cod_cargo');
    }
}
