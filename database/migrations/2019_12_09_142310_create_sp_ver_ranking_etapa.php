<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpVerRankingEtapa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
            CREATE PROCEDURE ver_ranking_etapa(IN sp_etapa INT) NO SQL 
            BEGIN 
            SELECT ROW_NUMBER() OVER (ORDER BY p_01_persona DESC) as row_num, 
            operativo_nombre, etapa_nombre, nombres, 
            CASE WHEN p_01_persona IS NULL THEN 0 ELSE p_01_persona END AS p_01_persona 
            FROM 
            (SELECT t1.operativo,
                        CASE 
                        WHEN operativo=1 THEN 'ECE' 
                        WHEN operativo=2 THEN 'EM'
                        ELSE NULL END AS operativo_nombre,
                        t2.id as persona_id,t2.nombres,t1.etapa,
                        CASE 
                        WHEN t1.etapa=1 THEN 'Inventario y Recepción' 
                        WHEN t1.etapa=2 THEN 'Revisión y Consistencia' 
                        WHEN t1.etapa=3 THEN 'Control de Calidad' 
                        WHEN t1.etapa=4 THEN 'Embalado' 
                        ELSE NULL END AS etapa_nombre,
                                    SUM(t4.p_01) AS p_01_persona 
                        FROM t_dig_documento t1
                        LEFT JOIN t_dig_persona t2 on t1.persona_id=t2.id 
                        LEFT JOIN t_cod_sede t3 on t1.sede_id=t3.id 
                        LEFT JOIN t_det_dig_documento t4 on t1.id=t4.cabecera_id
                        WHERE t2.estado=1 
                                    AND t1.etapa=sp_etapa 
                                    GROUP BY operativo, persona_id, etapa 
                                    ORDER BY operativo, persona_id, etapa) A;
            END");
        }else{

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP PROCEDURE ver_ranking_etapa");
    }
}
