<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documentos extends Model
{
    protected $table = 't_cod_documento';

    protected $fillable =[
        'tipo',
        'subtipo',
        'orden',
        'etiqueta',
    ];
}
