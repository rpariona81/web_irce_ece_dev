<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class GastoCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key)
        {
            if($row->estado==1) $estado = 'Activo';
            elseif($row->estado==1) $estado = 'Inactivo';
            else $estado = 'Eliminado';
            return [
                'N°' => $key + 1 ,
                'tipo_gasto' => $row->tipo_gasto->nombre,
                'concepto' => $row->concepto,
                'fecha' => $row->fecha,
                'moneda' => $row->moneda,
                'monto' => $row->monto,
                'estado' => $estado,                
                'created_at' => $row->created_at->format('Y-m-d H:i:s'),
            ];
        });
    }
}
