<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTPersona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_dig_persona', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cargo_id')->unsigned();
            $table->string('nombres', 100);
            $table->integer('doc_identidad')->nullable();
            $table->string('nro_documento', 20)->nullable();
            $table->boolean('estado')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('cargo_id')->references('id')->on('t_cod_cargo');

        });
        /*DB::table('t_dig_persona')->insert(array('cargo_id' => '1', 'nombres' => 'MARCO SOLIS', 'doc_identidad' => '1', 'nro_documento' => '99558800', 'estado' => '1'));
        DB::table('t_dig_persona')->insert(array('cargo_id' => '2', 'nombres' => 'NOLBERTO SOLANO', 'doc_identidad' => '1', 'nro_documento' => '99558801', 'estado' => '1'));
        DB::table('t_dig_persona')->insert(array('cargo_id' => '3', 'nombres' => 'ROBERTO PALACIOS', 'doc_identidad' => '1', 'nro_documento' => '99558802', 'estado' => '1'));
        DB::table('t_dig_persona')->insert(array('cargo_id' => '4', 'nombres' => 'CHARLIE BROWN', 'doc_identidad' => '1', 'nro_documento' => '99558803', 'estado' => '1'));
        DB::table('t_dig_persona')->insert(array('cargo_id' => '4', 'nombres' => 'STEVE ROGERS', 'doc_identidad' => '2', 'nro_documento' => '0099558804', 'estado' => '0'));*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_dig_persona');
    }
}
