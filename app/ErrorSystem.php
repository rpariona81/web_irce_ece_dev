<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ErrorSystem extends Model
{    
    protected $table = 't_error_systems';

    protected $fillable =[
        'id_empresa',
        'id_persona',
        'tabla',
        'accion',
        'accion_det',
        'descripcion',
    ];

    public function empresa(){
        return $this->belongsTo('App\Empresa','id_empresa');
    }

    public function usuario(){
        return $this->belongsTo('App\Persona','id_persona');
    }



}
