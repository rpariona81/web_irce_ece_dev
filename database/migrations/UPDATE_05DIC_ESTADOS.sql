/*UPDATE t_cod_documento SET tipo=1 where subtipo=2; */

ALTER TABLE t_dig_documento 
ADD COLUMN estado INTEGER DEFAULT 1 AFTER observacion_etapa;

ALTER TABLE t_cod_documento 
ADD COLUMN estado INTEGER DEFAULT 1 AFTER etiqueta;

/*CREATE UNIQUE INDEX unico_cabecera_doc ON t_det_dig_documento(cabecera_id,documento_id,operativo_marco);*/