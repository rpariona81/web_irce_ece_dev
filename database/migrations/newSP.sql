      CREATE VIEW  resum_marco_v2 AS 
            SELECT 
            sede_id,operativo_marco,t2.tipo as tipodoc,
            SUM(p_01_marco) AS p0_01_tot,
            0 AS p0_02_tot 
            from t_dig_marco t1 
			LEFT JOIN t_cod_documento t2 ON t1.documento_id=t2.id 
			GROUP BY t1.sede_id, t1.operativo_marco, t2.tipo;
            
			CREATE VIEW resum_sede_operativo_etapa1_v2 AS
            SELECT 
            t1.sede_id,
            t1.operativo, t3.tipo as tipodoc,
            SUM(t2.p_01) AS p1_01,
            SUM(t2.p_02) AS p1_02 
            from t_dig_documento t1
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
			LEFT JOIN t_cod_documento t3 on t3.id=t2.documento_id 
            where t1.etapa=1 
            group by t1.sede_id, t1.operativo, t3.tipo;

      CREATE VIEW resum_sede_operativo_etapa2_v2 AS
            SELECT 
            t1.sede_id,
            t1.operativo, t3.tipo as tipodoc,
            SUM(t2.p_01) AS p2_01,
            SUM(t2.p_02) AS p2_02 
            from t_dig_documento t1
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
			LEFT JOIN t_cod_documento t3 on t3.id=t2.documento_id 
            where t1.etapa=2 
            group by t1.sede_id, t1.operativo, t3.tipo;
			
      CREATE VIEW resum_sede_operativo_etapa3_v2 AS
            SELECT 
            t1.sede_id,
            t1.operativo, t3.tipo as tipodoc,
            SUM(t2.p_01) AS p3_01,
            SUM(t2.p_02) AS p3_02 
            from t_dig_documento t1
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
			LEFT JOIN t_cod_documento t3 on t3.id=t2.documento_id 
            where t1.etapa=3 
            group by t1.sede_id, t1.operativo, t3.tipo;
			
      CREATE VIEW resum_sede_operativo_etapa4_v2 AS
            SELECT 
            t1.sede_id,
            t1.operativo, t3.tipo as tipodoc,
            SUM(t2.p_01) AS p4_01,
            SUM(t2.p_02) AS p4_02 
            from t_dig_documento t1
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
			LEFT JOIN t_cod_documento t3 on t3.id=t2.documento_id 
            where t1.etapa=4 
            group by t1.sede_id, t1.operativo, t3.tipo;
			
      CREATE VIEW resum_sede_operativo_etapa_1234_v2 as 
            SELECT  
            t1.id as sede_id,
            t1.cod_sede_regional,
            t1.sede_regional,
            t1.cod_sede_provincial,
            t1.sede_provincial,
            t1.cod_sede_distrital,
            t1.sede_distrital,
            t0.operativo_marco as operativo,
            CASE 
            WHEN t0.operativo_marco=1 THEN 'ECE' 
            WHEN t0.operativo_marco=2 THEN 'EM' 
            ELSE NULL 
            END
            AS operativo_nombre,
			t0.tipodoc,
			CASE 
            WHEN t0.tipodoc=1 THEN 'MINEDU' 
            WHEN t0.tipodoc=2 THEN 'INEI' 
            ELSE NULL 
            END
            AS tipodoc_nombre,
            t0.p0_01_tot, 
            0 AS p0_02_tot, 
            IFNULL(t2.p1_01,0) AS p1_01, 
            IFNULL(t2.p1_02,0) AS p1_02, 
            IFNULL(t4.p2_01,0) AS p2_01,
            IFNULL(t4.p2_02,0) AS p2_02, 
            IFNULL(t5.p3_01,0) AS p3_01, 
            IFNULL(t5.p3_02,0) AS p3_02, 
            IFNULL(t6.p4_01,0) AS p4_01,
            IFNULL(t6.p4_02,0) AS p4_02 
            from t_cod_sede t1 
            left join resum_marco_v2 t0 on t1.id=t0.sede_id 
            left join resum_sede_operativo_etapa1_v2 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo AND t0.tipodoc=t2.tipodoc  
            left join resum_sede_operativo_etapa2_v2 t4 on t4.sede_id=t2.sede_id AND t0.operativo_marco=t4.operativo AND t0.tipodoc=t4.tipodoc 
            left join resum_sede_operativo_etapa3_v2 t5 on t5.sede_id=t2.sede_id AND t0.operativo_marco=t5.operativo AND t0.tipodoc=t5.tipodoc 
            left join resum_sede_operativo_etapa4_v2 t6 on t6.sede_id=t2.sede_id AND t0.operativo_marco=t6.operativo AND t0.tipodoc=t6.tipodoc 
            order by t1.id, t0.operativo_marco, t0.tipodoc;



/*SELECT
	t1.id AS sede_id,
	t1.cod_sede_regional AS cod_sede_regional,
	t1.sede_regional AS sede_regional,
	t1.cod_sede_provincial AS cod_sede_provincial,
	t1.sede_provincial AS sede_provincial,
	t1.cod_sede_distrital AS cod_sede_distrital,
	t1.sede_distrital AS sede_distrital,
	t0.operativo_marco AS operativo,
	CASE
WHEN t0.operativo_marco = 1 THEN
	'ECE'
WHEN t0.operativo_marco = 2 THEN
	'EM'
ELSE
	NULL
END AS operativo_nombre,
 t0.tipodoc AS tipodoc,
 CASE
WHEN t0.tipodoc = 1 THEN
	'MINEDU'
WHEN t0.tipodoc = 2 THEN
	'INEI'
ELSE
	NULL
END AS tipodoc_nombre,
 t0.p0_01_tot AS p0_01_tot,
 0 AS p0_02_tot,
 '0.00%' AS p0_02_porc,
 ifnull(t2.p1_01, 0)AS p1_01,
 concat(
	format(

		IF(
			ifnull(t2.p1_01, 0)= 0,
			0,
			t2.p1_01 / t0.p0_01_tot * 100.0
		),
		2
	),
	'%'
)AS p1_01_avance,
 ifnull(t2.p1_02, 0)AS p1_02,
 concat(
	format(

		IF(
			ifnull(t2.p1_02, 0)= 0,
			0,
			t2.p1_02 / t0.p0_01_tot * 100.0
		),
		2
	),
	'%'
)AS p1_02_avance,
 ifnull(t4.p2_01, 0)AS p2_01,
 concat(
	format(

		IF(
			ifnull(t4.p2_01, 0)= 0,
			0,
			t4.p2_01 / t0.p0_01_tot * 100.0
		),
		2
	),
	'%'
)AS p2_01_avance,
 ifnull(t4.p2_02, 0)AS p2_02,
 concat(
	format(

		IF(
			ifnull(t4.p2_02, 0)= 0,
			0,
			t4.p2_02 / t0.p0_01_tot * 100.0
		),
		2
	),
	'%'
)AS p2_02_avance,
 ifnull(t5.p3_01, 0)AS p3_01,
 concat(
	format(

		IF(
			ifnull(t5.p3_01, 0)= 0,
			0,
			t5.p3_01 / t0.p0_01_tot * 100.0
		),
		2
	),
	'%'
)AS p3_01_avance,
 ifnull(t5.p3_02, 0)AS p3_02,
 concat(
	format(

		IF(
			ifnull(t5.p3_02, 0)= 0,
			0,
			t5.p3_02 / t0.p0_01_tot * 100.0
		),
		2
	),
	'%'
)AS p3_02_avance,
 ifnull(t6.p4_01, 0)AS p4_01,
 concat(
	format(

		IF(
			ifnull(t6.p4_01, 0)= 0,
			0,
			t6.p4_01 / t0.p0_01_tot * 100.0
		),
		2
	),
	'%'
)AS p4_01_avance,
 ifnull(t6.p4_02, 0)AS p4_02,
 concat(
	format(

		IF(
			ifnull(t6.p4_02, 0)= 0,
			0,
			t6.p4_02 / t0.p0_01_tot * 100.0
		),
		2
	),
	'%'
)AS p4_02_avance
FROM
	(
		(
			(
				(
					(
						t_cod_sede t1
						LEFT JOIN resum_marco_v2 t0 ON(t1.id = t0.sede_id)
					)
					LEFT JOIN resum_sede_operativo_etapa1_v2 t2 ON(
						t2.sede_id = t1.id
						AND t0.operativo_marco = t2.operativo
						AND t0.tipodoc = t2.tipodoc
					)
				)
				LEFT JOIN resum_sede_operativo_etapa2_v2 t4 ON(
					t4.sede_id = t2.sede_id
					AND t0.operativo_marco = t4.operativo
					AND t0.tipodoc = t4.tipodoc
				)
			)
			LEFT JOIN resum_sede_operativo_etapa3_v2 t5 ON(
				t5.sede_id = t2.sede_id
				AND t0.operativo_marco = t5.operativo
				AND t0.tipodoc = t5.tipodoc
			)
		)
		LEFT JOIN resum_sede_operativo_etapa4_v2 t6 ON(
			t6.sede_id = t2.sede_id
			AND t0.operativo_marco = t6.operativo
			AND t0.tipodoc = t6.tipodoc
		)
	)
ORDER BY
	t1.id,
	t0.operativo_marco,
	t0.tipodoc*/
	
	
	
SELECT 
NULL AS sede_id,
NULL AS cod_sede_regional,
'TOTAL' AS sede_regional,
NULL AS cod_sede_provincial,
NULL AS sede_provincial,
NULL AS cod_sede_distrital,
NULL AS sede_distrital,
NULL AS operativo,
NULL AS operativo_nombre,
NULL AS tipodoc,
NULL AS tipodoc_nombre,
IFNULL(SUM(p0_01_tot),0) AS p0_01_tot,
IFNULL(SUM(p1_01),0) AS p1_01,
CONCAT(FORMAT((IFNULL(SUM(p1_01),0)/SUM(p0_01_tot))*100,2),'%') AS p1_01_avance, 
IFNULL(SUM(p1_02),0) AS p1_02, 
CONCAT(FORMAT((IFNULL(SUM(p1_02),0)/SUM(p0_01_tot))*100,2),'%') AS p1_02_avance,
IFNULL(SUM(p2_01),0) AS p2_01,
CONCAT(FORMAT((IFNULL(SUM(p2_01),0)/SUM(p0_01_tot))*100,2),'%') AS p2_01_avance, 
IFNULL(SUM(p2_02),0) AS p2_02, 
CONCAT(FORMAT((IFNULL(SUM(p2_02),0)/SUM(p0_01_tot))*100,2),'%') AS p2_02_avance, 
IFNULL(SUM(p3_01),0) AS p3_01,
CONCAT(FORMAT((IFNULL(SUM(p3_01),0)/SUM(p0_01_tot))*100,2),'%') AS p3_01_avance, 
IFNULL(SUM(p3_02),0) AS p3_02, 
CONCAT(FORMAT((IFNULL(SUM(p3_02),0)/SUM(p0_01_tot))*100,2),'%') AS p3_02_avance, 
IFNULL(SUM(p4_01),0) AS p4_01,
CONCAT(FORMAT((IFNULL(SUM(p4_01),0)/SUM(p0_01_tot))*100,2),'%') AS p4_01_avance, 
IFNULL(SUM(p4_02),0) AS p4_02, 
CONCAT(FORMAT((IFNULL(SUM(p4_02),0)/SUM(p0_01_tot))*100,2),'%') AS p4_02_avance 
FROM resum_sede_operativo_etapa_1234_v2 
WHERE operativo=1 
/*WHERE tipodoc=1 */
/*WHERE cod_sede_regional='001' */
/*WHERE cod_sede_regional='153' and cod_sede_provincial='15' */
/*WHERE cod_sede_regional='153' and cod_sede_provincial='15' AND cod_sede_distrital='01' */ 
UNION ALL
SELECT sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,
AVG(operativo) AS operativo,
CASE 
WHEN AVG(operativo)=1 THEN 'ECE' 
WHEN AVG(operativo)=2 THEN 'EM' 
ELSE 'ECE-EM' END AS operativo_nombre,
AVG(tipodoc) AS tipodoc,
CASE 
WHEN AVG(tipodoc)=1 THEN 'MINEDU' 
WHEN AVG(tipodoc)=2 THEN 'INEI' 
ELSE 'MINEDU-INEI' END AS tipodoc_nombre,
IFNULL(SUM(p0_01_tot),0) AS p0_01_tot,
IFNULL(SUM(p1_01),0) AS p1_01,
CONCAT(FORMAT((IFNULL(SUM(p1_01),0)/SUM(p0_01_tot))*100,2),'%') AS p1_01_avance, 
IFNULL(SUM(p1_02),0) AS p1_02, 
CONCAT(FORMAT((IFNULL(SUM(p1_02),0)/SUM(p0_01_tot))*100,2),'%') AS p1_02_avance,
IFNULL(SUM(p2_01),0) AS p2_01,
CONCAT(FORMAT((IFNULL(SUM(p2_01),0)/SUM(p0_01_tot))*100,2),'%') AS p2_01_avance, 
IFNULL(SUM(p2_02),0) AS p2_02, 
CONCAT(FORMAT((IFNULL(SUM(p2_02),0)/SUM(p0_01_tot))*100,2),'%') AS p2_02_avance, 
IFNULL(SUM(p3_01),0) AS p3_01,
CONCAT(FORMAT((IFNULL(SUM(p3_01),0)/SUM(p0_01_tot))*100,2),'%') AS p3_01_avance, 
IFNULL(SUM(p3_02),0) AS p3_02, 
CONCAT(FORMAT((IFNULL(SUM(p3_02),0)/SUM(p0_01_tot))*100,2),'%') AS p3_02_avance, 
IFNULL(SUM(p4_01),0) AS p4_01,
CONCAT(FORMAT((IFNULL(SUM(p4_01),0)/SUM(p0_01_tot))*100,2),'%') AS p4_01_avance, 
IFNULL(SUM(p4_02),0) AS p4_02, 
CONCAT(FORMAT((IFNULL(SUM(p4_02),0)/SUM(p0_01_tot))*100,2),'%') AS p4_02_avance 
FROM resum_sede_operativo_etapa_1234_v2 
WHERE operativo=1 
/*WHERE tipodoc=1   */
/*WHERE cod_sede_regional='001' */
/*WHERE cod_sede_regional='153' and cod_sede_provincial='15' */
/*WHERE cod_sede_regional='153' and cod_sede_provincial='15' AND cod_sede_distrital='01' */ 
GROUP BY sede_id




DELIMITER //
CREATE PROCEDURE sp_ver_avance_sede_operativo_tipodoc(IN sp_region CHAR(3), IN sp_provincia CHAR(2), IN sp_distrito CHAR(2), IN sp_operativo INT, IN sp_tipodoc INT) NO SQL 
BEGIN
SET @queryOperativo = ('');
SET @queryTipoDoc = ('');
SET @queryRegion = ('');
SET @queryProvincia = ('');
SET @queryDistrito = ('');
SET @queryAND = ('');
SET @queryWHERE = ('');
SET @queryPARAMS = 0;
IF sp_operativo <> 0 THEN
	SET @queryOperativo = CONCAT(' operativo = ', sp_operativo, ' ');
	SET @queryPARAMS = @queryPARAMS + 1;
END IF;
IF sp_tipodoc <> 0 THEN
	SET @queryTipoDoc = CONCAT(' tipodoc = ', sp_tipodoc, ' ');
	SET @queryPARAMS = @queryPARAMS + 1;
END IF;
IF sp_region <> '' THEN
	SET @queryRegion = CONCAT(' cod_sede_regional = ', sp_region, ' ');
	SET @queryPARAMS = @queryPARAMS + 1;
END IF;
IF sp_provincia <> '' THEN
	SET @queryProvincia = CONCAT(' cod_sede_provincial = ', sp_provincia, ' ');
	SET @queryPARAMS = @queryPARAMS + 1;
END IF;
IF sp_distrito <> '' THEN
	SET @queryDistrito = CONCAT(' cod_sede_distrital = ', sp_distrito, ' ');
	SET @queryPARAMS = @queryPARAMS + 2;
END IF;
IF @queryPARAMS > 0 THEN
	SET @queryWHERE = (' WHERE ');
END IF;
IF @queryPARAMS > 0 THEN
	SET @queryAND = (' AND ');
END IF;
SET @queryString = CONCAT("SELECT 
					NULL AS row_num,
					NULL AS sede_id,
					NULL AS cod_sede_regional,
					'TOTAL' AS sede_regional,
					NULL AS cod_sede_provincial,
					NULL AS sede_provincial,
					NULL AS cod_sede_distrital,
					NULL AS sede_distrital,
					NULL AS operativo,
					NULL AS operativo_nombre,
					NULL AS tipodoc,
					NULL AS tipodoc_nombre,
					IFNULL(SUM(p0_01_tot),0) AS p0_01_tot,
					IFNULL(SUM(p1_01),0) AS p1_01,
					CONCAT(FORMAT((IFNULL(SUM(p1_01),0)/SUM(p0_01_tot))*100,2),'%') AS p1_01_avance, 
					IFNULL(SUM(p1_02),0) AS p1_02, 
					CONCAT(FORMAT((IFNULL(SUM(p1_02),0)/SUM(p0_01_tot))*100,2),'%') AS p1_02_avance,
					IFNULL(SUM(p2_01),0) AS p2_01,
					CONCAT(FORMAT((IFNULL(SUM(p2_01),0)/SUM(p0_01_tot))*100,2),'%') AS p2_01_avance, 
					IFNULL(SUM(p2_02),0) AS p2_02, 
					CONCAT(FORMAT((IFNULL(SUM(p2_02),0)/SUM(p0_01_tot))*100,2),'%') AS p2_02_avance, 
					IFNULL(SUM(p3_01),0) AS p3_01,
					CONCAT(FORMAT((IFNULL(SUM(p3_01),0)/SUM(p0_01_tot))*100,2),'%') AS p3_01_avance, 
					IFNULL(SUM(p3_02),0) AS p3_02, 
					CONCAT(FORMAT((IFNULL(SUM(p3_02),0)/SUM(p0_01_tot))*100,2),'%') AS p3_02_avance, 
					IFNULL(SUM(p4_01),0) AS p4_01,
					CONCAT(FORMAT((IFNULL(SUM(p4_01),0)/SUM(p0_01_tot))*100,2),'%') AS p4_01_avance, 
					IFNULL(SUM(p4_02),0) AS p4_02, 
					CONCAT(FORMAT((IFNULL(SUM(p4_02),0)/SUM(p0_01_tot))*100,2),'%') AS p4_02_avance 
					FROM resum_sede_operativo_etapa_1234_v2 WHERE ", 
					IF(sp_operativo=0,TRUE,@queryOperativo), @queryAND, 
					IF(sp_tipoDoc=0,TRUE,@queryTipoDoc), @queryAND,
					IF(sp_region='',TRUE,@queryRegion), @queryAND,
					IF(sp_provincia='',TRUE,@queryProvincia), @queryAND,
					IF(sp_distrito='',TRUE,@queryDistrito),
					"    UNION ALL 
					SELECT ROW_NUMBER() OVER (ORDER BY sede_id ASC) AS row_num,
					sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,tipodoc,tipodoc_nombre,
					p0_01_tot,p1_01,p1_01_avance,p1_02,p1_02_avance,p2_01,p2_01_avance,p2_02,p2_02_avance,p3_01,p3_01_avance,p3_02,p3_02_avance,p4_01,p4_01_avance,p4_02,p4_02_avance FROM 
					(
					SELECT sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,
					AVG(operativo) AS operativo,
					CASE 
					WHEN AVG(operativo)=1 THEN 'ECE' 
					WHEN AVG(operativo)=2 THEN 'EM' 
					ELSE 'ECE-EM' END AS operativo_nombre,
					AVG(tipodoc) AS tipodoc,
					CASE 
					WHEN AVG(tipodoc)=1 THEN 'MINEDU' 
					WHEN AVG(tipodoc)=2 THEN 'INEI' 
					ELSE 'MINEDU-INEI' END AS tipodoc_nombre,
					IFNULL(SUM(p0_01_tot),0) AS p0_01_tot,
					IFNULL(SUM(p1_01),0) AS p1_01,
					CONCAT(FORMAT((IFNULL(SUM(p1_01),0)/SUM(p0_01_tot))*100,2),'%') AS p1_01_avance, 
					IFNULL(SUM(p1_02),0) AS p1_02, 
					CONCAT(FORMAT((IFNULL(SUM(p1_02),0)/SUM(p0_01_tot))*100,2),'%') AS p1_02_avance,
					IFNULL(SUM(p2_01),0) AS p2_01,
					CONCAT(FORMAT((IFNULL(SUM(p2_01),0)/SUM(p0_01_tot))*100,2),'%') AS p2_01_avance, 
					IFNULL(SUM(p2_02),0) AS p2_02, 
					CONCAT(FORMAT((IFNULL(SUM(p2_02),0)/SUM(p0_01_tot))*100,2),'%') AS p2_02_avance, 
					IFNULL(SUM(p3_01),0) AS p3_01,
					CONCAT(FORMAT((IFNULL(SUM(p3_01),0)/SUM(p0_01_tot))*100,2),'%') AS p3_01_avance, 
					IFNULL(SUM(p3_02),0) AS p3_02, 
					CONCAT(FORMAT((IFNULL(SUM(p3_02),0)/SUM(p0_01_tot))*100,2),'%') AS p3_02_avance, 
					IFNULL(SUM(p4_01),0) AS p4_01,
					CONCAT(FORMAT((IFNULL(SUM(p4_01),0)/SUM(p0_01_tot))*100,2),'%') AS p4_01_avance, 
					IFNULL(SUM(p4_02),0) AS p4_02, 
					CONCAT(FORMAT((IFNULL(SUM(p4_02),0)/SUM(p0_01_tot))*100,2),'%') AS p4_02_avance 
					FROM resum_sede_operativo_etapa_1234_v2 WHERE ", 
					IF(sp_operativo=0,TRUE,@queryOperativo), @queryAND, 
					IF(sp_tipoDoc=0,TRUE,@queryTipoDoc), @queryAND,
					IF(sp_region='',TRUE,@queryRegion), @queryAND,
					IF(sp_provincia='',TRUE,@queryProvincia), @queryAND,
					IF(sp_distrito='',TRUE,@queryDistrito),
					"    GROUP BY sede_id) A");
				PREPARE myquery FROM @queryString;
        EXECUTE myquery;
        END;