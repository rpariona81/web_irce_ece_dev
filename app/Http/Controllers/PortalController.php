<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Util\Service;
use App\Util\Stock;
use App\Util\Funciones;
use App\Util\DocumentosAdicionales;

use JWTAuth;
use App\Cliente;
use App\EmpresaCliente;


class PortalController extends Controller
{
    
    public function consultaRuc($nro_doc){

        $empresa = JWTAuth::parseToken()->authenticate()->empresa;   
        $ruta = $empresa->servidor->dominio  ."/api/services/ruc/" . $nro_doc;
        $token = "Bearer ".  $empresa->token;
        
        

        $respuesta = Service::consumirWebService($ruta,$token,1,$data_json);

          $leer_respuesta = json_decode($respuesta, true);

          if ($leer_respuesta['success'] == false) {
              //Mostramos los errores si los hay
              return response()->json(['message'=>$leer_respuesta['message']],422);
          } else {
            $entidad= $leer_respuesta['data'];
            return response()->json(['empresa'=>$entidad]);
          }
    }

    public function ClienteInfo($tipo_doc,$nro_doc){
        
        $empresa = JWTAuth::parseToken()->authenticate()->empresa;        
        $token = "Bearer ".  $empresa->token;

        $cliente = Cliente::where('tipo_doc_id', $tipo_doc)->where('nro_doc_id', $nro_doc)->first();

        if(count( (array) $cliente) > 0){
            $empresa_cliente = EmpresaCliente::where('id_empresa',$empresa->id)->where('id_cliente',$cliente->id)->first();          
           
            if(!isset($empresa_cliente)){
                $empresa_cliente = (object) array('email'=>'','email_cc'=>'','telefono'=>'','telefono_cc'=>'');               
            }

            $cliente_info = [
                "name"=> $cliente->rz,
                "trade_name"=> $cliente->nombre_comercial,
                "address"=> $cliente->dom_fiscal,
                "phone"=> '',
                "department"=> '',
                "department_id"=> substr($cliente->ubigeo, 0, 2),
                "province"=> '',
                "province_id"=> substr($cliente->ubigeo, 0, 4),
                "district"=> '',
                "district_id"=> $cliente->ubigeo,
                "email"=>$empresa_cliente->email,
                "email_cc"=>$empresa_cliente->email_cc ,
                "telefono"=>$empresa_cliente->telefono ,
                "telefono_cc"=>$empresa_cliente->telefono_cc ,
            
            ];
            return response()->json(['empresa'=>$cliente_info,"cliente_nuevo"=>0]);   
        }
        return response()->json(['empresa'=>'',"cliente_nuevo"=>2]);   
        
         
    }

    public function consultaClienteInfo($tipo_doc,$nro_doc){
        
        $empresa = JWTAuth::parseToken()->authenticate()->empresa;        
        $token = "Bearer ".  $empresa->token;

        $cliente = Cliente::where('tipo_doc_id', $tipo_doc)->where('nro_doc_id', $nro_doc)->first();

        if(count( (array) $cliente) > 0){
            $empresa_cliente = EmpresaCliente::where('id_empresa',$empresa->id)->where('id_cliente',$cliente->id)->first();          
           
            if(!isset($empresa_cliente)){
                $empresa_cliente = (object) array('email'=>'','email_cc'=>'','telefono'=>'','telefono_cc'=>'');               
            }

            $cliente_info = [
                "name"=> $cliente->rz,
                "trade_name"=> $cliente->nombre_comercial,
                "address"=> $cliente->dom_fiscal,
                "phone"=> '',
                "department"=> '',
                "department_id"=> substr($cliente->ubigeo, 0, 2),
                "province"=> '',
                "province_id"=> substr($cliente->ubigeo, 0, 4),
                "district"=> '',
                "district_id"=> $cliente->ubigeo,
                "email"=>$empresa_cliente->email,
                "email_cc"=>$empresa_cliente->email_cc,
                "telefono"=>$empresa_cliente->telefono,
                "telefono_cc"=>$empresa_cliente->telefono_cc ,
            
            ];
            return response()->json(['empresa'=>$cliente_info,"cliente_nuevo"=>0]);   
        }
        // Si no se encuentraa al cliente lo podemos sacar de aqui si se desea :p
        
        if($tipo_doc==6 && strlen($nro_doc) == 11){
            $ruta = $empresa->servidor->dominio  ."/api/services/ruc/" . $nro_doc;
            $respuesta = Service::consumirWebService($ruta,$token);       
            
            $leer_respuesta = json_decode($respuesta, true);
  
            if ($leer_respuesta['success'] == false) {
                //Mostramos los errores si los hay
                return response()->json(['message'=>$leer_respuesta['message']],422);
            } else {
              $entidad= $leer_respuesta['data'];
              return response()->json(['empresa'=>$entidad,"cliente_nuevo"=>1]);
            }
        }
        elseif($tipo_doc==1 && strlen($nro_doc) == 8){
            $ruta = $empresa->servidor->dominio  ."/api/services/dni/" . $nro_doc;
            $respuesta = Service::consumirWebService($ruta,$token);
            $leer_respuesta = json_decode($respuesta, true);
  
            if ($leer_respuesta['success'] == false) {
                //Mostramos los errores si los hay
                return response()->json(['message'=>$leer_respuesta['message']],422);
            } else {
              $entidad= $leer_respuesta['data'];
              return response()->json(['empresa'=>$entidad,"cliente_nuevo"=>1]);
            }

        }
        else{
            return response()->json(['message'=>'Cliente no encontrado'],422);
            return response()->json(['empresa'=>'',"cliente_nuevo"=>2]);
        }
         
    }

    

    public function crear(Request $request){

        $usuario = JWTAuth::parseToken()->authenticate();
        $empresa =  $usuario->empresa;   
        $ruta = $empresa->servidor->dominio  ."/api/documents";        
        $token = "Bearer ".  $empresa->token;

       // $ruta = "http://127.0.0.1:82/api/documents";
     
        $comprobante = $request->comprobante; 
        array_splice($comprobante['items'],-1,1); //QUITO EL ULTIMO ELEMENTO DEL COMPROBANTE

        //return DocumentosAdicionales::crearDocumento($comprobante);    
    
        $data_json = json_encode($comprobante);
        //return $data_json;
        $respuesta = Service::consumirWebService($ruta,$token,1,$data_json);
           
            $leer_respuesta = json_decode($respuesta, true);       
         
          if (!isset($leer_respuesta)) {
              //Mostramos los errores si los hay               
              $error = substr($respuesta, 22, 200);
              Service::registrarError($usuario->id_persona,$empresa->id,'Comprobantes','C','Emitir Comprobante',$error);
              return response()->json(['message'=> $respuesta ],422);
          } else {
            if($request->stock==1){//veemos si hay que descontar o no el stock
                Stock::descontarStock($request->almacen,$comprobante['items']);
            }
            return response()->json(['cpe_devuelto_info'=>$leer_respuesta]);
            
          }
    }

    public function anular(Request $request){
        $usuario = JWTAuth::parseToken()->authenticate();
        $ruta = $empresa->servidor->dominio  ."/api/voided";        
        $token = "Bearer ".  $usuario->empresa->token;
        $respuesta = Service::consumirWebService($ruta,$token,1,json_encode($request->comprobante));
        $leer_respuesta = json_decode($respuesta, true);  

        if (!isset($leer_respuesta)) {
            //Mostramos los errores si los hay               
            $error = substr($respuesta, 22, 200);
            Service::registrarError($usuario->id_persona,$usuario->empresa->id,'Comprobantes','A','Anulación',$error);
            return response()->json(['message'=> $respuesta ],422);
        } else {
         /* if($request->stock==1){//veemos si hay que descontar o no el stock
              Stock::descontarStock($request->almacen,$comprobante['items']);
          } */
          return response()->json(['cpe_devuelto_info'=>$leer_respuesta]);
          
        }
         
    }

    public function buscarCPE(Request $request){

        $usuario = JWTAuth::parseToken()->authenticate();
        $empresa =  $usuario->empresa;   
        $ruta = $empresa->servidor->dominio  ."/api/search";      

        $data_json = json_encode($request->all());
        return Service::consumirWebService($ruta,$token,1,$data_json);
    }

    public function listar(){

        $usuario = JWTAuth::parseToken()->authenticate();
        $empresa =  $usuario->empresa;   
        $ruta = $empresa->servidor->dominio  ."/api/documents?" . Funciones::codificarURL(request()->all()); 
        $token = "Bearer ".  $empresa->token; 

        //return  $ruta;
        //return response()->json(['cpe_devuelto_info'=>Service::consumirWebService($ruta,$token)]);
        return Service::consumirWebService($ruta,$token);

    }

    public function listarFast(){

        $usuario = JWTAuth::parseToken()->authenticate();
        $empresa =  $usuario->empresa;   
        $ruta = $empresa->url_ws  ."/api/documents_fast?" . Funciones::codificarURL(request()->all()); 
        $token = "Bearer ".  $empresa->token;
       
        return response()->json(['cpe_devuelto_info'=>"hola"]);
        //return response()->json(Service::consumirWebService($ruta,$token));


    }
    





}//final
