<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewDetalleSedeOperativoEtapas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
            CREATE VIEW resum_sede_operativo_etapa1_documento AS 
            SELECT 
            t1.sede_id,
            t1.operativo,
            t2.documento_id,
            SUM(t2.p_01) AS p1_01,
            SUM(t2.p_02) AS p1_02 
            from t_dig_documento t1 
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
            where t1.etapa=1 
            group by t1.sede_id, t1.operativo, t2.documento_id;
            CREATE VIEW resum_sede_operativo_etapa2_documento AS 
            SELECT 
            t1.sede_id,
            t1.operativo,
            t2.documento_id,
            SUM(t2.p_01) AS p2_01,
            SUM(t2.p_02) AS p2_02 
            from t_dig_documento t1 
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
            where t1.etapa=2 
            group by t1.sede_id, t1.operativo, t2.documento_id;
            CREATE VIEW resum_sede_operativo_etapa3_documento AS 
            SELECT 
            t1.sede_id,
            t1.operativo,
            t2.documento_id,
            SUM(t2.p_01) AS p3_01,
            SUM(t2.p_02) AS p3_02 
            from t_dig_documento t1 
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
            where t1.etapa=3 
            group by t1.sede_id, t1.operativo, t2.documento_id;
            CREATE VIEW resum_sede_operativo_etapa4_documento AS 
            SELECT 
            t1.sede_id,
            t1.operativo,
            t2.documento_id,
            SUM(t2.p_01) AS p4_01,
            SUM(t2.p_02) AS p4_02 
            from t_dig_documento t1 
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
            where t1.etapa=4 
            group by t1.sede_id, t1.operativo, t2.documento_id; 
            CREATE VIEW resum_sede_operativo_etapa_1234_documento as 
            SELECT 
            t1.id as sede_id,
            t1.cod_sede_regional,
            t1.sede_regional,
            t1.cod_sede_provincial,
            t1.sede_provincial,
            t1.cod_sede_distrital,
            t1.sede_distrital,
            t2.operativo,
            CASE 
            WHEN t2.operativo=1 THEN 'ECE' 
            WHEN t2.operativo=2 THEN 'EM' 
            ELSE NULL 
            END 
            AS operativo_nombre,
            CASE 
            WHEN t3.tipo = 1 THEN 'MINEDU' 
            WHEN t3.tipo = 2 THEN 'INEI' 
            ELSE NULL  
            END AS tipo,
            CASE 
            WHEN t3.subtipo=1 THEN '1.ENTREGABLE' 
            WHEN t3.subtipo=2 THEN '2.NO APLICACIÓN' 
            WHEN t3.subtipo=3 THEN '3.OPERACIÓN DE CAMPO - INEI' 
            ELSE NULL
            END AS subtipo,
            t3.orden,
            t3.etiqueta,
            t2.p1_01,
            t2.p1_02,
            t4.p2_01,
            t4.p2_02,
            t5.p3_01,
            t5.p3_02,
            t6.p4_01,
            t6.p4_02 
            from 
            t_cod_sede t1 
            left join resum_sede_operativo_etapa1_documento t2 on t2.sede_id=t1.id 
            left join resum_sede_operativo_etapa2_documento t4 on t4.sede_id=t2.sede_id and t4.documento_id=t2.documento_id and t4.operativo=t2.operativo 
            left join resum_sede_operativo_etapa3_documento t5 on t5.sede_id=t2.sede_id and t5.documento_id=t2.documento_id and t5.operativo=t2.operativo 
            left join resum_sede_operativo_etapa4_documento t6 on t6.sede_id=t2.sede_id and t6.documento_id=t2.documento_id and t6.operativo=t2.operativo 
            left join t_cod_documento t3 on t3.id=t2.documento_id 
            order by t1.codigo_sede, t2.operativo, t3.tipo, t3.subtipo, t3.orden;
            ");
        } else {
            DB::unprepared("
            ");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("
        DROP VIEW resum_sede_operativo_etapa_1234_documento;
        DROP VIEW resum_sede_operativo_etapa4_documento;
        DROP VIEW resum_sede_operativo_etapa3_documento;
        DROP VIEW resum_sede_operativo_etapa2_documento;
        DROP VIEW resum_sede_operativo_etapa1_documento;
        ");
    }
}
