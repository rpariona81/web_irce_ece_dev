<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',50);
            $table->string('descripcion',200)->nullable();
            $table->integer('estado')->default(1);            
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();            
            $table->integer('deleted_by')->nullable();  
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable(); 
        });

        DB::table('t_roles')->insert(array('nombre'=>'Administrador','descripcion'=>'El que podrá modificar todo desde el inicio','estado'=>'1'));
        DB::table('t_roles')->insert(array('nombre'=>'Usuario','descripcion'=>'Solo podra guardar los registros','estado'=>'1'));
        DB::table('t_roles')->insert(array('nombre'=>'Supervisor','descripcion'=>'Supervisa','estado'=>'1'));
        DB::table('t_roles')->insert(array('nombre'=>'Registrador','descripcion'=>'Registra','estado'=>'1'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_roles');
    }
}
