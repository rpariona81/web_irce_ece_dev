<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewProductividadPersonaFecha extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
            CREATE VIEW detalle_productividad_operativo_sede_persona as 
            SELECT t1.operativo,
            CASE 
            WHEN operativo=1 THEN 'ECE' 
            WHEN operativo=2 THEN 'EM'
            ELSE NULL END AS operativo_nombre,
            t3.id as sede_id,t3.cod_sede_regional,t3.sede_regional,t3.cod_sede_provincial,t3.sede_provincial,t3.cod_sede_distrital,t3.sede_distrital,t2.id as persona_id,t2.nombres,t1.etapa,
            CASE 
            WHEN t1.etapa=1 THEN 'Inventario y Recepción' 
            WHEN t1.etapa=2 THEN 'Revisión y Consistencia' 
            WHEN t1.etapa=3 THEN 'Control de Calidad' 
            WHEN t1.etapa=4 THEN 'Embalado' 
            ELSE NULL END AS etapa_nombre,
            t1.fecha_registro, t5.tipo, t5.subtipo, t5.orden, t5.etiqueta, t4.p_01,t4.p_02,t4.p_obs 
            FROM t_dig_documento t1
            LEFT JOIN t_dig_persona t2 on t1.persona_id=t2.id 
            LEFT JOIN t_cod_sede t3 on t1.sede_id=t3.id 
            LEFT JOIN t_det_dig_documento t4 on t1.id=t4.cabecera_id
            LEFT JOIN t_cod_documento t5 on t4.documento_id=t5.id 
            WHERE t2.estado=1
            ");
        } else {
            DB::unprepared("
                    ");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("drop view detalle_productividad_operativo_sede_persona");
    }
}
