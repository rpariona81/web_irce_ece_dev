<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpVerAvanceSedeRegional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
    CREATE PROCEDURE sp_ver_avance_regional(IN sp_operativo INT) NO SQL 
    BEGIN 
    IF (sp_operativo = '') THEN 
        BEGIN
            SELECT  
                NULL AS row_num,
                NULL AS id,
                NULL AS cod_sede_regional,
                'TOTAL' AS sede_regional,
                NULL AS operativo,
                NULL AS operativo_nombre,
                FORMAT(SUM(IFNULL(t0.p0_01_tot,0)),'Currency') AS p0_01_tot,
                            FORMAT(SUM(IFNULL(t0.p0_02_tot,0)),'Currency') AS p0_02_tot,
                CONCAT(FORMAT(if(SUM(IFNULL(t0.p0_02_tot,0))=0,0,(SUM(t0.p0_02_tot)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p0_02_porc, 
                            FORMAT(SUM(IFNULL(t2.p1_01,0)),'Currency') AS p1_01,
                CONCAT(FORMAT(if(SUM(IFNULL(t2.p1_01,0))=0,0,(SUM(t2.p1_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p1_01_avance, 
                            FORMAT(SUM(IFNULL(t2.p1_02,0)),'Currency') AS p1_02,
                CONCAT(FORMAT(if(SUM(IFNULL(t2.p1_02,0))=0,0,(SUM(t2.p1_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p1_02_avance, 
                            FORMAT(SUM(IFNULL(t4.p2_01,0)),'Currency') AS p2_01,
                CONCAT(FORMAT(if(SUM(IFNULL(t4.p2_01,0))=0,0,(SUM(t4.p2_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p2_01_avance, 
                            FORMAT(SUM(IFNULL(t4.p2_02,0)),'Currency') AS p2_02,
                CONCAT(FORMAT(if(SUM(IFNULL(t4.p2_02,0))=0,0,(SUM(t4.p2_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p2_02_avance, 
                            FORMAT(SUM(IFNULL(t5.p3_01,0)),'Currency') AS p3_01,
                CONCAT(FORMAT(if(SUM(IFNULL(t5.p3_01,0))=0,0,(SUM(t5.p3_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p3_01_avance, 
                            FORMAT(SUM(IFNULL(t5.p3_02,0)),'Currency') AS p3_02,
                CONCAT(FORMAT(if(SUM(IFNULL(t5.p3_02,0))=0,0,(SUM(t5.p3_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p3_02_avance, 
                FORMAT(SUM(IFNULL(t6.p4_01,0)),'Currency') AS p4_01,
                CONCAT(FORMAT(if(SUM(IFNULL(t6.p4_01,0))=0,0,(SUM(t6.p4_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p4_01_avance, 
                            FORMAT(SUM(IFNULL(t6.p4_02,0)),'Currency') AS p4_02,
                CONCAT(FORMAT(if(SUM(IFNULL(t6.p4_02,0))=0,0,(SUM(t6.p4_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p4_02_avance 
                from t_cod_sede t1 
                left join resum_marco t0 on t1.id=t0.sede_id 
                left join resum_sede_operativo_etapa1 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
                left join resum_sede_operativo_etapa2 t4 on t4.sede_id=t2.sede_id and t0.operativo_marco=t4.operativo 
                left join resum_sede_operativo_etapa3 t5 on t5.sede_id=t2.sede_id and t0.operativo_marco=t5.operativo 
                left join resum_sede_operativo_etapa4 t6 on t6.sede_id=t2.sede_id and t0.operativo_marco=t6.operativo 
            UNION ALL 
            SELECT row_number() over (order by A.id) AS row_num, id, 
                cod_sede_regional, sede_regional, operativo, operativo_nombre, p0_01_tot, p0_02_tot, p0_02_porc, p1_01, p1_01_avance, p1_02, p1_02_avance, p2_01, p2_01_avance, p2_02, p2_02_avance, p3_01, p3_01_avance, p3_02, p3_02_avance, p4_01, p4_01_avance, p4_02, p4_02_avance 
                FROM (
                    SELECT  
                        t1.id,
                        t1.cod_sede_regional,
                        t1.sede_regional,
                        t0.operativo_marco as operativo,
                        CASE 
                        WHEN t0.operativo_marco=1 THEN 'ECE' 
                        WHEN t0.operativo_marco=2 THEN 'EM' 
                        ELSE NULL 
                        END
                        AS operativo_nombre,
                        FORMAT(SUM(IFNULL(t0.p0_01_tot,0)),'Currency') AS p0_01_tot,
                                    FORMAT(SUM(IFNULL(t0.p0_02_tot,0)),'Currency') AS p0_02_tot,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t0.p0_02_tot,0))=0,0,(SUM(t0.p0_02_tot)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p0_02_porc,
                                    FORMAT(SUM(IFNULL(t2.p1_01,0)),'Currency') AS p1_01,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t2.p1_01,0))=0,0,(SUM(t2.p1_01)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p1_01_avance,
                        FORMAT(SUM(IFNULL(t2.p1_02,0)),'Currency') AS p1_02,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t2.p1_02,0))=0,0,(SUM(t2.p1_02)/SUM(t0.p0_02_tot))*100.0),2),'%') AS p1_02_avance,
                                    FORMAT(SUM(IFNULL(t4.p2_01,0)),'Currency') AS p2_01,
                        CONCAT(FORMAT(if(SUM(IFNULL(t4.p2_01,0))=0,0,(SUM(t4.p2_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p2_01_avance, 
                                    FORMAT(SUM(IFNULL(t4.p2_02,0)),'Currency') AS p2_02,
                        CONCAT(FORMAT(if(SUM(IFNULL(t4.p2_02,0))=0,0,(SUM(t4.p2_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p2_02_avance, 
                                    FORMAT(SUM(IFNULL(t5.p3_01,0)),'Currency') AS p3_01,
                        CONCAT(FORMAT(if(SUM(IFNULL(t5.p3_01,0))=0,0,(SUM(t5.p3_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p3_01_avance, 
                                    FORMAT(SUM(IFNULL(t5.p3_02,0)),'Currency') AS p3_02,
                        CONCAT(FORMAT(if(SUM(IFNULL(t5.p3_02,0))=0,0,(SUM(t5.p3_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p3_02_avance, 
                                    FORMAT(SUM(IFNULL(t6.p4_01,0)),'Currency') AS p4_01,
                        CONCAT(FORMAT(if(SUM(IFNULL(t6.p4_01,0))=0,0,(SUM(t6.p4_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p4_01_avance, 
                                    FORMAT(SUM(IFNULL(t6.p4_02,0)),'Currency') AS p4_02,
                        CONCAT(FORMAT(if(SUM(IFNULL(t6.p4_02,0))=0,0,(SUM(t6.p4_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p4_02_avance 
                        from t_cod_sede t1 
                        left join resum_marco t0 on t1.id=t0.sede_id 
                        left join resum_sede_operativo_etapa1 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
                        left join resum_sede_operativo_etapa2 t4 on t4.sede_id=t2.sede_id and t0.operativo_marco=t4.operativo 
                        left join resum_sede_operativo_etapa3 t5 on t5.sede_id=t2.sede_id and t0.operativo_marco=t5.operativo 
                        left join resum_sede_operativo_etapa4 t6 on t6.sede_id=t2.sede_id and t0.operativo_marco=t6.operativo 
                        GROUP BY t1.cod_sede_regional, t1.sede_regional, t0.operativo_marco, operativo_nombre) A 
                        order by id;
            END;
        ELSEIF (sp_operativo IS NOT NULL) THEN 
        BEGIN
            SELECT  
                NULL AS row_num,
                NULL AS id,
                NULL AS cod_sede_regional,
                'TOTAL' AS sede_regional,
                NULL AS operativo,
                NULL AS operativo_nombre,
                FORMAT(SUM(IFNULL(t0.p0_01_tot,0)),'Currency') AS p0_01_tot,
                            FORMAT(SUM(IFNULL(t0.p0_02_tot,0)),'Currency') AS p0_02_tot,
                CONCAT(FORMAT(if(SUM(IFNULL(t0.p0_02_tot,0))=0,0,(SUM(t0.p0_02_tot)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p0_02_porc, 
                            FORMAT(SUM(IFNULL(t2.p1_01,0)),'Currency') AS p1_01,
                CONCAT(FORMAT(if(SUM(IFNULL(t2.p1_01,0))=0,0,(SUM(t2.p1_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p1_01_avance, 
                            FORMAT(SUM(IFNULL(t2.p1_02,0)),'Currency') AS p1_02,
                CONCAT(FORMAT(if(SUM(IFNULL(t2.p1_02,0))=0,0,(SUM(t2.p1_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p1_02_avance, 
                            FORMAT(SUM(IFNULL(t4.p2_01,0)),'Currency') AS p2_01,
                CONCAT(FORMAT(if(SUM(IFNULL(t4.p2_01,0))=0,0,(SUM(t4.p2_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p2_01_avance, 
                            FORMAT(SUM(IFNULL(t4.p2_02,0)),'Currency') AS p2_02,
                CONCAT(FORMAT(if(SUM(IFNULL(t4.p2_02,0))=0,0,(SUM(t4.p2_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p2_02_avance, 
                            FORMAT(SUM(IFNULL(t5.p3_01,0)),'Currency') AS p3_01,
                CONCAT(FORMAT(if(SUM(IFNULL(t5.p3_01,0))=0,0,(SUM(t5.p3_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p3_01_avance, 
                            FORMAT(SUM(IFNULL(t5.p3_02,0)),'Currency') AS p3_02,
                CONCAT(FORMAT(if(SUM(IFNULL(t5.p3_02,0))=0,0,(SUM(t5.p3_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p3_02_avance, 
                FORMAT(SUM(IFNULL(t6.p4_01,0)),'Currency') AS p4_01,
                CONCAT(FORMAT(if(SUM(IFNULL(t6.p4_01,0))=0,0,(SUM(t6.p4_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p4_01_avance, 
                            FORMAT(SUM(IFNULL(t6.p4_02,0)),'Currency') AS p4_02,
                CONCAT(FORMAT(if(SUM(IFNULL(t6.p4_02,0))=0,0,(SUM(t6.p4_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p4_02_avance 
                from t_cod_sede t1 
                left join resum_marco t0 on t1.id=t0.sede_id 
                left join resum_sede_operativo_etapa1 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
                left join resum_sede_operativo_etapa2 t4 on t4.sede_id=t2.sede_id and t0.operativo_marco=t4.operativo 
                left join resum_sede_operativo_etapa3 t5 on t5.sede_id=t2.sede_id and t0.operativo_marco=t5.operativo 
                left join resum_sede_operativo_etapa4 t6 on t6.sede_id=t2.sede_id and t0.operativo_marco=t6.operativo 
                WHERE t0.operativo_marco = sp_operativo 
            UNION ALL 
            SELECT row_number() over (order by A.id) AS row_num, id, 
                cod_sede_regional, sede_regional, operativo, operativo_nombre, p0_01_tot, p0_02_tot, p0_02_porc, p1_01, p1_01_avance, p1_02, p1_02_avance, p2_01, p2_01_avance, p2_02, p2_02_avance, p3_01, p3_01_avance, p3_02, p3_02_avance, p4_01, p4_01_avance, p4_02, p4_02_avance 
                FROM (
                    SELECT  
                        t1.id,
                        t1.cod_sede_regional,
                        t1.sede_regional,
                        t0.operativo_marco as operativo,
                        CASE 
                        WHEN t0.operativo_marco=1 THEN 'ECE' 
                        WHEN t0.operativo_marco=2 THEN 'EM' 
                        ELSE NULL 
                        END
                        AS operativo_nombre,
                        FORMAT(SUM(IFNULL(t0.p0_01_tot,0)),'Currency') AS p0_01_tot,
                                    FORMAT(SUM(IFNULL(t0.p0_02_tot,0)),'Currency') AS p0_02_tot,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t0.p0_02_tot,0))=0,0,(SUM(t0.p0_02_tot)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p0_02_porc,
                                    FORMAT(SUM(IFNULL(t2.p1_01,0)),'Currency') AS p1_01,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t2.p1_01,0))=0,0,(SUM(t2.p1_01)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p1_01_avance,
                        FORMAT(SUM(IFNULL(t2.p1_02,0)),'Currency') AS p1_02,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t2.p1_02,0))=0,0,(SUM(t2.p1_02)/SUM(t0.p0_02_tot))*100.0),2),'%') AS p1_02_avance,
                                    FORMAT(SUM(IFNULL(t4.p2_01,0)),'Currency') AS p2_01,
                        CONCAT(FORMAT(if(SUM(IFNULL(t4.p2_01,0))=0,0,(SUM(t4.p2_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p2_01_avance, 
                                    FORMAT(SUM(IFNULL(t4.p2_02,0)),'Currency') AS p2_02,
                        CONCAT(FORMAT(if(SUM(IFNULL(t4.p2_02,0))=0,0,(SUM(t4.p2_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p2_02_avance, 
                                    FORMAT(SUM(IFNULL(t5.p3_01,0)),'Currency') AS p3_01,
                        CONCAT(FORMAT(if(SUM(IFNULL(t5.p3_01,0))=0,0,(SUM(t5.p3_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p3_01_avance, 
                                    FORMAT(SUM(IFNULL(t5.p3_02,0)),'Currency') AS p3_02,
                        CONCAT(FORMAT(if(SUM(IFNULL(t5.p3_02,0))=0,0,(SUM(t5.p3_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p3_02_avance, 
                                    FORMAT(SUM(IFNULL(t6.p4_01,0)),'Currency') AS p4_01,
                        CONCAT(FORMAT(if(SUM(IFNULL(t6.p4_01,0))=0,0,(SUM(t6.p4_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p4_01_avance, 
                                    FORMAT(SUM(IFNULL(t6.p4_02,0)),'Currency') AS p4_02,
                        CONCAT(FORMAT(if(SUM(IFNULL(t6.p4_02,0))=0,0,(SUM(t6.p4_02)/SUM(t0.p0_02_tot))*100.0),2),'%') as p4_02_avance 
                        from t_cod_sede t1 
                        left join resum_marco t0 on t1.id=t0.sede_id 
                        left join resum_sede_operativo_etapa1 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
                        left join resum_sede_operativo_etapa2 t4 on t4.sede_id=t2.sede_id and t0.operativo_marco=t4.operativo 
                        left join resum_sede_operativo_etapa3 t5 on t5.sede_id=t2.sede_id and t0.operativo_marco=t5.operativo 
                        left join resum_sede_operativo_etapa4 t6 on t6.sede_id=t2.sede_id and t0.operativo_marco=t6.operativo 
                        WHERE t0.operativo_marco = sp_operativo 
                        GROUP BY t1.cod_sede_regional, t1.sede_regional, t0.operativo_marco, operativo_nombre) A 
                        order by id;
                END;
            END IF;
        END");
        }else{

        };
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("drop procedure sp_ver_avance_regional");
    }
}
