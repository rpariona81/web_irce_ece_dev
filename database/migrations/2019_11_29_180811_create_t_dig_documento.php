<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTDigDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_dig_documento', function (Blueprint $table) {
            $table->bigincrements('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('sede_id')->unsigned();
            $table->integer('etapa');
            $table->integer('operativo');
            $table->date('fecha_registro');
            $table->string('observacion_etapa',2000)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->boolean('estado')->default(1);
            $table->timestamps();
            $table->foreign('sede_id')->references('id')->on('t_cod_sede');
            $table->foreign('persona_id')->references('id')->on('t_dig_persona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_dig_documento');
    }
}
