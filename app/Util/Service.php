<?php
namespace App\Util;


//use Illuminate\Support\Facades\DB;

use JWTAuth;
use App\Menu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\ErrorSystem;

class Service
{
    public function __construct()
    {
        //--
    }   

    public static function consumirWebService($ruta, $token, $post = 0, $json = null){
       
        if($post==0){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $ruta);
            curl_setopt(
                $ch, CURLOPT_HTTPHEADER, array(
                'Authorization: ' . $token,
                )
            );
            curl_setopt($ch, CURLOPT_POST, $post);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $respuesta  = curl_exec($ch);
            curl_close($ch);  
        }
        else{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $ruta);
            curl_setopt(
                $ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: ' . $token,
                )
            );
            curl_setopt($ch, CURLOPT_POST, $post);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $respuesta  = curl_exec($ch);
    
            curl_close($ch);  
        }
       

        return $respuesta;  

    }

    public static function getStoreGeneral($store,$parameters)
    {       
        if(config('database.default')=='mysql'){                              
            $stringImplode = implode(",", $parameters);
            return DB::select("CALL $store ($stringImplode)");    
        }
        else{//SQLSERVER 
            $vars = '';
            foreach ($parameters as $count) {
                $vars .= ' ?,';
            }
            $vars = rtrim($vars,",");
            return DB::select("$store $vars", $parameters);  
        }       
           
    }

    
    public static function makeMenu( $pag_seleccionada)
    {
        $rol = Auth::user()->rol->first();
        if($rol){
            return self::prepareMenu(Menu::porRol($rol->id), null, null, $pag_seleccionada);
        }else{

        }
    }

    protected static function prepareMenu($menus, $parentid = null, $indice = null, $pag_seleccionada = null)
    {
        $response = '';
        $li_item  = '';
        $num_orden = [];

        foreach ($menus as $menu) {
            if(!$menu->status) {
                continue;
            }
            if ($menu->padre_id != $parentid) {
                continue;
            }

            $esPadre = $menu->hijos->count() > 0;

            $link = $esPadre ? '#' :  asset($menu->link);

            $esLinkHijo = $parentid ? '' :'side-nav-link' ;
            $li_item = '  <li class="side-nav-item">  ';

            if($link == $pag_seleccionada) {
                $a_titulo = "<a href='{$link}' class='{$esLinkHijo} active'>";
            }else{
                $a_titulo = "<a href='{$link}' class='{$esLinkHijo}'>";
            }

            if(isset($menu->icono) && $menu->icono != '') {
                $i_icono  = "<i class='dripicons-gear' ></i>";
            } else {
                $i_icono = '';
            }

            if($menu->padre_id === null) {
                $titulo   = " <span class='menu-item-parent' style='font-size: 14px'>{$menu->titulo}</span>";
            } else {
                $titulo = self::generarIndice($menu, $indice) .'<span class="menu-titulo">'. $menu->titulo.'</span>';
                //$orden = self::generarIndice($array_orden);
            }

            //$i_popover = "&nbsp;&nbsp;<i class='fa fa-comment-o' data-toggle='popover' data-trigger='hover' data-container='body' data-html='true' title='' data-content='{$menu->descripcion}'></i>";
            if($esPadre) {
                $a_titulo .= $i_icono . $titulo . "<span class='menu-arrow'></span></a>";
            } else {
                $a_titulo .= $i_icono . $titulo . "</a>";
            }

            // JUNTANDO EL MENU CREADO
            $li_item .= $a_titulo;

            if($esPadre) {
                $li_item .= '  <ul class="side-nav-second-level" aria-expanded="false">' . self::prepareMenu($menus->sortBy('orden'), $menu->id, $menu->indice, $pag_seleccionada) . '</ul>  ';
            }
            // CERRAR MENU
            $response .= $li_item . '</li>  ';
        }
        //dump($menu->padre);
        return $response;
    }

    private static function generarIndice($menu, $indice)
    {
        if(in_array($menu->padre_id, [5,7,8])) {
            $indice         .= num_romanos($menu->orden);
            $menu->indice   = $menu->orden.'.';
        } else {
            $indice         .= $menu->orden;
            $menu->indice   = $indice.'.';
        }

        return '<span class="menu-indice">'.$indice.'.&nbsp;</span>';
        //return $indice.'. ';
    }

    public static function registrarError($empresa,$persona,$tabla,$accion,$accion_det,$descripcion)
    {
        $error = new ErrorSystem;
        $error->id_empresa = $empresa;
        $error->id_persona = $persona;
        $error->tabla = $tabla;
        $error->accion = $accion;
        $error->accion_det = $accion_det;
        $error->descripcion = $descripcion;
        $error->save();

    }

    public static function prepararMenuJson(){

        $rol = JWTAuth::parseToken()->authenticate()->id_rol;        
        if($rol > 1 ){            
            $menus_x_rol = Menu::menuxRol($rol)->sortBy('orden');
        }
        else {
            $menus_x_rol = Menu::select(['id','id_padre','titulo','icono','link','orden'])->where('estado',1)->get()->sortBy('orden');
        }
    
        return self::prepararMenu($menus_x_rol,null,0);
    }

    public static  function prepararMenu($menus,$id_padre=null,$nivel=0){       
        $array =[];
        foreach($menus as $menu){
            if($menu->id_padre != $id_padre) continue;

            $hijos =[];
            if($menu->hijos->count() > 0){
                $hijos = self::prepararMenu($menus,$menu->id,$nivel+1) ; 
            }

            $menu_array = array(
                'id'=>$menu->id,
                'nivel'=>$nivel,
                'titulo'=>$menu->titulo,
                'icono'=>$menu->icono,
                'link'=>$menu->link,
                'orden'=>$menu->orden,                 
                'hijos'=>$hijos,
            );
            array_push($array,$menu_array);
        }
        return $array;

    }

   

    
}