<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewGraficasAvanceEtapas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
            CREATE VIEW grafico_operativo1_avance_etapas AS 
            SELECT 
            t0.operativo_marco, t1.etapa, 
            CASE 
            WHEN t1.etapa=1 THEN 'Inventario y Recepción' 
            WHEN t1.etapa=2 THEN 'Revisión y Consistencia' 
            WHEN t1.etapa=3 THEN 'Control de Calidad' 
            WHEN t1.etapa=4 THEN 'Inventario Final' 
            ELSE NULL END AS etapa_nombre, 
            p_01_marco_tot, SUM(t2.p_01) AS p_01_avance_tot, 
            FORMAT(IF(IFNULL(SUM(t2.p_01),0)=0,0,(SUM(t2.p_01)/p_01_marco_tot))*100.0,2) AS p_01_porc_avance, 
            FORMAT(IF(IFNULL(p_01_marco_tot,0)=0,0,(p_01_marco_tot/p_01_marco_tot))*100.0,2) AS p_01_porc_marco, 
            FORMAT(IF(IFNULL(SUM(t2.p_01),0)=0,0,(1-(SUM(t2.p_01)/p_01_marco_tot)))*100.0,2) AS p_01_porc_restante 
            FROM (SELECT operativo_marco, SUM(p_01_marco) as p_01_marco_tot FROM t_dig_marco GROUP BY operativo_marco) t0 
            INNER JOIN t_dig_documento t1 ON t0.operativo_marco = t1.operativo 
            LEFT JOIN t_det_dig_documento t2 ON t1.id = t2.cabecera_id 
            WHERE t0.operativo_marco = 1 
            GROUP BY t0.operativo_marco, t1.etapa;

            CREATE VIEW grafico_operativo2_avance_etapas AS 
            SELECT 
            t0.operativo_marco, t1.etapa, 
            CASE 
            WHEN t1.etapa=1 THEN 'Inventario y Recepción' 
            WHEN t1.etapa=2 THEN 'Revisión y Consistencia' 
            WHEN t1.etapa=3 THEN 'Control de Calidad' 
            WHEN t1.etapa=4 THEN 'Inventario Final' 
            ELSE NULL END AS etapa_nombre, 
            p_01_marco_tot, SUM(t2.p_01) AS p_01_avance_tot, 
            FORMAT(IF(IFNULL(SUM(t2.p_01),0)=0,0,(SUM(t2.p_01)/p_01_marco_tot))*100.0,2) AS p_01_porc_avance, 
            FORMAT(IF(IFNULL(p_01_marco_tot,0)=0,0,(p_01_marco_tot/p_01_marco_tot))*100.0,2) AS p_01_porc_marco, 
            FORMAT(IF(IFNULL(SUM(t2.p_01),0)=0,0,(1-(SUM(t2.p_01)/p_01_marco_tot)))*100.0,2) AS p_01_porc_restante 
            FROM (SELECT operativo_marco, SUM(p_01_marco) as p_01_marco_tot FROM t_dig_marco GROUP BY operativo_marco) t0 
            INNER JOIN t_dig_documento t1 ON t0.operativo_marco = t1.operativo 
            LEFT JOIN t_det_dig_documento t2 ON t1.id = t2.cabecera_id 
            WHERE t0.operativo_marco = 2 
            GROUP BY t0.operativo_marco, t1.etapa");
        }else{
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("
        DROP VIEW grafico_operativo2_avance_etapas;
        DROP VIEW grafico_operativo1_avance_etapas;
        ");
    }
}
