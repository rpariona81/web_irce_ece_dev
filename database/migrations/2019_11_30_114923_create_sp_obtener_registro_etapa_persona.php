<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpObtenerRegistroEtapaPersona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
            CREATE PROCEDURE sp_obtener_registro_etapa_persona (IN sp_cabecera INT) NO SQL
            BEGIN
            declare EXISTE INT DEFAULT 0;
            SET EXISTE = (select count(t1.id) from t_dig_documento t1 where t1.id = sp_cabecera);
			IF (EXISTE > 0)  then
            	BEGIN
                select
                t2.id AS id,
                t3.id AS documento_id,
                t3.tipo AS tipo,
                t3.subtipo AS subtipo,
                t3.orden AS orden,
                t3.etiqueta AS etiqueta,
                t2.p_01 AS p_01,
                t2.p_02 AS p_02,
                t2.p_obs AS p_obs
                from t_dig_documento t1 
                left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
                left join t_cod_documento t3 on t2.documento_id = t3.id 
                where t1.id = sp_cabecera;
                END;
			ELSE
				BEGIN
					SELECT 
					NULL AS id,
					a1.id AS documento_id,
					a1.tipo AS tipo,
					a1.subtipo AS subtipo,
					a1.orden AS orden,
					a1.etiqueta AS etiqueta,
					NULL AS p_01,
					NULL AS p_02,
					NULL AS p_obs
					FROM t_cod_documento a1;
				END;
            END IF;
            END
            ");
        } else {
            DB::unprepared("
            CREATE PROCEDURE sp_obtener_registro_etapa_persona(@sp_cabecera INT) 
            as 
            BEGIN 
			declare @EXISTE INT;
			SET @EXISTE = (select count(t1.id) from t_dig_documento t1 where t1.id = @sp_cabecera)
			IF (@EXISTE > 0)  
				begin
					(select
					t2.id AS id,
					t3.id AS documento_id,
					t3.tipo AS tipo,
					t3.subtipo AS subtipo,
					t3.orden AS orden,
					t3.etiqueta AS etiqueta,
					t2.p_01 AS p_01,
					t2.p_02 AS p_02,
					t2.p_obs AS p_obs
					from t_dig_documento t1 
					left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
					left join t_cod_documento t3 on t2.documento_id = t3.id 
					where t1.id = @sp_cabecera) 
				end
			ELSE
				begin
					SELECT 
					NULL AS id,
					a1.id AS documento_id,
					a1.tipo AS tipo,
					a1.subtipo AS subtipo,
					a1.orden AS orden,
					a1.etiqueta AS etiqueta,
					NULL AS p_01,
					NULL AS p_02,
					NULL AS p_obs
					FROM t_cod_documento a1
				end
            END
            ");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("drop procedure sp_obtener_registro_etapa_persona");
    }
}
