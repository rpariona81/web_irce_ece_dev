<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpVerAvanceSedeRegionalOperativoTipdoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            CREATE PROCEDURE sp_ver_avance_sede_regional_operativo_tipdoc(IN sp_operativo INT, IN sp_tipo_documento INT) NO SQL 
            BEGIN
            SELECT  
                                NULL AS row_num,
                                NULL AS id,
                                NULL AS cod_sede_regional,
                                'TOTAL' AS sede_regional,
                                FORMAT(SUM(IFNULL(t0.p0_01_tot,0)),'Currency') AS p0_01_tot,
                                FORMAT(SUM(IFNULL(t0.p0_02_tot,0)),'Currency') AS p0_02_tot,
                                CONCAT(FORMAT(if(SUM(IFNULL(t0.p0_02_tot,0))=0,0,(SUM(t0.p0_02_tot)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p0_02_porc,
                                FORMAT(SUM(IFNULL(t2.p1_01,0)),'Currency') AS p1_01,
                                CONCAT(FORMAT(if(SUM(IFNULL(t2.p1_01,0))=0,0,(SUM(t2.p1_01)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p1_01_avance,
                                FORMAT(SUM(IFNULL(t2.p1_02,0)),'Currency') AS p1_02,
                                CONCAT(FORMAT(if(SUM(IFNULL(t2.p1_02,0))=0,0,(SUM(t2.p1_02)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p1_02_avance,
                                FORMAT(SUM(IFNULL(t4.p2_01,0)),'Currency') AS p2_01,
                                CONCAT(FORMAT(if(SUM(IFNULL(t4.p2_01,0))=0,0,(SUM(t4.p2_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p2_01_avance, 
                                FORMAT(SUM(IFNULL(t4.p2_02,0)),'Currency') AS p2_02,
                                CONCAT(FORMAT(if(SUM(IFNULL(t4.p2_02,0))=0,0,(SUM(t4.p2_02)/SUM(t0.p0_01_tot))*100.0),2),'%') as p2_02_avance, 
                                FORMAT(SUM(IFNULL(t5.p3_01,0)),'Currency') AS p3_01,
                                CONCAT(FORMAT(if(SUM(IFNULL(t5.p3_01,0))=0,0,(SUM(t5.p3_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p3_01_avance, 
                                FORMAT(SUM(IFNULL(t5.p3_02,0)),'Currency') AS p3_02,
                                CONCAT(FORMAT(if(SUM(IFNULL(t5.p3_02,0))=0,0,(SUM(t5.p3_02)/SUM(t0.p0_01_tot))*100.0),2),'%') as p3_02_avance, 
                                FORMAT(SUM(IFNULL(t6.p4_01,0)),'Currency') AS p4_01,
                                CONCAT(FORMAT(if(SUM(IFNULL(t6.p4_01,0))=0,0,(SUM(t6.p4_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p4_01_avance, 
                                FORMAT(SUM(IFNULL(t6.p4_02,0)),'Currency') AS p4_02,
                                CONCAT(FORMAT(if(SUM(IFNULL(t6.p4_02,0))=0,0,(SUM(t6.p4_02)/SUM(t0.p0_01_tot))*100.0),2),'%') as p4_02_avance 
                            from t_cod_sede t1 
                                left join (
                                    SELECT m1.sede_id, m1.operativo_marco, SUM(m1.p_01_marco) AS p0_01_tot, 0 AS p0_02_tot 
                                    from t_dig_marco m1 left join t_cod_documento m2 on m2.id=m1.documento_id
                                    WHERE if(sp_operativo=0,true,m1.operativo_marco=sp_operativo) and if(sp_tipo_documento=0,true,m2.tipo=sp_tipo_documento)
                                    GROUP BY m1.sede_id
                                ) t0 on t0.sede_id=t1.id
                                left join (
                                    select a2.sede_id, a2.operativo, a3.tipo, SUM(a1.p_01) AS p1_01, SUM(a1.p_02) AS p1_02 
                                    from t_det_dig_documento a1
                                    left join t_dig_documento a2 on a2.id=a1.cabecera_id
                                    left join t_cod_documento a3 on a3.id=a1.documento_id
                                    WHERE a2.etapa=1 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                                    GROUP BY a2.sede_id
                                ) t2 on t2.sede_id=t1.id
                                left join (
                                    select a2.sede_id, a2.operativo, a3.tipo, SUM(a1.p_01) AS p2_01, SUM(a1.p_02) AS p2_02 
                                    from t_det_dig_documento a1
                                    left join t_dig_documento a2 on a2.id=a1.cabecera_id
                                    left join t_cod_documento a3 on a3.id=a1.documento_id
                                    WHERE a2.etapa=2 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                                    GROUP BY a2.sede_id
                                ) t4 on t4.sede_id=t2.sede_id
                                left join (
                                    select a2.sede_id, a2.operativo, a3.tipo, SUM(a1.p_01) AS p3_01, SUM(a1.p_02) AS p3_02 
                                    from t_det_dig_documento a1
                                    left join t_dig_documento a2 on a2.id=a1.cabecera_id
                                    left join t_cod_documento a3 on a3.id=a1.documento_id
                                    WHERE a2.etapa=3 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                                    GROUP BY a2.sede_id
                                ) t5 on t5.sede_id=t2.sede_id
                                left join (
                                    select a2.sede_id, a2.operativo, a3.tipo, SUM(a1.p_01) AS p4_01, SUM(a1.p_02) AS p4_02 
                                    from t_det_dig_documento a1
                                    left join t_dig_documento a2 on a2.id=a1.cabecera_id
                                    left join t_cod_documento a3 on a3.id=a1.documento_id
                                    WHERE a2.etapa=4 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                                    GROUP BY a2.sede_id
                                ) t6 on t6.sede_id=t2.sede_id
                            UNION ALL
                            SELECT row_number() over (order by A.id) AS row_num, id, cod_sede_regional, sede_regional, p0_01_tot, p0_02_tot, p0_02_porc, p1_01, p1_01_avance, p1_02, p1_02_avance, p2_01, p2_01_avance, p2_02, p2_02_avance, p3_01, p3_01_avance, p3_02, p3_02_avance, p4_01, p4_01_avance, p4_02, p4_02_avance 
                            FROM (
                                SELECT
                                    t1.id,
                                    t1.cod_sede_regional,
                                    t1.sede_regional,
                                    FORMAT(SUM(IFNULL(t0.p0_01_tot,0)),'Currency') AS p0_01_tot,
                                    FORMAT(SUM(IFNULL(t0.p0_02_tot,0)),'Currency') AS p0_02_tot,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t0.p0_02_tot,0))=0,0,(SUM(t0.p0_02_tot)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p0_02_porc,
                                    FORMAT(SUM(IFNULL(t2.p1_01,0)),'Currency') AS p1_01,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t2.p1_01,0))=0,0,(SUM(t2.p1_01)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p1_01_avance,
                                    FORMAT(SUM(IFNULL(t2.p1_02,0)),'Currency') AS p1_02,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t2.p1_02,0))=0,0,(SUM(t2.p1_02)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p1_02_avance,
                                    FORMAT(SUM(IFNULL(t4.p2_01,0)),'Currency') AS p2_01,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t4.p2_01,0))=0,0,(SUM(t4.p2_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p2_01_avance, 
                                    FORMAT(SUM(IFNULL(t4.p2_02,0)),'Currency') AS p2_02,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t4.p2_02,0))=0,0,(SUM(t4.p2_02)/SUM(t0.p0_01_tot))*100.0),2),'%') as p2_02_avance, 
                                    FORMAT(SUM(IFNULL(t5.p3_01,0)),'Currency') AS p3_01,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t5.p3_01,0))=0,0,(SUM(t5.p3_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p3_01_avance, 
                                    FORMAT(SUM(IFNULL(t5.p3_02,0)),'Currency') AS p3_02,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t5.p3_02,0))=0,0,(SUM(t5.p3_02)/SUM(t0.p0_01_tot))*100.0),2),'%') as p3_02_avance, 
                                    FORMAT(SUM(IFNULL(t6.p4_01,0)),'Currency') AS p4_01,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t6.p4_01,0))=0,0,(SUM(t6.p4_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p4_01_avance, 
                                    FORMAT(SUM(IFNULL(t6.p4_02,0)),'Currency') AS p4_02,
                                    CONCAT(FORMAT(if(SUM(IFNULL(t6.p4_02,0))=0,0,(SUM(t6.p4_02)/SUM(t0.p0_01_tot))*100.0),2),'%') as p4_02_avance 
                                from t_cod_sede t1 
                                    left join (
                                        SELECT m1.sede_id, m1.operativo_marco, SUM(m1.p_01_marco) AS p0_01_tot, 0 AS p0_02_tot 
                                                                    from t_dig_marco m1 left join t_cod_documento m2 on m2.id=m1.documento_id
                                                                    WHERE if(sp_operativo=0,true,m1.operativo_marco=sp_operativo) and if(sp_tipo_documento=0,true,m2.tipo=sp_tipo_documento)
                                                                    GROUP BY m1.sede_id
                                    ) t0 on t0.sede_id=t1.id
                                    left join (
                                        select a2.sede_id, a2.operativo, a3.tipo, SUM(a1.p_01) AS p1_01, SUM(a1.p_02) AS p1_02 
                                        from t_det_dig_documento a1
                                        left join t_dig_documento a2 on a2.id=a1.cabecera_id
                                        left join t_cod_documento a3 on a3.id=a1.documento_id
                                        WHERE a2.etapa=1 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                                        GROUP BY a2.sede_id
                                    ) t2 on t2.sede_id=t1.id
                                    left join (
                                        select a2.sede_id, a2.operativo, a3.tipo, SUM(a1.p_01) AS p2_01, SUM(a1.p_02) AS p2_02 
                                        from t_det_dig_documento a1
                                        left join t_dig_documento a2 on a2.id=a1.cabecera_id
                                        left join t_cod_documento a3 on a3.id=a1.documento_id
                                        WHERE a2.etapa=2 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                                        GROUP BY a2.sede_id
                                    ) t4 on t4.sede_id=t2.sede_id
                                    left join (
                                        select a2.sede_id, a2.operativo, a3.tipo, SUM(a1.p_01) AS p3_01, SUM(a1.p_02) AS p3_02 
                                        from t_det_dig_documento a1
                                        left join t_dig_documento a2 on a2.id=a1.cabecera_id
                                        left join t_cod_documento a3 on a3.id=a1.documento_id
                                        WHERE a2.etapa=3 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                                        GROUP BY a2.sede_id
                                    ) t5 on t5.sede_id=t2.sede_id
                                    left join (
                                        select a2.sede_id, a2.operativo, a3.tipo, SUM(a1.p_01) AS p4_01, SUM(a1.p_02) AS p4_02 
                                        from t_det_dig_documento a1
                                        left join t_dig_documento a2 on a2.id=a1.cabecera_id
                                        left join t_cod_documento a3 on a3.id=a1.documento_id
                                        WHERE a2.etapa=4 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                                        GROUP BY a2.sede_id
                                    ) t6 on t6.sede_id=t2.sede_id
                                GROUP BY t1.cod_sede_regional) A 
                                order by id;
            END;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP PROCEDURE sp_ver_avance_sede_regional_operativo_tipdoc");
    }
}
