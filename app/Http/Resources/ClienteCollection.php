<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClienteCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key)
        {
            $tipo_doc_name = $row->cliente->tipo_cliente->description;
            return [
                'id' => $row->id,                
                "estado" => $row->estado,
                "id_empresa" => $row->id_empresa,
                "id_cliente" => $row->id_cliente,
                "tipo_doc_id" => $row->cliente->tipo_doc_id,
                "tipo_doc" => $tipo_doc_name,                
                "nro_doc_id" => $row->cliente->nro_doc_id,
                "rz" => $row->cliente->rz,
                "ubigeo" => $row->cliente->ubigeo,
                "dom_fiscal" => $row->cliente->dom_fiscal,
                "telefono" => $row->telefono,
                "email" => $row->email,
                "created_at" => $row->created_at->format('Y-m-d H:i:s'),
            ];
        });
    }
    
}
