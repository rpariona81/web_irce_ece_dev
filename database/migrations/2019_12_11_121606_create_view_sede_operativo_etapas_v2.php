<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewSedeOperativoEtapasV2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("        
            CREATE VIEW resum_marco_v2 AS 
            SELECT 
            sede_id,operativo_marco,t2.tipo as tipodoc,
            SUM(p_01_marco) AS p0_01_tot,
            0 AS p0_02_tot 
            from t_dig_marco t1 
			LEFT JOIN t_cod_documento t2 ON t1.documento_id=t2.id 
			GROUP BY t1.sede_id, t1.operativo_marco, t2.tipo;
            
			CREATE VIEW resum_sede_operativo_etapa1_v2 AS
            SELECT 
            t1.sede_id,
            t1.operativo, t3.tipo as tipodoc,
            SUM(t2.p_01) AS p1_01,
            SUM(t2.p_02) AS p1_02 
            from t_dig_documento t1
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
			LEFT JOIN t_cod_documento t3 on t3.id=t2.documento_id 
            where t1.etapa=1 
            group by t1.sede_id, t1.operativo, t3.tipo;

            CREATE VIEW resum_sede_operativo_etapa2_v2 AS
            SELECT 
            t1.sede_id,
            t1.operativo, t3.tipo as tipodoc,
            SUM(t2.p_01) AS p2_01,
            SUM(t2.p_02) AS p2_02 
            from t_dig_documento t1
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
			LEFT JOIN t_cod_documento t3 on t3.id=t2.documento_id 
            where t1.etapa=2 
            group by t1.sede_id, t1.operativo, t3.tipo;
			
            CREATE VIEW resum_sede_operativo_etapa3_v2 AS
            SELECT 
            t1.sede_id,
            t1.operativo, t3.tipo as tipodoc,
            SUM(t2.p_01) AS p3_01,
            SUM(t2.p_02) AS p3_02 
            from t_dig_documento t1
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
			LEFT JOIN t_cod_documento t3 on t3.id=t2.documento_id 
            where t1.etapa=3 
            group by t1.sede_id, t1.operativo, t3.tipo;
			
            CREATE VIEW resum_sede_operativo_etapa4_v2 AS
            SELECT 
            t1.sede_id,
            t1.operativo, t3.tipo as tipodoc,
            SUM(t2.p_01) AS p4_01,
            SUM(t2.p_02) AS p4_02 
            from t_dig_documento t1
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
			LEFT JOIN t_cod_documento t3 on t3.id=t2.documento_id 
            where t1.etapa=4 
            group by t1.sede_id, t1.operativo, t3.tipo;
			
            CREATE VIEW resum_sede_operativo_etapa_1234_v2 as 
            SELECT  
            t1.id as sede_id,
            t1.cod_sede_regional,
            t1.sede_regional,
            t1.cod_sede_provincial,
            t1.sede_provincial,
            t1.cod_sede_distrital,
            t1.sede_distrital,
            t0.operativo_marco as operativo,
            CASE 
            WHEN t0.operativo_marco=1 THEN 'ECE' 
            WHEN t0.operativo_marco=2 THEN 'EM' 
            ELSE NULL 
            END
            AS operativo_nombre,
			t0.tipodoc,
			CASE 
            WHEN t0.tipodoc=1 THEN 'MINEDU' 
            WHEN t0.tipodoc=2 THEN 'INEI' 
            ELSE NULL 
            END
            AS tipodoc_nombre,
            t0.p0_01_tot, 
            0 AS p0_02_tot, 
            IFNULL(t2.p1_01,0) AS p1_01, 
            IFNULL(t2.p1_02,0) AS p1_02, 
            IFNULL(t4.p2_01,0) AS p2_01,
            IFNULL(t4.p2_02,0) AS p2_02, 
            IFNULL(t5.p3_01,0) AS p3_01, 
            IFNULL(t5.p3_02,0) AS p3_02, 
            IFNULL(t6.p4_01,0) AS p4_01,
            IFNULL(t6.p4_02,0) AS p4_02 
            from t_cod_sede t1 
            left join resum_marco_v2 t0 on t1.id=t0.sede_id 
            left join resum_sede_operativo_etapa1_v2 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo AND t0.tipodoc=t2.tipodoc  
            left join resum_sede_operativo_etapa2_v2 t4 on t4.sede_id=t2.sede_id AND t0.operativo_marco=t4.operativo AND t0.tipodoc=t4.tipodoc 
            left join resum_sede_operativo_etapa3_v2 t5 on t5.sede_id=t2.sede_id AND t0.operativo_marco=t5.operativo AND t0.tipodoc=t5.tipodoc 
            left join resum_sede_operativo_etapa4_v2 t6 on t6.sede_id=t2.sede_id AND t0.operativo_marco=t6.operativo AND t0.tipodoc=t6.tipodoc 
            order by t1.id, t0.operativo_marco, t0.tipodoc");
        }else{

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        DB::unprepared("
        DROP VIEW resum_sede_operativo_etapa_1234_v2;
        DROP VIEW resum_sede_operativo_etapa4_v2;
        DROP VIEW resum_sede_operativo_etapa3_v2;
        DROP VIEW resum_sede_operativo_etapa2_v2;
        DROP VIEW resum_sede_operativo_etapa1_v2;
        DROP VIEW resum_marco_v2;
        ");
    }
}
