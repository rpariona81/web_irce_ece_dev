<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;

class PersonaController extends Controller
{
     //llenar el combo
     public function llenarSelectMultiple($descripcion){
        $select = Persona::select(['id','cargo_id','nombres','nro_documento'])
            ->where('estado',1)
            ->where(function($q) use ($descripcion){
                $q->where('nombres','like','%'.$descripcion.'%')
                ->orWhere('nro_documento','like','%'.$descripcion.'%');
              
            })
            ->get();
        return $select;
    }
    //llenar el combo
    public function llenarSelect(){
        $select = Persona::select(['id','nombre'])->where('estado',1)->get();
        return $select;
    }

    //funciones generales de mantenimiento
    public function listar(){
        $lista = Persona::select('id','cargo_id','nombres','nro_documento','doc_identidad','estado','created_at');
        if(request()->has('estado'))
			$lista->where('estado',request('estado'));
        
        
        return response()->json(['resp_listar'=>$lista->get()]);
    }

    public function crear(Request $request){
        $nuevo = new Persona($request->all());        
        $nuevo->save();
        return response()->json(['message' => 'La Persona se registró correctamente']);
    }

    public function modificar(Request $request){
        $editado = Persona::findOrFail($request->id);        
        $editado->update($request->all()); 
        return response()->json(['message' => 'La Persona se modificó correctamente']);
    }

    public function eliminar(Request $request){
        $eliminado = Persona::findOrFail($request->id);
        $eliminado->estado = 9 ;
        $eliminado->save(); 
        return response()->json(['message' => 'La Persona se Eliminó correctamente']);
    }

    public function inactivar(Request $request){
        $inactivado = Persona::findOrFail($request->id);
        $inactivado->estado = 0 ;
        $inactivado->save(); 
        return response()->json(['message' => 'La Persona se Inactivó correctamente']);
    }

    public function activar(Request $request){
        $activado = Persona::findOrFail($request->id);
        $activado->estado = 1;
        $activado->save(); 
        return response()->json(['message' => 'La Persona se Activó correctamente']);
    }
    //fin funciones generales de mantenimiento
}
