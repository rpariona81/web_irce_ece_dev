<?php
namespace App\Util;


//use Illuminate\Support\Facades\DB;

use JWTAuth;
use App\Menu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\TemporalCPE;

class DocumentosAdicionales
{    
    public static function crearDocumento($inputs)
    {       
        $user = JWTAuth::parseToken()->authenticate();        
      
        $temporal = new TemporalCPE();      
        $temporal->user_id = $user->id_persona ;
        $temporal->external_id = (string) Str::random(20);
        $temporal->establishment_id = '';
        $temporal->establishment = '{"country_id":"PE","country":{"id":"PE","description":"PERU"},"department_id":"15","department":{"id":"15","description":"LIMA"},"province_id":"1501","province":{"id":"1501","description":"Lima"},"district_id":"150101","district":{"id":"150101","description":"Lima"},"urbanization":null,"address":"Luis villegas","email":null,"telephone":null,"code":"0000"}' ;
        $temporal->soap_type_id = '01' ;
        $temporal->state_type_id = '01' ;
        $temporal->ubl_version = '2.1' ;
        $temporal->group_id = '03' ;
        $temporal->document_type_id = '91';
        $temporal->series = $inputs['serie_documento'];
       /* $temporal->number = ;
        $temporal->date_of_issue = ;
        $temporal->time_of_issue = ;
        $temporal->customer_id = ;
        $temporal->customer = ;
        $temporal->currency_type_id = ;
        $temporal->purchase_order = ;
        $temporal->exchange_rate_sale = ;
        $temporal->total_prepayment = ;
        $temporal->total_charge = ;
        $temporal->total_discount = ;
        $temporal->total_exportation = ;
        $temporal->total_free = ;
        $temporal->total_taxed = ;
        $temporal->total_unaffected = ;
        $temporal->total_exonerated = ;
        $temporal->total_igv = ;
        $temporal->total_base_isc = ;
        $temporal->total_isc = ;
        $temporal->total_base_other_taxes = ;
        $temporal->total_other_taxes = ;
        $temporal->total_taxes = ;
        $temporal->total_value = ;
        $temporal->total = ;
        $temporal->charges = ;
        $temporal->discounts = ;
        $temporal->prepayments = ;
        $temporal->guides = ;
        $temporal->related = ;
        $temporal->perception = ;
        $temporal->detraction = ;
        $temporal->legends = ;
        $temporal->additional_information = ;
        $temporal->filename = ;
        $temporal->hash = ;
        $temporal->qr = ;
        $temporal->has_xml = ;
        $temporal->has_pdf = ;
        $temporal->has_cdr = ;
         */
        return $temporal;
        $temporal->save();
    }

    
       
}