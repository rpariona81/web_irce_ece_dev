<?php
namespace App\Util;

use App;

class Funciones
{
    public function __construct()
    {
        //--
    }  

    public static function crearCodBarrasAuto($model,$empresa,$cod_producto,$almacen,$stock){
        $codigo_unico = Funciones::generarCodigoInternoV2($model,'id_empresa',$empresa,'cod_producto',$cod_producto,'#');
        for ($i=0; $i < $stock; $i++) { 
            $alm_prod_unico = new $model;
                $alm_prod_unico->id_almacen_producto = $almacen;
                $alm_prod_unico->id_empresa = $empresa;
                $alm_prod_unico->cod_producto = $cod_producto;
                $alm_prod_unico->cod_interno = $codigo_unico + $i;
            $alm_prod_unico->save();
        } 
    } 
    public static function generarCodigoInterno($model,$empresa,$codigo_interno){       
        if($codigo_interno=='#'){
                $codigo_interno = $model::select(['cod_interno'])
                    ->where('id_empresa',$empresa)                                        
                    ->orderBy('cod_interno', 'desc')
                    ->first();
            if($codigo_interno) 
                 return $codigo_interno->cod_interno + 1;//aumentamos mas uno
            else
                 return 1;
         }
         return $codigo_interno;
    }

    public static function generarCodigoInternoV2($model,$col1,$val1,$col2,$val2,$codigo_interno){       
    if($codigo_interno=='#'){
            $codigo_interno = $model::select(['cod_interno'])
                ->where($col1,$val1) 
                ->where($col2,$val2)                     
                ->orderBy('cod_interno', 'desc')
                ->first();
        if($codigo_interno) 
                return $codigo_interno->cod_interno + 1;//aumentamos mas uno
        else
                return 1;
        }
        return $codigo_interno;
    }

    public static  function generarNumeroComprobante($model,$empresa,$documento,$serie,$numero){       
       if($numero=='#'){
            $documento = $model::select(['numero'])
                    ->where('id_empresa',$empresa)
                    ->where('tipo_documento',$documento)
                    ->where('serie',$serie)
                    ->orderBy('numero', 'desc')
                    ->first();
            if($documento) 
                return $documento->numero + 1;//aumentamos mas uno
            else
                return 1;
        }
        return $numero;
    }

    public static  function crearExternalID($model,$campo,$carateres){    
        $valor = str_random($carateres);   //aleatorio        
        $existe = $model::where($campo)->first();
        if($existe){
            return crearExternalID($model);
        }
        return $valor;
 
    }

    public static  function validarMovimientoAlmacenes($tipo_movimiento, $origen, $destino){    
        
        if($tipo_movimiento=='I'){
            if($origen){
                return 0;
            }
            if(!$destino){
                return 0;
            }
        }
        elseif($tipo_movimiento=='S'){
            if($destino){
                return 0;
            }
            if(!$origen){
                return 0;
            }
        }
        elseif($tipo_movimiento=='C'){
            if(!$origen){
                return 0;
            }
            if(!$destino){
                return 0;
            }
        }
        return 1; 
    }

    public static  function registrarMovimientoProducto($empresa,$tipo_movimiento,$almacen_origen,$almacen_destino, $cp_prod, $id_movimiento){    
        $id_producto = null;       
        if($tipo_movimiento=='I'){ 
            
           $id_producto = Funciones::aumentarStockAlmacen($empresa,$almacen_destino,$cp_prod);
            
        }               
        elseif($tipo_movimiento=='S'){
            if(!Funciones::descontarStockAlmacen($empresa,$almacen_origen,$cp_prod)){
                return 0;
            }
            
        }                 
        elseif($tipo_movimiento=='C') {
            //$producto_origen = App\AlmacenProducto::where('id_almacen',$almacen_origen)->where('id_producto',$cp_prod['id_item'] )->first();
            
            if(!Funciones::descontarStockAlmacen($empresa,$almacen_origen,$cp_prod)){
                return 0;
            }
            $id_producto = Funciones::aumentarStockAlmacen($empresa,$almacen_destino,$cp_prod);

            /* if($producto_origen){//vemos si existe en el almacén   
                $id_producto = $producto_origen->id_producto;                             
                $cantidad = $producto_origen->cantidad - $cp_prod['cantidad'];                         
                $producto_origen->update(['cantidad' => $cantidad]);
                //ahora ingresamos
                $producto_destino = App\AlmacenProducto::where('id_almacen',$almacen_destino)->where('id_producto',$cp_prod['id_item'] )->first();          
                if($producto_destino){//vemos si existe en el almacén   
                    $id_producto = $producto_destino->id_producto;                             
                    $cantidad = $producto_destino->cantidad + $cp_prod['cantidad'];                         
                    $producto_destino->update(['cantidad' => $cantidad]);
                    
                }
                else{//creamos el almacen_producto en el almacen si no existe
                    $almacen_producto = new App\AlmacenProducto();
                        $almacen_producto->id_almacen = $almacen_destino;
                        $almacen_producto->id_producto = $cp_prod['id_item'];
                        $almacen_producto->cantidad = $cp_prod['cantidad'];
                        $almacen_producto->minimo = 10;//default
                    $almacen_producto->save();               
                    $id_producto= $almacen_producto->id_producto;
                }
            }
            else{
                return 0;
            }  */ 
        }   
               
        //luego guardamos el movimiento
        $detalle = new App\MovimientoDetalle();     
            $detalle->id_movimiento = $id_movimiento;
            $detalle->id_producto = $id_producto;
            $detalle->descripcion = $cp_prod['descripcion'];
            $detalle->tipo_afectacion_igv = $cp_prod['codigo_tipo_afectacion_igv'];
            $detalle->codigo = $cp_prod['codigo_interno'];
            $detalle->uni_medida = $cp_prod['unidad_de_medida'];
            $detalle->cantidad = $cp_prod['cantidad'];
            $detalle->valor_unitario = $cp_prod['valor_unitario'];
            $detalle->valor_venta = $cp_prod['total_valor_item'];
            $detalle->precio_unitario = $cp_prod['precio_unitario'];
            $detalle->precio_venta = $cp_prod['total_item'];
            $detalle->igv_item = $cp_prod['total_igv'];
        $detalle->save();

        return 1;
        
    }

    public static function aumentarStockAlmacen($empresa,$almacen_destino,$cp_prod){
        $id_producto = null;
     
        $almacen_producto = App\AlmacenProducto::where('id_almacen',$almacen_destino)->where('id_producto',$cp_prod['id_item'] )->first();          
            if($almacen_producto){//vemos si existe en el almacén 
                $id_producto = $almacen_producto->id_producto;                             
                $cantidad = $almacen_producto->cantidad + $cp_prod['cantidad'];                         
                $almacen_producto->update(['cantidad' => $cantidad]);

            }
            else{//creamos el nuevo almacenproducto en el almacen si no existe
                $almacen_producto = new App\AlmacenProducto();
                    $almacen_producto->id_almacen = $almacen_destino;
                    $almacen_producto->id_producto = $cp_prod['id_item'];
                    $almacen_producto->cantidad = $cp_prod['cantidad'];
                    $almacen_producto->minimo = 10;//default
                $almacen_producto->save();               
                $id_producto= $almacen_producto->id_producto;
            }

            if($cp_prod['codigo_unico']){//creamos los códigos únicos para los productos
                Funciones::crearCodBarrasAuto(App\AlmacenProductoUnico::class,$empresa,$cp_prod['cod_interno'],$almacen_producto->id,$cp_prod['cantidad']);
            }
        return $id_producto;
    }

    public static function descontarStockAlmacen($empresa,$almacen_origen,$cp_prod){
        $almacen_producto = App\AlmacenProducto::where('id_almacen',$almacen_origen)->where('id_producto',$cp_prod['id_item'] )->first();          
            if($almacen_producto){//vemos si existe en el almacén  
                if($almacen_producto->cantidad < $cp_prod['cantidad']){
                    return 0;
                } 
                else{
                    $id_producto = $almacen_producto->id_producto;                             
                    $cantidad = $almacen_producto->cantidad - $cp_prod['cantidad'];                         
                    $almacen_producto->update(['cantidad' => $cantidad]);
                    if($almacen_producto->codigo_unico){//cambiamos el estado del código único a vendido
                        $unico_prod = App\AlmacenProductoUnico::find($almacen_producto->id_producto_unico);
                        if($unico_prod){                                                
                            $unico_prod->update(['estado' => 2]);//vendido
                        }
                        else {
                            return 0;
                        }
                    }
                }

            }
            else{
                return 0;
            }   
            return 1;
    }
 
    public static  function codificarURL($data){    
        $url = '';
        $array = $data;
        foreach ($array as $key => $value) {
            $url .= ($value) ? '&' . $key . '=' . urlencode($value) : '';          
         }       
        return $url;
 
    }
     

    
}