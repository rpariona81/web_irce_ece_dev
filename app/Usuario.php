<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use Notifiable;

    protected $table = 't_usuarios';

    protected $primaryKey = 'id';

    protected $fillable = [      
        'id_rol',
        'email',
        'estado',
        'nombres',
        'cargo',
        'avatar',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    

   /*  protected $casts = [
        'email_verified_at' => 'datetime',
    ]; */
    
    public function rol(){
        return $this->belongsTo('App\Rol','id_rol');
    }

}
