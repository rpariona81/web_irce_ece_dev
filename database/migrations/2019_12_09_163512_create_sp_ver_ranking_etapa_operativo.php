<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpVerRankingEtapaOperativo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("        
            CREATE PROCEDURE `ver_ranking_etapa_operativo`(IN sp_operativo INT,IN sp_tipo_documento INT)
            NO SQL
        BEGIN 
        
        set @queryOperativo = ('');
        set @queryTipoDocumento = ('');
        
        IF sp_operativo <> 0 THEN
            set @queryOperativo = CONCAT(' and a.operativo = ', sp_operativo );
        END IF;
        
        IF sp_tipo_documento <> 0 THEN
            set @queryTipoDocumento = CONCAT(' and c.tipo= ', sp_tipo_documento );
        END IF;
        
        set @queryString = CONCAT('
                                (SELECT 
                    NULL AS row_num_etapa1, ''Total'' as nombres_etapa1, SUM(P.p_01_persona) as p_01_persona_etapa1,
                                NULL AS row_num_etapa2, ''Total'' as nombres_etapa2, SUM(Q.p_01_persona) as p_01_persona_etapa2,
                                NULL AS row_num_etapa3, ''Total'' as nombres_etapa3, SUM(R.p_01_persona) as p_01_persona_etapa3,
                                NULL AS row_num_etapa4, ''Total'' as nombres_etapa4, SUM(S.p_01_persona) as p_01_persona_etapa4 
                            FROM 
                            (
                            SELECT ROW_NUMBER() OVER (ORDER BY p_01_persona DESC) AS row_num,nombres,p_01_persona FROM
                            (SELECT t2.id, t2.nombres, t4.p_01_persona FROM t_dig_persona t2 
                            LEFT JOIN (SELECT a.persona_id, SUM(b.p_01) as p_01_persona FROM t_dig_documento a LEFT JOIN t_det_dig_documento b ON a.id=b.cabecera_id
                                                LEFT JOIN t_cod_documento c ON b.documento_id = c.id
                            WHERE a.etapa=1 ',@queryOperativo,' ',@queryTipoDocumento,' GROUP BY a.persona_id) t4 ON t2.id=t4.persona_id
                            WHERE t2.estado=1) A) P  
                            LEFT JOIN 
                            (SELECT ROW_NUMBER() OVER (ORDER BY p_01_persona DESC) AS row_num,nombres,p_01_persona FROM
                            (SELECT t2.id, t2.nombres, t4.p_01_persona FROM t_dig_persona t2 
                            LEFT JOIN (SELECT a.persona_id, SUM(b.p_01) as p_01_persona FROM t_dig_documento a LEFT JOIN t_det_dig_documento b ON a.id=b.cabecera_id 
                                                LEFT JOIN t_cod_documento c ON b.documento_id = c.id
                            WHERE a.etapa=2 ',@queryOperativo,' ',@queryTipoDocumento,'   GROUP BY a.persona_id) t4 ON t2.id=t4.persona_id
                            WHERE t2.estado=1) B) Q ON P.row_num=Q.row_num 
                            LEFT JOIN 
                            (SELECT ROW_NUMBER() OVER (ORDER BY p_01_persona DESC) AS row_num,nombres,p_01_persona FROM
                            (SELECT t2.id, t2.nombres, t4.p_01_persona FROM t_dig_persona t2 
                            LEFT JOIN (SELECT a.persona_id, SUM(b.p_01) as p_01_persona FROM t_dig_documento a LEFT JOIN t_det_dig_documento b ON a.id=b.cabecera_id 
                                                LEFT JOIN t_cod_documento c ON b.documento_id = c.id
                            WHERE a.etapa=3 ',@queryOperativo,' ',@queryTipoDocumento,'  GROUP BY a.persona_id) t4 ON t2.id=t4.persona_id
                            WHERE t2.estado=1) A) R ON P.row_num=R.row_num 
                            LEFT JOIN 
                            (SELECT ROW_NUMBER() OVER (ORDER BY p_01_persona DESC) AS row_num,nombres,p_01_persona FROM
                            (SELECT t2.id, t2.nombres, t4.p_01_persona FROM t_dig_persona t2 
                            LEFT JOIN (SELECT a.persona_id, SUM(b.p_01) as p_01_persona FROM t_dig_documento a LEFT JOIN t_det_dig_documento b ON a.id=b.cabecera_id 
                                                LEFT JOIN t_cod_documento c ON b.documento_id = c.id
                            WHERE a.etapa=4 ',@queryOperativo,' ',@queryTipoDocumento,'  GROUP BY a.persona_id) t4 ON t2.id=t4.persona_id
                            WHERE t2.estado=1) A) S ON P.row_num=S.row_num 
                            ORDER BY P.row_num ASC)
                                                UNION ALL
                                                (SELECT 
                    P.row_num AS row_num_etapa1, P.nombres as nombres_etapa1, CASE WHEN P.p_01_persona IS NULL THEN 0 ELSE P.p_01_persona END as p_01_persona_etapa1,
                                Q.row_num AS row_num_etapa2, Q.nombres as nombres_etapa2, CASE WHEN Q.p_01_persona IS NULL THEN 0 ELSE Q.p_01_persona END as p_01_persona_etapa2,
                                R.row_num AS row_num_etapa3, R.nombres as nombres_etapa3, CASE WHEN R.p_01_persona IS NULL THEN 0 ELSE R.p_01_persona END as p_01_persona_etapa3,
                                S.row_num AS row_num_etapa4, S.nombres as nombres_etapa4, CASE WHEN S.p_01_persona IS NULL THEN 0 ELSE S.p_01_persona END as p_01_persona_etapa4 
                            FROM 
                            (
                            SELECT ROW_NUMBER() OVER (ORDER BY p_01_persona DESC) AS row_num,nombres,p_01_persona FROM
                            (SELECT t2.id, t2.nombres, t4.p_01_persona FROM t_dig_persona t2 
                            LEFT JOIN (SELECT a.persona_id, SUM(b.p_01) as p_01_persona FROM t_dig_documento a LEFT JOIN t_det_dig_documento b ON a.id=b.cabecera_id
                                                LEFT JOIN t_cod_documento c ON b.documento_id = c.id
                            WHERE a.etapa=1 ',@queryOperativo,' ',@queryTipoDocumento,' GROUP BY a.persona_id) t4 ON t2.id=t4.persona_id
                            WHERE t2.estado=1) A) P  
                            LEFT JOIN 
                            (SELECT ROW_NUMBER() OVER (ORDER BY p_01_persona DESC) AS row_num,nombres,p_01_persona FROM
                            (SELECT t2.id, t2.nombres, t4.p_01_persona FROM t_dig_persona t2 
                            LEFT JOIN (SELECT a.persona_id, SUM(b.p_01) as p_01_persona FROM t_dig_documento a LEFT JOIN t_det_dig_documento b ON a.id=b.cabecera_id 
                                                LEFT JOIN t_cod_documento c ON b.documento_id = c.id
                            WHERE a.etapa=2 ',@queryOperativo,' ',@queryTipoDocumento,'   GROUP BY a.persona_id) t4 ON t2.id=t4.persona_id
                            WHERE t2.estado=1) B) Q ON P.row_num=Q.row_num 
                            LEFT JOIN 
                            (SELECT ROW_NUMBER() OVER (ORDER BY p_01_persona DESC) AS row_num,nombres,p_01_persona FROM
                            (SELECT t2.id, t2.nombres, t4.p_01_persona FROM t_dig_persona t2 
                            LEFT JOIN (SELECT a.persona_id, SUM(b.p_01) as p_01_persona FROM t_dig_documento a LEFT JOIN t_det_dig_documento b ON a.id=b.cabecera_id 
                                                LEFT JOIN t_cod_documento c ON b.documento_id = c.id
                            WHERE a.etapa=3 ',@queryOperativo,' ',@queryTipoDocumento,'  GROUP BY a.persona_id) t4 ON t2.id=t4.persona_id
                            WHERE t2.estado=1) A) R ON P.row_num=R.row_num 
                            LEFT JOIN 
                            (SELECT ROW_NUMBER() OVER (ORDER BY p_01_persona DESC) AS row_num,nombres,p_01_persona FROM
                            (SELECT t2.id, t2.nombres, t4.p_01_persona FROM t_dig_persona t2 
                            LEFT JOIN (SELECT a.persona_id, SUM(b.p_01) as p_01_persona FROM t_dig_documento a LEFT JOIN t_det_dig_documento b ON a.id=b.cabecera_id 
                                                LEFT JOIN t_cod_documento c ON b.documento_id = c.id
                            WHERE a.etapa=4 ',@queryOperativo,' ',@queryTipoDocumento,'  GROUP BY a.persona_id) t4 ON t2.id=t4.persona_id
                            WHERE t2.estado=1) A) S ON P.row_num=S.row_num 
                            ORDER BY P.row_num ASC) ORDER BY row_num_etapa1;');
        
        PREPARE myquery FROM @queryString;
        EXECUTE myquery;
        
        END      
        
        
            ");
        }else{
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP PROCEDURE ver_ranking_etapa_operativo");
    }
}
