<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordReset extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    protected $user;
    protected $token;

    public function __construct($user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/password_reset_'.$this->token);

        return (new MailMessage)
                    ->from('no-reply@facturalo.com', 'no-reply')
                    ->subject('Reestablecer Contraseña')
                    ->greeting('!Hola!')
                    ->line('Hemos recibido su solicitud para Recuperar su contraseña')
                    ->line('Haga clic en el siguiente enlace para restablecer su contraseña.')
                    ->action('Reestablecer Contraseña', $url)
                    ->line('Si no ha solicitado restablecer su contraseña, ignore este mensaje.')
                    ->line('¡Gacias!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
