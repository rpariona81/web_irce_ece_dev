<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTMarco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_dig_marco', function (Blueprint $table) {
            $table->bigincrements('id');
            $table->integer('sede_id')->unsigned();
            $table->integer('documento_id')->unsigned();
            $table->integer('operativo_marco');
            $table->date('fecha_registro_marco')->nullable();
            $table->integer('p_folder_marco')->nullable();
            $table->integer('p_01_marco')->nullable();
            $table->integer('p_02_marco')->nullable();
            $table->string('p_obs_marco',2000)->nullable();
            $table->integer('p_validacion')->nullable();
            $table->boolean('estado')->default(1);
            $table->timestamps();
            $table->unique(['sede_id','documento_id','operativo_marco'],'unico_sede_doc_oper');
            $table->foreign('sede_id')->references('id')->on('t_cod_sede');
            $table->foreign('documento_id')->references('id')->on('t_cod_documento');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_dig_marco');
    }
}
