<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProveedorCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key)
        {
            $tipo_doc_name = $row->proveedor->tipo_cliente->description;
            return [
                'id' => $row->id,                
                "estado" => $row->estado,
                "id_empresa" => $row->id_empresa,
                "id_proveedor" => $row->id_proveedor,
                "tipo_doc_id" => $row->proveedor->tipo_doc_id,
                "tipo_doc" => $tipo_doc_name,                
                "nro_doc_id" => $row->proveedor->nro_doc_id,
                "rz" => $row->proveedor->rz,
                "ubigeo" => $row->proveedor->ubigeo,
                "dom_fiscal" => $row->proveedor->dom_fiscal,
                "telefono" => $row->telefono,
                "email" => $row->email,
                "created_at" => $row->created_at->format('Y-m-d H:i:s'),
            ];
        });
    }
    
}
