<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\Notifications\Activation;
use App\Notifications\Activated;
use App\Notifications\PasswordReset;
use App\Notifications\PasswordResetted;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Util\Funciones;

use App\Usuario;

class AuthController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => '¡Credenciales incorrectas! Por favor inténtelo nuevamente.'], 422);
            }
        } catch (JWTException $e) {
            return response()->json(['message' => '¡Ocurrió un error. Inténtelo nuevamente!'], 500);
        }

        $user = Auth::user();

        if($user->estado == 2)
            return response()->json(['message' => 'Tu cuenta esta inhabilitado. Por favor verificar su Correo Electrónico.'], 422);

        if($user->estado == 9)
            return response()->json(['message' => 'Tu cuenta esta eliminada. Por favor contacte con el Administrador.'], 422);
        

        return response()->json(['message' => 'Inicio de Sesión Correcto!','token' => $token]);
    }

    public function getAuthUser(){
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response()->json(['authenticated' => false],422);
        }

        $user = JWTAuth::parseToken()->authenticate();
        $social_auth = ($user->password) ? 0 : 1;

        return response()->json(compact('user','social_auth'));
    }

    public function check()
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (JWTException $e) {
            return response(['authenticated' => false]);
        }

        return response(['authenticated' => true]);
    }

    public function logout()
    {

        try {
            $token = JWTAuth::getToken();

            if ($token) {
                JWTAuth::invalidate($token);
            }

        } catch (JWTException $e) {
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(['message' => '¡Cierre de sesión correcto!']);
    }

    public function register(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password'
        ]);

        if($validation->fails())
            return response()->json(['message' => $validation->messages()->first()],422);

        $user = \App\Usuario::create([
            'email' => request('email'),
            'status' => 'pending_activation',
            'password' => bcrypt(request('password'))
        ]);

        $user->activation_token = generateUuid();
        $user->save();
        $profile = new \App\Profile;
        $profile->first_name = request('first_name');
        $profile->last_name = request('last_name');
        $user->profile()->save($profile);

        $user->notify(new Activation($user));

        return response()->json(['message' => 'You have registered successfully. Please check your email for activation!']);
    }

    public function activate($activation_token){
        $user = \App\Usuario::whereActivationToken($activation_token)->first();

        if(!$user)
            return response()->json(['message' => 'Invalid activation token!'],422);

        if($user->status == 'activated')
            return response()->json(['message' => 'Your account has already been activated!'],422);

        if($user->status != 'pending_activation')
            return response()->json(['message' => 'Invalid activation token!'],422);

        $user->status = 'activated';
        $user->save();
        $user->notify(new Activated($user));

        return response()->json(['message' => 'Your account has been activated!']);
    }

    public function password(Request $request){

        $validation = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if($validation->fails())
            return response()->json(['message' => $validation->messages()->first()],422);

        $user = \App\Usuario::whereEmail(request('email'))->first();

        if(!$user)
            return response()->json(['message' => 'No pudimos encontrar el correo ingresado. ¡Inténtelo denuevo mas tarde!'],422);

        $token = generateUuid();
        \DB::table('password_resets')->insert([
            'email' => request('email'),
            'token' => $token
        ]);
        $user->notify(new PasswordReset($user,$token));

        return response()->json(['message' => 'Enviamos el recordatorio a su correo electrónico . ¡Por favor revise su bandeja de entrada!']);
    }

    public function validatePasswordReset(Request $request){
        $validate_password_request = \DB::table('password_resets')->where('token','=',request('token'))->first();

        if(!$validate_password_request)
            return response()->json(['message' => '¡El Token de reestablecimiento de contraseña es incorrecto!'],422);

        if(date("Y-m-d H:i:s", strtotime($validate_password_request->created_at . "+30 minutes")) < date('Y-m-d H:i:s'))
            return response()->json(['message' => 'El token para reestablecer su contraseña ha expirado. ¡Por favor inténtelo nuevamente!'],422);

        return response()->json(['message' => '']);
    }

    public function reset(Request $request){

        $validation = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password'
        ]);

        if($validation->fails())
            return response()->json(['message' => $validation->messages()->first()],422);

        $user = \App\Usuario::whereEmail(request('email'))->first();

        if(!$user)
            return response()->json(['message' => 'No pudimos encontrar ningún usuario con este correo electrónico. ¡Inténtalo de nuevamente!'],422);

        $validate_password_request = \DB::table('password_resets')->where('email','=',request('email'))->where('token','=',request('token'))->first();

        if(!$validate_password_request)
            return response()->json(['message' => '¡Token de reestablecimiento de contraseña incorrecto!'],422);

        if(date("Y-m-d H:i:s", strtotime($validate_password_request->created_at . "+30 minutes")) < date('Y-m-d H:i:s'))
            return response()->json(['message' => 'El token para reestablecer su contraseña ha expirado. ¡Por favor inténtelo nuevamente!'],422);

        $user->password = bcrypt(request('password'));
        $user->save();

        $user->notify(new PasswordResetted($user));

        return response()->json(['message' => 'Tu contraseña ha sido modifica correctamente. ¡Por favor vuelva a iniciar sesión!']);
    }

    public function changePassword(Request $request){
        if(env('IS_DEMO'))
            return response()->json(['message' => 'You are not allowed to perform this action in this mode.'],422);

        $validation = Validator::make($request->all(),[
            'current_password' => 'required',
            'new_password' => 'required|confirmed|different:current_password|min:6',
            'new_password_confirmation' => 'required|same:new_password'
        ]);

        if($validation->fails())
            return response()->json(['message' => $validation->messages()->first()],422);

        $user = JWTAuth::parseToken()->authenticate();

        if(!\Hash::check(request('current_password'),$user->password))
            return response()->json(['message' => 'Old password does not match! Please try again!'],422);

        $user->password = bcrypt(request('new_password'));
        $user->save();

        return response()->json(['message' => 'Your password has been changed successfully!']);
    }

    public function registrarCuentaFree(Request $request){

        

        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password'
        ]);
       
        if($validation->fails())
            return response()->json(['message' => $validation->messages()->first()],422);

        try { 
        
            DB::beginTransaction();

            //registramos en el API por el momento registra local deberia registrar por un API xd
            $user = new \App\Models\User();//buscacmos el
            $user->name =  $request->input('name');
            $user->email = $request->input('email');
            $user->api_token = Funciones::crearExternalID(\App\Models\User::class,'email', 50);
            $user->password = bcrypt('123456789');
            $user->role = 'user';
            $user->save();

            //registramos a la persona
            $persona = new \App\Persona();       
            $persona->nombres = $request->input('name');
            $persona->save();
            
            //registramos com empresa

            $empresa = new \App\Empresa();        
            $empresa->ruc = ($request->input('ruc')=='') ? '10000000001' : $request->input('ruc') ;
            $empresa->rz =  $request->input('name');
            $empresa->dom_fiscal = 'Domicilio Fiscal';
            $empresa->ubigeo = '150101';
            $empresa->token = $user->api_token;
            $empresa->produccion = 0;
            $empresa->url_ws = "http://facturappweb.com"; //la ruta provisional
            $empresa->f_ini_contrato = date('Y-m-d');
            $empresa->f_fin_contrato = date('Y-m-d',strtotime(date('Y-m-d')."+ 10 days")) ;
            $empresa->save();

            //registramos al Usuario

            $usuario = new \App\Usuario();       
            $usuario->id_persona = $persona->id; 
            $usuario->id_rol = 9;//POSTERIORMENTE LO DEFINIMOS BIEN QUE ROLES SERAN 
            $usuario->id_empresa = $empresa->id;
            $usuario->email = $request->input('email');
            $usuario->password = bcrypt($request->input('password'));
            $usuario->save();
         
            DB::commit();
            return response()->json(['message' => 'La cuenta de prueba fue registrado correctamente']);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Tuvimos un problema inténtelo nuevamente']);
        }


    }


}
