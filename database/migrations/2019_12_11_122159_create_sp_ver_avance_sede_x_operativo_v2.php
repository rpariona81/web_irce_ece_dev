<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpVerAvanceSedeXOperativoV2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
            CREATE PROCEDURE sp_ver_avance_sede_operativo_tipodoc(IN sp_region CHAR(3), IN sp_provincia CHAR(2), IN sp_distrito CHAR(2), IN sp_operativo INT, IN sp_tipodoc INT) NO SQL 
            BEGIN
            SET @queryOperativo = ('');
            SET @queryTipoDoc = ('');
            SET @queryRegion = ('');
            SET @queryProvincia = ('');
            SET @queryDistrito = ('');
            SET @queryAND = ('');
            SET @queryWHERE = ('');
            SET @queryPARAMS = 0;
            IF sp_operativo <> 0 THEN
                SET @queryOperativo = CONCAT(' operativo = ', sp_operativo, ' ');
                SET @queryPARAMS = @queryPARAMS + 1;
            END IF;
            IF sp_tipodoc <> 0 THEN
                SET @queryTipoDoc = CONCAT(' tipodoc = ', sp_tipodoc, ' ');
                SET @queryPARAMS = @queryPARAMS + 1;
            END IF;
            IF sp_region <> '' THEN
                SET @queryRegion = CONCAT(' cod_sede_regional = ''', sp_region, '''');
                SET @queryPARAMS = @queryPARAMS + 1;
            END IF;
            IF sp_provincia <> '' THEN
                SET @queryProvincia = CONCAT(' cod_sede_provincial = ''', sp_provincia, '''');
                SET @queryPARAMS = @queryPARAMS + 1;
            END IF;
            IF sp_distrito <> '' THEN
                SET @queryDistrito = CONCAT(' cod_sede_distrital = ''', sp_distrito, '''');
                SET @queryPARAMS = @queryPARAMS + 2;
            END IF;
            IF @queryPARAMS > 0 THEN
                SET @queryWHERE = (' WHERE ');
            END IF;
            IF @queryPARAMS > 0 THEN
                SET @queryAND = (' AND ');
            END IF;
            SET @queryString = CONCAT('SELECT 
					NULL AS row_num,
					NULL AS sede_id,
					NULL AS cod_sede_regional,
					''TOTAL'' AS sede_regional,
					NULL AS cod_sede_provincial,
					NULL AS sede_provincial,
					NULL AS cod_sede_distrital,
					NULL AS sede_distrital,
					NULL AS operativo,
					NULL AS tipodoc,
					FORMAT(IFNULL(SUM(p0_01_tot),0),''currency'') AS p0_01_tot,
					0 as p0_02_tot,
					''0.00%'' AS p0_02_porc, 
					FORMAT(IFNULL(SUM(p1_01),0),''currency'') AS p1_01,
					CONCAT(FORMAT((IFNULL(SUM(p1_01),0)/SUM(p0_01_tot))*100,2),''%'') AS p1_01_avance, 
					FORMAT(IFNULL(SUM(p1_02),0),''currency'') AS p1_02, 
					CONCAT(FORMAT((IFNULL(SUM(p1_02),0)/SUM(p0_01_tot))*100,2),''%'') AS p1_02_avance,
					FORMAT(IFNULL(SUM(p2_01),0),''currency'') AS p2_01,
					CONCAT(FORMAT((IFNULL(SUM(p2_01),0)/SUM(p0_01_tot))*100,2),''%'') AS p2_01_avance, 
					FORMAT(IFNULL(SUM(p2_02),0),''currency'') AS p2_02, 
					CONCAT(FORMAT((IFNULL(SUM(p2_02),0)/SUM(p0_01_tot))*100,2),''%'') AS p2_02_avance, 
					FORMAT(IFNULL(SUM(p3_01),0),''currency'') AS p3_01,
					CONCAT(FORMAT((IFNULL(SUM(p3_01),0)/SUM(p0_01_tot))*100,2),''%'') AS p3_01_avance, 
					FORMAT(IFNULL(SUM(p3_02),0),''currency'') AS p3_02, 
					CONCAT(FORMAT((IFNULL(SUM(p3_02),0)/SUM(p0_01_tot))*100,2),''%'') AS p3_02_avance, 
					FORMAT(IFNULL(SUM(p4_01),0),''currency'') AS p4_01,
					CONCAT(FORMAT((IFNULL(SUM(p4_01),0)/SUM(p0_01_tot))*100,2),''%'') AS p4_01_avance, 
					FORMAT(IFNULL(SUM(p4_02),0),''currency'') AS p4_02, 
					CONCAT(FORMAT((IFNULL(SUM(p4_02),0)/SUM(p0_01_tot))*100,2),''%'') AS p4_02_avance 
					FROM resum_sede_operativo_etapa_1234_v2 WHERE ', 
					IF(sp_operativo=0,TRUE,@queryOperativo), @queryAND, 
					IF(sp_tipoDoc=0,TRUE,@queryTipoDoc), @queryAND,
					IF(sp_region='',TRUE,@queryRegion), @queryAND,
					IF(sp_provincia='',TRUE,@queryProvincia), @queryAND,
					IF(sp_distrito='',TRUE,@queryDistrito),
					'  UNION ALL 
					SELECT ROW_NUMBER() OVER (ORDER BY sede_id ASC) AS row_num,
					sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,tipodoc,
					p0_01_tot,
					0 as p0_02_tot,
					''0.00%'' AS p0_02_porc, 
					p1_01,p1_01_avance,p1_02,p1_02_avance,p2_01,p2_01_avance,p2_02,p2_02_avance,p3_01,p3_01_avance,p3_02,p3_02_avance,p4_01,p4_01_avance,p4_02,p4_02_avance FROM 
					(
					SELECT sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,
					AVG(operativo) AS operativo,
					AVG(tipodoc) AS tipodoc,
					FORMAT(IFNULL(SUM(p0_01_tot),0),''currency'') AS p0_01_tot,
					FORMAT(IFNULL(SUM(p1_01),0),''currency'') AS p1_01,
					CONCAT(FORMAT((IFNULL(SUM(p1_01),0)/SUM(p0_01_tot))*100,2),''%'') AS p1_01_avance, 
					FORMAT(IFNULL(SUM(p1_02),0),''currency'') AS p1_02, 
					CONCAT(FORMAT((IFNULL(SUM(p1_02),0)/SUM(p0_01_tot))*100,2),''%'') AS p1_02_avance,
					FORMAT(IFNULL(SUM(p2_01),0),''currency'') AS p2_01,
					CONCAT(FORMAT((IFNULL(SUM(p2_01),0)/SUM(p0_01_tot))*100,2),''%'') AS p2_01_avance, 
					FORMAT(IFNULL(SUM(p2_02),0),''currency'') AS p2_02, 
					CONCAT(FORMAT((IFNULL(SUM(p2_02),0)/SUM(p0_01_tot))*100,2),''%'') AS p2_02_avance, 
					FORMAT(IFNULL(SUM(p3_01),0),''currency'') AS p3_01,
					CONCAT(FORMAT((IFNULL(SUM(p3_01),0)/SUM(p0_01_tot))*100,2),''%'') AS p3_01_avance, 
					FORMAT(IFNULL(SUM(p3_02),0),''currency'') AS p3_02, 
					CONCAT(FORMAT((IFNULL(SUM(p3_02),0)/SUM(p0_01_tot))*100,2),''%'') AS p3_02_avance, 
					FORMAT(IFNULL(SUM(p4_01),0),''currency'') AS p4_01,
					CONCAT(FORMAT((IFNULL(SUM(p4_01),0)/SUM(p0_01_tot))*100,2),''%'') AS p4_01_avance, 
					FORMAT(IFNULL(SUM(p4_02),0),''currency'') AS p4_02, 
					CONCAT(FORMAT((IFNULL(SUM(p4_02),0)/SUM(p0_01_tot))*100,2),''%'') AS p4_02_avance 
					FROM resum_sede_operativo_etapa_1234_v2 WHERE ', 
					IF(sp_operativo=0,TRUE,@queryOperativo), @queryAND, 
					IF(sp_tipoDoc=0,TRUE,@queryTipoDoc), @queryAND,
					IF(sp_region='',TRUE,@queryRegion), @queryAND,
					IF(sp_provincia='',TRUE,@queryProvincia), @queryAND,
					IF(sp_distrito='',TRUE,@queryDistrito),
					'    GROUP BY sede_id) A');
				PREPARE myquery FROM @queryString;
                EXECUTE myquery;
            END;
            ");
        }else{

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP PROCEDURE sp_ver_avance_sede_operativo_tipodoc;");
    }
}
