<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpVerAvanceDocumentosOperativo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
			

			CREATE PROCEDURE `sp_ver_avance_documentos_operativo`(IN sp_operativo INT, IN sp_tipo_documento INT)
			NO SQL
		BEGIN 
		
		set @queryOperativOMarco = ('');
		set @queryOperativo = ('');
		set @queryTipoDocumento = ('');
		
		IF sp_operativo <> 0 THEN
		BEGIN
		set @queryOperativo = CONCAT(' and a1.operativo =', sp_operativo );
		set @queryOperativoMarco =  CONCAT(' WHERE t0.operativo_marco = ', sp_operativo );
		END;
		END IF;
		
		IF sp_tipo_documento <> 0 THEN
		set @queryTipoDocumento = CONCAT(' WHERE t1.tipo = ', sp_tipo_documento );
		END IF;
		
		set @queryString = CONCAT('
				(SELECT 		
			  NULL as row_num,	
				''TOTAL'' as documento_id, 
				NULL as tipo, 
				NULL as tipo_nombre,
				NULL as subtipo, 
				NULL as orden, 
				NULL as etiqueta,
				SUM(M.p0_01_tot_doc) as p0_01_tot_doc,
				NULL as p0_01_tot_obs,
				NULL AS p0_01_porc_obs,
				SUM(t2.p1_01_tot_doc) as p1_01_tot_doc,
				CONCAT(FORMAT(if(IFNULL(SUM(t2.p1_01_tot_doc),0)=0,0,(SUM(t2.p1_01_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p1_01_porc_doc, 
				SUM(t2.p1_02_tot_doc) AS p1_02_tot_doc,
				CONCAT(FORMAT(if(IFNULL(SUM(t2.p1_02_tot_doc),0)=0,0,(SUM(t2.p1_02_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p1_02_porc_obs, 
				SUM(t3.p2_01_tot_doc) AS p2_01_tot_doc,
				CONCAT(FORMAT(if(IFNULL(SUM(t3.p2_01_tot_doc),0)=0,0,(SUM(t3.p2_01_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p2_01_porc_doc, 
				SUM(t3.p2_02_tot_doc) AS p2_02_tot_doc,
				CONCAT(FORMAT(if(IFNULL(SUM(t3.p2_02_tot_doc),0)=0,0,(SUM(t3.p2_02_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p2_02_porc_obs, 
				SUM(t4.p3_01_tot_doc) AS p3_01_tot_doc,
				CONCAT(FORMAT(if(IFNULL(SUM(t4.p3_01_tot_doc),0)=0,0,(SUM(t4.p3_01_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p3_01_porc_doc, 
				SUM(t4.p3_02_tot_doc) AS p3_02_tot_doc,
				CONCAT(FORMAT(if(IFNULL(SUM(t4.p3_02_tot_doc),0)=0,0,(SUM(t4.p3_02_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p3_02_porc_obs, 
				SUM(t5.p4_01_tot_doc) AS p4_01_tot_doc,
				CONCAT(FORMAT(if(IFNULL(SUM(t5.p4_01_tot_doc),0)=0,0,(SUM(t5.p4_01_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p4_01_porc_doc, 
				SUM(t5.p4_02_tot_doc) AS p4_02_tot_doc,
				CONCAT(FORMAT(if(IFNULL(SUM(t5.p4_02_tot_doc),0)=0,0,(SUM(t5.p4_02_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p4_02_porc_obs
				FROM t_cod_documento t1 
				INNER JOIN 
					(SELECT t0.documento_id, SUM(t0.p_01_marco) as p0_01_tot_doc FROM t_dig_marco t0 ' , @queryOperativoMarco , ' GROUP BY t0.documento_id ) M ON M.documento_id=t1.id 
				LEFT JOIN 
					(SELECT a1.etapa, a2.documento_id, SUM(a2.p_01) AS p1_01_tot_doc, SUM(a2.p_02) AS p1_02_tot_doc 
					FROM t_dig_documento a1 
					LEFT JOIN t_det_dig_documento a2 ON a1.id=a2.cabecera_id 
					WHERE a1.etapa=1 ' , @queryOperativo ,' GROUP BY a2.documento_id) t2 ON t1.id=t2.documento_id 
				LEFT JOIN 
					(SELECT a1.etapa, a2.documento_id, SUM(a2.p_01) AS p2_01_tot_doc, SUM(a2.p_02) AS p2_02_tot_doc 
					FROM t_dig_documento a1 
					LEFT JOIN t_det_dig_documento a2 ON a1.id=a2.cabecera_id 
					WHERE a1.etapa=2 ' , @queryOperativo , ' GROUP BY a2.documento_id) t3 ON t1.id=t3.documento_id 
				LEFT JOIN 
					(SELECT a1.etapa,  a2.documento_id, SUM(a2.p_01) AS p3_01_tot_doc, SUM(a2.p_02) AS p3_02_tot_doc 
					FROM t_dig_documento a1 
					LEFT JOIN t_det_dig_documento a2 ON a1.id=a2.cabecera_id 
					WHERE a1.etapa=3 ' , @queryOperativo , ' GROUP BY a2.documento_id) t4 ON t1.id=t4.documento_id 
				LEFT JOIN
					(SELECT a1.etapa, a2.documento_id, SUM(a2.p_01) AS p4_01_tot_doc, SUM(a2.p_02) AS p4_02_tot_doc 
					FROM t_dig_documento a1 
					LEFT JOIN t_det_dig_documento a2 ON a1.id=a2.cabecera_id 
					WHERE a1.etapa=4 ', @queryOperativo ,' GROUP BY a2.documento_id) t5 ON t1.id=t5.documento_id
				' , @queryTipoDocumento ,')
				UNION ALL 
				(SELECT 		
			  row_number() over (order by t1.id) AS row_num,	
				t1.id as documento_id, 
				t1.tipo, 
				CASE WHEN tipo = 1 THEN ''MINEDU'' WHEN tipo = 2 THEN ''INEI'' ELSE NULL	END AS tipo_nombre,
				t1.subtipo, 
				t1.orden, 
				t1.etiqueta,
				M.p0_01_tot_doc,
				NULL AS p0_01_tot_obs,
				NULL AS p0_01_porc_obs,
				t2.p1_01_tot_doc,
				CONCAT(FORMAT(if(IFNULL(t2.p1_01_tot_doc,0)=0,0,(t2.p1_01_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p1_01_porc_doc, 
				t2.p1_02_tot_doc,
				CONCAT(FORMAT(if(IFNULL(t2.p1_02_tot_doc,0)=0,0,(t2.p1_02_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p1_02_porc_obs, 
				t3.p2_01_tot_doc,
				CONCAT(FORMAT(if(IFNULL(t3.p2_01_tot_doc,0)=0,0,(t3.p2_01_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p2_01_porc_doc, 
				t3.p2_02_tot_doc,
				CONCAT(FORMAT(if(IFNULL(t3.p2_02_tot_doc,0)=0,0,(t3.p2_02_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p2_02_porc_obs, 
				t4.p3_01_tot_doc,
				CONCAT(FORMAT(if(IFNULL(t4.p3_01_tot_doc,0)=0,0,(t4.p3_01_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p3_01_porc_doc, 
				t4.p3_02_tot_doc,
				CONCAT(FORMAT(if(IFNULL(t4.p3_02_tot_doc,0)=0,0,(t4.p3_02_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p3_02_porc_obs, 
				t5.p4_01_tot_doc,
				CONCAT(FORMAT(if(IFNULL(t5.p4_01_tot_doc,0)=0,0,(t5.p4_01_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p4_01_porc_doc, 
				t5.p4_02_tot_doc,
				CONCAT(FORMAT(if(IFNULL(t5.p4_02_tot_doc,0)=0,0,(t5.p4_02_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p4_02_porc_obs 
				FROM t_cod_documento t1 
				INNER JOIN 
					(SELECT t0.documento_id, SUM(t0.p_01_marco) as p0_01_tot_doc FROM t_dig_marco t0 ' , @queryOperativoMarco , ' GROUP BY t0.documento_id ) M ON M.documento_id=t1.id 
				LEFT JOIN 
					(SELECT a1.etapa, a2.documento_id, SUM(a2.p_01) AS p1_01_tot_doc, SUM(a2.p_02) AS p1_02_tot_doc 
					FROM t_dig_documento a1 
					LEFT JOIN t_det_dig_documento a2 ON a1.id=a2.cabecera_id 
					WHERE a1.etapa=1 ' , @queryOperativo , ' GROUP BY a2.documento_id) t2 ON t1.id=t2.documento_id 
				LEFT JOIN 
					(SELECT a1.etapa, a2.documento_id, SUM(a2.p_01) AS p2_01_tot_doc, SUM(a2.p_02) AS p2_02_tot_doc 
					FROM t_dig_documento a1 
					LEFT JOIN t_det_dig_documento a2 ON a1.id=a2.cabecera_id 
					WHERE a1.etapa=2 ' , @queryOperativo , ' GROUP BY a2.documento_id) t3 ON t1.id=t3.documento_id 
				LEFT JOIN 
					(SELECT a1.etapa,  a2.documento_id, SUM(a2.p_01) AS p3_01_tot_doc, SUM(a2.p_02) AS p3_02_tot_doc 
					FROM t_dig_documento a1 
					LEFT JOIN t_det_dig_documento a2 ON a1.id=a2.cabecera_id 
					WHERE a1.etapa=3 ' , @queryOperativo , ' GROUP BY a2.documento_id) t4 ON t1.id=t4.documento_id 
				LEFT JOIN
					(SELECT a1.etapa, a2.documento_id, SUM(a2.p_01) AS p4_01_tot_doc, SUM(a2.p_02) AS p4_02_tot_doc 
					FROM t_dig_documento a1 
					LEFT JOIN t_det_dig_documento a2 ON a1.id=a2.cabecera_id 
					WHERE a1.etapa=4 ' , @queryOperativo , ' GROUP BY a2.documento_id) t5 ON t1.id=t5.documento_id
				' , @queryTipoDocumento ,');');
				
		PREPARE myquery FROM @queryString;
		EXECUTE myquery;
		
		END
            ");
        } else {
            DB::unprepared("
            ");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("drop procedure sp_ver_avance_documentos_operativo");
    }
}
