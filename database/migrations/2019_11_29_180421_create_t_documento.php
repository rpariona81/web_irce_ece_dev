<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use phpDocumentor\Reflection\Types\Nullable;

class CreateTDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_cod_documento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo')->nullable();
            $table->integer('subtipo')->nullable();
            $table->integer('orden')->nullable();
            $table->string('etiqueta', 200);
        });

        DB::table('t_cod_documento')->insert(array('tipo' => '1', 'subtipo' => '1', 'orden' => '1', 'etiqueta' => 'Registro de Asistencia de la Capacitación del Nivel III (APA)-Sede Regional.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '1', 'subtipo' => '1', 'orden' => '2', 'etiqueta' => 'Registro de Asistencia de la Capacitación del Nivel IV (Aplicadores)-Sede'));
        DB::table('t_cod_documento')->insert(array('tipo' => '1', 'subtipo' => '1', 'orden' => '3', 'etiqueta' => 'Declaración Jurada del Protocolo de prevención de violencia contra las niñas, los niños y adolescentes durante la implementación de las evaluaciones estandarizadas (Aplicadores).'));
        DB::table('t_cod_documento')->insert(array('tipo' => '1', 'subtipo' => '1', 'orden' => '4', 'etiqueta' => 'DOC.ECE.01A Declaración Jurada del Aplicador.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '1', 'subtipo' => '1', 'orden' => '5', 'etiqueta' => 'DOC.ECE.10 Acta de Incidencia (sólo los codificados como A-1, A-2 y B-3) y las Actas de Incidencia de los casos de No Aplicación con códigos de 10 al 14.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '1', 'subtipo' => '1', 'orden' => '6', 'etiqueta' => 'DOC.ECE.11 Registro del Personal de Seguridad e Incidencias del Almacén del Local Provincial/Distrital.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '1', 'subtipo' => '1', 'orden' => '7', 'etiqueta' => 'DOC.ECE.14 Acta de Despacho y Recepción del Material Minedu,'));
        DB::table('t_cod_documento')->insert(array('tipo' => '1', 'subtipo' => '2', 'orden' => '1', 'etiqueta' => 'Fotocopia de la CARÁTULA de la Ficha del Aplicador de Sección (ECE) o FOAR (EM).'));
        DB::table('t_cod_documento')->insert(array('tipo' => '1', 'subtipo' => '2', 'orden' => '2', 'etiqueta' => 'Fotocopia de la CONSTANCIA DE NO APLICACIÓN de la Ficha del Aplicador de Sección (ECE) o FOAR (EM).'));
        DB::table('t_cod_documento')->insert(array('tipo' => '1', 'subtipo' => '2', 'orden' => '3', 'etiqueta' => 'DOC.ECE.09 Acta de No Aplicación.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '1', 'subtipo' => '2', 'orden' => '4', 'etiqueta' => 'Constancia de No Aplicación emitido por la UGEL.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '1', 'subtipo' => '2', 'orden' => '5', 'etiqueta' => 'Fotocopia de Ficha de Verificación de la sección No aplicada.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '2', 'subtipo' => '3', 'orden' => '1', 'etiqueta' => 'DOC.ECE.01 Acta de Compromiso del Nivel III (APA).'));
        DB::table('t_cod_documento')->insert(array('tipo' => '2', 'subtipo' => '3', 'orden' => '2', 'etiqueta' => 'DOC.ECE.01 Acta de Compromiso (confidencialidad) del Nivel IV (APLICADOR).'));
        DB::table('t_cod_documento')->insert(array('tipo' => '2', 'subtipo' => '3', 'orden' => '3', 'etiqueta' => 'DOC.ECE.03 Ficha de Contacto Previo por Grado - Actualización de Datos de la I.E.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '2', 'subtipo' => '3', 'orden' => '4', 'etiqueta' => 'DOC.ECE.04 Cronograma de Salida-Retorno y Asignación de Aplicadores por I.E. del Asistente de Procesos de Aplicación.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '2', 'subtipo' => '3', 'orden' => '5', 'etiqueta' => 'DOC.ECE.05 Reporte de Cobertura del Asistente de Procesos de Aplicación.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '2', 'subtipo' => '3', 'orden' => '6', 'etiqueta' => 'DOC.ECE.06 Reporte de Incidencias del Asistente de Procesos de Aplicación.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '2', 'subtipo' => '3', 'orden' => '7', 'etiqueta' => 'DOC.ECE.07 Reporte de Aplicación.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '2', 'subtipo' => '3', 'orden' => '8', 'etiqueta' => 'DOC.ECE.08 Control de Entrega y devolución del Material Minedu de la Red Administrativa.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '2', 'subtipo' => '3', 'orden' => '9', 'etiqueta' => 'DOC.ECE.10 Acta de Incidencia (todos los códigos excepto A-1, A-2 y B-3).'));
        DB::table('t_cod_documento')->insert(array('tipo' => '2', 'subtipo' => '3', 'orden' => '10', 'etiqueta' => 'Informe Técnico Final de la Sede Provincial/Distrital.'));
        DB::table('t_cod_documento')->insert(array('tipo' => '2', 'subtipo' => '3', 'orden' => '11', 'etiqueta' => 'Informe Técnico Final de la Sede Regional.'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_cod_documento');
    }
}
