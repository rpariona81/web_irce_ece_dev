<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewSedeOperativoEtapas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            DB::unprepared("            
            create view resum_marco as 
            SELECT 
            sede_id,operativo_marco,
            SUM(p_01_marco) AS p0_01_tot,
            0 AS p0_02_tot 
            from t_dig_marco GROUP BY sede_id,operativo_marco;
            CREATE VIEW resum_sede_operativo_etapa1 AS
            SELECT 
            t1.sede_id,
            t1.operativo,
            SUM(t2.p_01) AS p1_01,
            SUM(t2.p_02) AS p1_02 
            from t_dig_documento t1
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
            where t1.etapa=1 
            group by t1.sede_id, t1.operativo;
            CREATE VIEW resum_sede_operativo_etapa2 AS
            SELECT 
            t1.sede_id,
            t1.operativo,
            SUM(t2.p_01) AS p2_01,
            SUM(t2.p_02) AS p2_02 
            from t_dig_documento t1
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
            where t1.etapa=2 
            group by t1.sede_id, t1.operativo;
            CREATE VIEW resum_sede_operativo_etapa3 AS
            SELECT 
            t1.sede_id,
            t1.operativo,
            SUM(t2.p_01) AS p3_01,
            SUM(t2.p_02) AS p3_02 
            from t_dig_documento t1
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
            where t1.etapa=3 
            group by t1.sede_id, t1.operativo;
            CREATE VIEW resum_sede_operativo_etapa4 AS
            SELECT 
            t1.sede_id,
            t1.operativo,
            SUM(t2.p_01) AS p4_01,
            SUM(t2.p_02) AS p4_02 
            from t_dig_documento t1
            left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
            where t1.etapa=4 
            group by t1.sede_id, t1.operativo; 
            CREATE VIEW resum_sede_operativo_etapa_1234 as 
            SELECT  
            t1.id as sede_id,
            t1.cod_sede_regional,
            t1.sede_regional,
            t1.cod_sede_provincial,
            t1.sede_provincial,
            t1.cod_sede_distrital,
            t1.sede_distrital,
            t0.operativo_marco as operativo,
            CASE 
            WHEN t0.operativo_marco=1 THEN 'ECE' 
            WHEN t0.operativo_marco=2 THEN 'EM' 
            ELSE NULL 
            END
            AS operativo_nombre,
            t0.p0_01_tot, 
            0 AS p0_02_tot, 
            '0.00%' AS p0_02_porc, 
            IFNULL(t2.p1_01,0) AS p1_01, 
            CONCAT(FORMAT(if(IFNULL(t2.p1_01,0)=0,0,(t2.p1_01/t0.p0_01_tot)*100.0),2),'%') as p1_01_avance, 
            IFNULL(t2.p1_02,0) AS p1_02, 
            CONCAT(FORMAT(if(IFNULL(t2.p1_02,0)=0,0,(t2.p1_02/t0.p0_01_tot)*100.0),2),'%') as p1_02_avance, 
            IFNULL(t4.p2_01,0) AS p2_01,
            CONCAT(FORMAT(if(IFNULL(t4.p2_01,0)=0,0,(t4.p2_01/t0.p0_01_tot)*100.0),2),'%') as p2_01_avance, 
            IFNULL(t4.p2_02,0) AS p2_02, 
            CONCAT(FORMAT(if(IFNULL(t4.p2_02,0)=0,0,(t4.p2_02/t0.p0_01_tot)*100.0),2),'%') as p2_02_avance, 
            IFNULL(t5.p3_01,0) AS p3_01, 
            CONCAT(FORMAT(if(IFNULL(t5.p3_01,0)=0,0,(t5.p3_01/t0.p0_01_tot)*100.0),2),'%') as p3_01_avance, 
            IFNULL(t5.p3_02,0) AS p3_02, 
            CONCAT(FORMAT(if(IFNULL(t5.p3_02,0)=0,0,(t5.p3_02/t0.p0_01_tot)*100.0),2),'%') as p3_02_avance, 
            IFNULL(t6.p4_01,0) AS p4_01,
            CONCAT(FORMAT(if(IFNULL(t6.p4_01,0)=0,0,(t6.p4_01/t0.p0_01_tot)*100.0),2),'%') as p4_01_avance, 
            IFNULL(t6.p4_02,0) AS p4_02, 
            CONCAT(FORMAT(if(IFNULL(t6.p4_02,0)=0,0,(t6.p4_02/t0.p0_01_tot)*100.0),2),'%') as p4_02_avance 
            from t_cod_sede t1 
            left join resum_marco t0 on t1.id=t0.sede_id 
            left join resum_sede_operativo_etapa1 t2 on t2.sede_id=t1.id AND t0.operativo_marco=t2.operativo 
            left join resum_sede_operativo_etapa2 t4 on t4.sede_id=t2.sede_id and t0.operativo_marco=t4.operativo 
            left join resum_sede_operativo_etapa3 t5 on t5.sede_id=t2.sede_id and t0.operativo_marco=t5.operativo 
            left join resum_sede_operativo_etapa4 t6 on t6.sede_id=t2.sede_id and t0.operativo_marco=t6.operativo 
            order by t1.id, t0.operativo_marco;
            ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("
        DROP VIEW resum_sede_operativo_etapa_1234;
        DROP VIEW resum_sede_operativo_etapa4;
        DROP VIEW resum_sede_operativo_etapa3;
        DROP VIEW resum_sede_operativo_etapa2;
        DROP VIEW resum_sede_operativo_etapa1;
        DROP VIEW resum_marco;
        ");
    }
}
