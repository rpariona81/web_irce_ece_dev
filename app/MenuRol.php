<?php

namespace App;

use App\Admin\Traits\Auditorias;
use Illuminate\Database\Eloquent\Model;

class MenuRol extends Model
{
    use Auditorias;
    protected $table = 't_menu_roles';

    protected $fillable =[
        'id_rol',
        'id_menu',
        'estado',
        'created_by',
    ];

   
}
