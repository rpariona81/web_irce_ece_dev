
-- Procedimiento para obtener registro de ficha por filtro de sede, etapa, operativo, persona y fecha de registro
DELIMITER //
CREATE DEFINER=root@localhost PROCEDURE sp_obtener_registro_etapa_persona (IN sp_cabecera INT) NO SQL
BEGIN
declare EXISTE INT DEFAULT 0;
SET EXISTE = (select count(t1.id) from t_dig_documento t1 where t1.id = sp_cabecera);
			IF (EXISTE > 0)  then
            	BEGIN
                select
                t2.id AS id,
                t3.id AS documento_id,
                t3.tipo AS tipo,
                t3.subtipo AS subtipo,
                t3.orden AS orden,
                t3.etiqueta AS etiqueta,
                t2.p_01 AS p_01,
                t2.p_02 AS p_02,
                t2.p_obs AS p_obs
                from t_dig_documento t1 
                left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
                left join t_cod_documento t3 on t2.documento_id = t3.id 
                where t1.id = sp_cabecera;
                END;
			ELSE
				BEGIN
					SELECT 
					NULL AS id,
					a1.id AS documento_id,
					a1.tipo AS tipo,
					a1.subtipo AS subtipo,
					a1.orden AS orden,
					a1.etiqueta AS etiqueta,
					NULL AS p_01,
					NULL AS p_02,
					NULL AS p_obs
					FROM t_cod_documento a1;
				END;
            END IF;
         END
//
DELIMITER ;
--Ejecución del procedimiento
SET @p0='1'; SET @p1='1'; SET @p2='1'; SET @p3='1'; SET @p4='2019-05-09'; CALL sp_obtener_registro_etapa_persona(@p0, @p1, @p2, @p3, @p4);

--Vistas del sistema

--VISTA DETALLE VERTICAL: SEDE - OPERATIVO - PERSONA - ETAPA - DOCUMENTO
CREATE VIEW resum_detalle_vertical as 
SELECT 
t1.id AS cabecera_id,
t4.codigo_sede,
t4.sede_regional,
t4.sede_provincial,
t4.sede_distrital,
CASE 
WHEN
	t1.operativo=1 THEN 'ECE' ELSE 'EM' 
END
AS operativo,
t5.id AS persona_id,
CONCAT_WS('-',t5.nombres,t5.nro_documento) as persona,
CASE 
WHEN t1.etapa=1 THEN 'Inventario y Recepción' 
WHEN t1.etapa=2 THEN 'Revisión y Consistencia' 
WHEN t1.etapa=3 THEN 'Control de Calidad' 
ELSE 'Inventario' 
END
AS etapa,
t2.id AS id,
t3.id AS documento_id,
CASE 
WHEN t3.tipo = 1 THEN 'MINEDU' 
ELSE 'INEI'
END 
AS tipo,
CASE 
WHEN t3.subtipo=1 THEN '1.ENTREGABLE'
WHEN t3.subtipo=2 THEN '2.NO APLICACIÓN'
ELSE '3.OPERACIÓN DE CAMPO - INEI'
END 
AS subtipo,
t3.orden AS orden,
t3.etiqueta AS etiqueta,
SUM(t2.p_01) AS p_01_tot,
SUM(t2.p_02) AS p_02_tot,
COUNT(t2.p_obs) AS p_obs_tot 
from t_dig_documento t1
left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
left join t_cod_documento t3 on t2.documento_id = t3.id
left join t_cod_sede t4 on t4.id = t1.sede_id 
left join t_dig_persona t5 on t5.id = t1.persona_id 
group by t1.sede_id, t1.etapa, t1.operativo, t2.documento_id;

--resumen_persona_sede_fecha_etapa=1
select 
t1.persona_id,
t1.sede_id,
t1.fecha_registro,
sum(t2.p_01) AS p1_01_tot,
sum(t2.p_02) AS p1_02_tot 
from t_dig_documento t1 
left join t_det_dig_documento t2 on t1.id = t2.cabecera_id 
where t1.etapa = 1 
group by t1.persona_id,t1.sede_id,t1.fecha_registro;

----vista de resumen 01 - por sede - operativo - etapa 
DROP VIEW IF EXISTS resum_sede_operativo_etapa1;
CREATE VIEW resum_sede_operativo_etapa1 AS
SELECT 
t1.sede_id,
t1.operativo,
SUM(t2.p_01) AS p1_01,
SUM(t2.p_02) AS p1_02 
from t_dig_documento t1
left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
where t1.etapa=1 
group by t1.sede_id, t1.operativo;

DROP VIEW IF EXISTS resum_sede_operativo_etapa2;
CREATE VIEW resum_sede_operativo_etapa2 AS
SELECT 
t1.sede_id,
t1.operativo,
SUM(t2.p_01) AS p2_01,
SUM(t2.p_02) AS p2_02 
from t_dig_documento t1
left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
where t1.etapa=2 
group by t1.sede_id, t1.operativo;

DROP VIEW IF EXISTS resum_sede_operativo_etapa3;
CREATE VIEW resum_sede_operativo_etapa3 AS
SELECT 
t1.sede_id,
t1.operativo,
SUM(t2.p_01) AS p3_01,
SUM(t2.p_02) AS p3_02 
from t_dig_documento t1
left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
where t1.etapa=3 
group by t1.sede_id, t1.operativo;

DROP VIEW IF EXISTS resum_sede_operativo_etapa4;
CREATE VIEW resum_sede_operativo_etapa4 AS
SELECT 
t1.sede_id,
t1.operativo,
SUM(t2.p_01) AS p4_01,
SUM(t2.p_02) AS p4_02 
from t_dig_documento t1
left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
where t1.etapa=4 
group by t1.sede_id, t1.operativo; 



DROP VIEW IF EXISTS resum_marco;
create view resum_marco as 
SELECT 
sede_id,operativo_marco,
SUM(p_01_marco) AS p0_01_tot,
SUM(p_02_marco) AS p0_02_tot  
from t_dig_marco GROUP BY sede_id,operativo_marco;


--VISTA DETALLE HORIZONTAL: SEDE - OPERATIVO - DOCUMENTO
DROP VIEW IF EXISTS resum_sede_operativo_etapa_1234;
CREATE VIEW resum_sede_operativo_etapa_1234 as 
select 
t1.id as sede_id,
t1.cod_sede_regional,
t1.sede_regional,
t1.cod_sede_provincial,
t1.sede_provincial,
t1.cod_sede_distrital,
t1.sede_distrital,
t2.operativo,
CASE 
WHEN
	t2.operativo=1 THEN 'ECE' ELSE 'EM' 
END
AS operativo_nombre,
t0.p0_01_tot,
t0.p0_02_tot,
t2.p1_01,
CONCAT(FORMAT(if(t2.p1_01=0,0,(t2.p1_01/t0.p0_01_tot)*100.0),2),'%') as p1_01_avance,
t2.p1_02,
CONCAT(FORMAT(if(t2.p1_02=0,0,(t2.p1_02/t0.p0_02_tot)*100.0),2),'%') as p1_02_avance,
t4.p2_01,
CONCAT(FORMAT(if(t4.p2_01=0,0,(t4.p2_01/t0.p0_01_tot)*100.0),2),'%') as p2_01_avance,
t4.p2_02,
CONCAT(FORMAT(if(t4.p2_02=0,0,(t4.p2_02/t0.p0_02_tot)*100.0),2),'%') as p2_02_avance,
t5.p3_01,
CONCAT(FORMAT(if(t5.p3_01=0,0,(t5.p3_01/t0.p0_01_tot)*100.0),2),'%') as p3_01_avance,
t5.p3_02,
CONCAT(FORMAT(if(t5.p3_02=0,0,(t5.p3_02/t0.p0_02_tot)*100.0),2),'%') as p3_02_avance,
t6.p4_01,
CONCAT(FORMAT(if(t6.p4_01=0,0,(t6.p4_01/t0.p0_01_tot)*100.0),2),'%') as p4_01_avance,
t6.p4_02,
CONCAT(FORMAT(if(t6.p4_02=0,0,(t6.p4_02/t0.p0_02_tot)*100.0),2),'%') as p4_02_avance 
from 
t_cod_sede t1
left join resum_sede_operativo_etapa1 t2 on t2.sede_id=t1.id 
left join resum_sede_operativo_etapa2 t4 on t4.sede_id=t2.sede_id and t4.operativo=t2.operativo 
left join resum_sede_operativo_etapa3 t5 on t5.sede_id=t2.sede_id and t5.operativo=t2.operativo 
left join resum_sede_operativo_etapa4 t6 on t6.sede_id=t2.sede_id and t6.operativo=t2.operativo 
left join resum_marco t0 on t1.id=t0.sede_id AND t0.operativo_marco=t2.operativo 
order by t1.codigo_sede, t2.operativo;





----vista de detalle de resumen 01 - por sede - operativo - etapa - documento
CREATE VIEW resum_sede_operativo_etapa1_documento AS
SELECT 
t1.sede_id,
t1.operativo,
t2.documento_id,
SUM(t2.p_01) AS p1_01,
SUM(t2.p_02) AS p1_02 
from t_dig_documento t1
left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
where t1.etapa=1 
group by t1.sede_id, t1.operativo, t2.documento_id;
CREATE VIEW resum_sede_operativo_etapa2_documento AS
SELECT 
t1.sede_id,
t1.operativo,
t2.documento_id,
SUM(t2.p_01) AS p2_01,
SUM(t2.p_02) AS p2_02 
from t_dig_documento t1
left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
where t1.etapa=2 
group by t1.sede_id, t1.operativo, t2.documento_id;
CREATE VIEW resum_sede_operativo_etapa3_documento AS
SELECT 
t1.sede_id,
t1.operativo,
t2.documento_id,
SUM(t2.p_01) AS p3_01,
SUM(t2.p_02) AS p3_02 
from t_dig_documento t1
left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
where t1.etapa=3 
group by t1.sede_id, t1.operativo, t2.documento_id;
CREATE VIEW resum_sede_operativo_etapa4_documento AS
SELECT 
t1.sede_id,
t1.operativo,
t2.documento_id,
SUM(t2.p_01) AS p4_01,
SUM(t2.p_02) AS p4_02 
from t_dig_documento t1
left join t_det_dig_documento t2 on t1.id = t2.cabecera_id
where t1.etapa=4 
group by t1.sede_id, t1.operativo, t2.documento_id; 
CREATE VIEW resum_sede_operativo_etapa_1234_documento as 
SELECT 
t1.id as sede_id,
t1.codigo_sede,
t1.sede_regional,
t1.sede_provincial,
t1.sede_distrital,
t2.operativo,
CASE 
WHEN t2.operativo=1 THEN 'ECE' 
WHEN t2.operativo=2 THEN 'EM' 
ELSE NULL  
END
AS operativo_nombre,
CASE 
WHEN t3.tipo = 1 THEN 'MINEDU' 
ELSE 'INEI'
END 
AS tipo,
CASE 
WHEN t3.subtipo=1 THEN '1.ENTREGABLE'
WHEN t3.subtipo=2 THEN '2.NO APLICACIÓN'
ELSE '3.OPERACIÓN DE CAMPO - INEI'
END 
AS subtipo,
t3.orden,
t3.etiqueta,
t2.p1_01,
t2.p1_02,
t4.p2_01,
t4.p2_02,
t5.p3_01,
t5.p3_02,
t6.p4_01,
t6.p4_02  
from 
t_cod_sede t1 
left join resum_sede_operativo_etapa1_documento t2 on t2.sede_id=t1.id 
left join resum_sede_operativo_etapa2_documento t4 on t4.sede_id=t2.sede_id and t4.documento_id=t2.documento_id and t4.operativo=t2.operativo 
left join resum_sede_operativo_etapa3_documento t5 on t5.sede_id=t2.sede_id and t5.documento_id=t2.documento_id and t5.operativo=t2.operativo 
left join resum_sede_operativo_etapa4_documento t6 on t6.sede_id=t2.sede_id and t6.documento_id=t2.documento_id and t6.operativo=t2.operativo 
left join t_cod_documento t3 on t3.id=t2.documento_id 
order by t1.codigo_sede, t2.operativo, t3.tipo, t3.subtipo, t3.orden;








/*Pruebas Procedimiento para ver operativo*/
DELIMITER //
CREATE DEFINER=root@localhost PROCEDURE sp_ver_avance_sede_operativo(IN sp_region CHAR(3), IN sp_provincia CHAR(2), IN sp_distrito CHAR(2), IN sp_operativo INT) NO SQL
BEGIN
IF (sp_operativo = '') THEN 
BEGIN
            IF (sp_region = '' AND sp_provincia = '' AND sp_distrito = '') THEN
				BEGIN 
				SELECT sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,
                operativo,operativo_nombre,p0_01_tot,p0_02_tot,p1_01,p1_01_avance,p1_02,p1_02_avance,p2_01,p2_01_avance,
                p2_02,p2_02_avance,p3_01,p3_01_avance,p3_02,p3_02_avance,p4_01,p4_01_avance,p4_02,p4_02_avance 
				FROM resum_sede_operativo_etapa_1234;
                END;
            ELSEIF (sp_region IS NOT NULL AND sp_provincia = '' AND sp_distrito = '') THEN
            	BEGIN 
				SELECT sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,
                operativo,operativo_nombre,p0_01_tot,p0_02_tot,p1_01,p1_01_avance,p1_02,p1_02_avance,p2_01,p2_01_avance,
                p2_02,p2_02_avance,p3_01,p3_01_avance,p3_02,p3_02_avance,p4_01,p4_01_avance,p4_02,p4_02_avance 
				FROM resum_sede_operativo_etapa_1234 WHERE cod_sede_regional = sp_region;
				END;
			ELSEIF (sp_region IS NOT NULL AND sp_provincia IS NOT NULL AND sp_distrito = '') THEN
            	BEGIN 
				SELECT sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,
                operativo,operativo_nombre,p0_01_tot,p0_02_tot,p1_01,p1_01_avance,p1_02,p1_02_avance,p2_01,p2_01_avance,
                p2_02,p2_02_avance,p3_01,p3_01_avance,p3_02,p3_02_avance,p4_01,p4_01_avance,p4_02,p4_02_avance 
				FROM resum_sede_operativo_etapa_1234 WHERE cod_sede_regional = sp_region and cod_sede_provincial = sp_provincia;
				END;
			ELSEIF (sp_region IS NOT NULL AND sp_provincia IS NOT NULL AND sp_distrito IS NOT NULL) THEN
            	BEGIN 
				SELECT sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,
                operativo,operativo_nombre,p0_01_tot,p0_02_tot,p1_01,p1_01_avance,p1_02,p1_02_avance,p2_01,p2_01_avance,
                p2_02,p2_02_avance,p3_01,p3_01_avance,p3_02,p3_02_avance,p4_01,p4_01_avance,p4_02,p4_02_avance 
				FROM resum_sede_operativo_etapa_1234 WHERE cod_sede_regional = sp_region and cod_sede_provincial = sp_provincia and cod_sede_distrital = sp_distrito;
				END;
            END IF;
		END;
ELSEIF (sp_operativo IS NOT NULL) THEN 
            IF (sp_region = '' AND sp_provincia = '' AND sp_distrito = '') THEN
				BEGIN 
				SELECT sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,
                operativo,operativo_nombre,p0_01_tot,p0_02_tot,p1_01,p1_01_avance,p1_02,p1_02_avance,p2_01,p2_01_avance,
                p2_02,p2_02_avance,p3_01,p3_01_avance,p3_02,p3_02_avance,p4_01,p4_01_avance,p4_02,p4_02_avance 
				FROM resum_sede_operativo_etapa_1234 WHERE operativo = sp_operativo;
                END;
            ELSEIF (sp_region IS NOT NULL AND sp_provincia = '' AND sp_distrito = '') THEN
            	BEGIN 
				SELECT sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,
                operativo,operativo_nombre,p0_01_tot,p0_02_tot,p1_01,p1_01_avance,p1_02,p1_02_avance,p2_01,p2_01_avance,
                p2_02,p2_02_avance,p3_01,p3_01_avance,p3_02,p3_02_avance,p4_01,p4_01_avance,p4_02,p4_02_avance 
				FROM resum_sede_operativo_etapa_1234 WHERE operativo = sp_operativo and cod_sede_regional = sp_region;
				END;
			ELSEIF (sp_region IS NOT NULL AND sp_provincia IS NOT NULL AND sp_distrito = '') THEN
            	BEGIN 
				SELECT sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,
                operativo,operativo_nombre,p0_01_tot,p0_02_tot,p1_01,p1_01_avance,p1_02,p1_02_avance,p2_01,p2_01_avance,
                p2_02,p2_02_avance,p3_01,p3_01_avance,p3_02,p3_02_avance,p4_01,p4_01_avance,p4_02,p4_02_avance 
				FROM resum_sede_operativo_etapa_1234 WHERE operativo = sp_operativo and cod_sede_regional = sp_region and cod_sede_provincial = sp_provincia;
				END;
			ELSEIF (sp_region IS NOT NULL AND sp_provincia IS NOT NULL AND sp_distrito IS NOT NULL) THEN
            	BEGIN 
				SELECT sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,
                operativo,operativo_nombre,p0_01_tot,p0_02_tot,p1_01,p1_01_avance,p1_02,p1_02_avance,p2_01,p2_01_avance,
                p2_02,p2_02_avance,p3_01,p3_01_avance,p3_02,p3_02_avance,p4_01,p4_01_avance,p4_02,p4_02_avance 
				FROM resum_sede_operativo_etapa_1234 WHERE operativo = sp_operativo and cod_sede_regional = sp_region and cod_sede_provincial = sp_provincia and cod_sede_distrital = sp_distrito;
				END;
            END IF;
            END IF;
            END
//
DELIMITER ;



SELECT t0.id,t0.nombres,A.p_01_1,A.p_02_1,B.p_01_2,B.p_02_2,C.p_01_3,C.p_02_3,D.p_01_4,D.p_02_4 
FROM 
t_dig_persona t0 
LEFT JOIN
(SELECT persona_id,SUM(p_01) as p_01_1,SUM(p_02) as p_02_1 FROM t_dig_documento t1 left join t_det_dig_documento t2 on t1.id=t2.cabecera_id where etapa=1 group by persona_id) A on A.persona_id=t0.id 
LEFT JOIN 
(SELECT persona_id,SUM(p_01) as p_01_2,SUM(p_02) as p_02_2 FROM t_dig_documento t1 left join t_det_dig_documento t2 on t1.id=t2.cabecera_id where etapa=2 group by persona_id) B ON B.persona_id=t0.id 
LEFT JOIN 
(SELECT persona_id,SUM(p_01) as p_01_3,SUM(p_02) as p_02_3 FROM t_dig_documento t1 left join t_det_dig_documento t2 on t1.id=t2.cabecera_id where etapa=3 group by persona_id) C ON C.persona_id=t0.id 
LEFT JOIN 
(SELECT persona_id,SUM(p_01) as p_01_4,SUM(p_02) as p_02_4 FROM t_dig_documento t1 left join t_det_dig_documento t2 on t1.id=t2.cabecera_id where etapa=4 group by persona_id) D ON D.persona_id=t0.id




--procedimiento para ver productividad por persona - sede - fecha

DELIMITER //
CREATE PROCEDURE repo_productividad_persona_sede_fecha(IN sp_persona_id INT, IN operativo_id INT, IN fecha_ini DATE, IN fecha_fin DATE) NO SQL 
BEGIN 
select t2.nombres,CONCAT_WS(" ",t3.sede_regional,t3.sede_provincial,t3.sede_distrital) as sede,t1.fecha_registro
,t4.p_01 as 'p_01_1'
,t4.p_02 as 'p_02_1'
,t4.p_obs as 'p_obs_1'
,t5.p_01 as 'p_01_2'
,t5.p_02 as 'p_02_2'
,t5.p_obs as 'p_obs_2'
,t6.p_01 as 'p_01_3'
,t6.p_02 as 'p_02_3'
,t6.p_obs as 'p_obs_3'
,t7.p_01 as 'p_01_4'
,t7.p_02 as 'p_02_4'
,t7.p_obs as 'p_obs_4'
,t1.updated_at,t1.updated_by
from t_dig_documento t1
left join t_dig_persona t2 on t2.id=t1.persona_id
left join t_cod_sede t3 on t3.id=t1.sede_id
left join (
	select a1.cabecera_id, sum(a1.p_01) as p_01, sum(a1.p_02) as p_02, count(a1.p_obs) as p_obs,a2.etapa,a2.operativo, a2.sede_id
	from t_det_dig_documento a1
	left join t_dig_documento a2 on a2.id=a1.cabecera_id
	where a2.persona_id=sp_persona_id and a2.fecha_registro BETWEEN sp_fecha_ini and sp_fecha_fin and a2.etapa=1
	group by a2.persona_id,a2.sede_id, a2.fecha_registro
) t4 on t4.cabecera_id=t1.id
left join (
	select a1.cabecera_id, sum(a1.p_01) as p_01, sum(a1.p_02) as p_02, count(a1.p_obs) as p_obs,a2.etapa,a2.operativo, a2.sede_id
	from t_det_dig_documento a1
	left join t_dig_documento a2 on a2.id=a1.cabecera_id
	where a2.persona_id=sp_persona_id and a2.fecha_registro BETWEEN sp_fecha_ini and sp_fecha_fin and a2.etapa=2
	group by a2.persona_id,a2.sede_id, a2.fecha_registro
) t5 on t5.cabecera_id=t1.id
left join (
	select a1.cabecera_id, sum(a1.p_01) as p_01, sum(a1.p_02) as p_02, count(a1.p_obs) as p_obs,a2.etapa,a2.operativo, a2.sede_id
	from t_det_dig_documento a1
	left join t_dig_documento a2 on a2.id=a1.cabecera_id
	where a2.persona_id=sp_persona_id and a2.fecha_registro BETWEEN sp_fecha_ini and sp_fecha_fin and a2.etapa=3
	group by a2.persona_id,a2.sede_id, a2.fecha_registro
) t6 on t6.cabecera_id=t1.id
left join (
	select a1.cabecera_id, sum(a1.p_01) as p_01, sum(a1.p_02) as p_02, count(a1.p_obs) as p_obs,a2.etapa,a2.operativo, a2.sede_id
	from t_det_dig_documento a1
	left join t_dig_documento a2 on a2.id=a1.cabecera_id
	where a2.persona_id=sp_persona_id and a2.fecha_registro BETWEEN sp_fecha_ini and sp_fecha_fin and a2.etapa=4
	group by a2.persona_id,a2.sede_id, a2.fecha_registro
) t7 on t7.cabecera_id=t1.id
where t1.persona_id=sp_persona_id and t1.operativo=sp_operativo_id and fecha_registro BETWEEN sp_fecha_ini and sp_fecha_fin
group by t1.persona_id, t1.sede_id, t1.fecha_registro;
END;
//


-- reporte de productividad por fecha 

SELECT Z.id, Z.cargo_id,Z.nombres,Z.doc_identidad, 
CASE 
WHEN Z.doc_identidad=1 THEN 'DNI' 
WHEN Z.doc_identidad=2 THEN 'Carnet de extranjería' 
WHEN Z.doc_identidad=3 THEN 'RUC' 
WHEN Z.doc_identidad=4 THEN 'Otro' 
ELSE 'No indicado' END AS tipo_doc_identidad,Z.nro_documento, 
X.fecha_registro,X.operativo, 
A.p_01_1,A.p_02_1,A.p_obs_1,
B.p_01_2,B.p_02_2,B.p_obs_2,
C.p_01_3,C.p_02_3,C.p_obs_3,
D.p_01_4,D.p_02_4,D.p_obs_4   
FROM t_dig_persona Z LEFT JOIN 
(SELECT DISTINCT persona_id, fecha_registro, operativo FROM t_dig_documento) X ON Z.id = X.persona_id 
LEFT JOIN 
(select 
a1.operativo,a1.persona_id,a1.fecha_registro,
SUM(a2.p_01) AS p_01_1, SUM(a2.p_02) AS p_02_1, SUM(if (p_obs = '',0,1)) AS p_obs_1 
from t_dig_documento a1 
left join t_det_dig_documento a2 on a1.id=a2.cabecera_id 
where etapa=1 
GROUP BY a1.etapa,a1.operativo,a1.persona_id,a1.fecha_registro) A ON A.persona_id=Z.id AND X.fecha_registro=A.fecha_registro and X.operativo=A.operativo 
LEFT JOIN 
(select 
a1.operativo,a1.persona_id,a1.fecha_registro,
SUM(a2.p_01) AS p_01_2, SUM(a2.p_02) AS p_02_2, SUM(if (p_obs = '',0,1)) AS p_obs_2 
from t_dig_documento a1 
left join t_det_dig_documento a2 on a1.id=a2.cabecera_id 
where etapa=2 
GROUP BY a1.etapa,a1.operativo,a1.persona_id,a1.fecha_registro) B ON B.persona_id=Z.id AND X.fecha_registro=B.fecha_registro and X.operativo=B.operativo 
LEFT JOIN 
(select 
a1.operativo,a1.persona_id,a1.fecha_registro,
SUM(a2.p_01) AS p_01_3, SUM(a2.p_02) AS p_02_3, SUM(if (p_obs = '',0,1)) AS p_obs_3 
from t_dig_documento a1 
left join t_det_dig_documento a2 on a1.id=a2.cabecera_id 
where etapa=3 
GROUP BY a1.etapa,a1.operativo,a1.persona_id,a1.fecha_registro) C ON C.persona_id=Z.id AND X.fecha_registro=C.fecha_registro and X.operativo=C.operativo
LEFT JOIN 
(select 
a1.operativo,a1.persona_id,a1.fecha_registro, 
SUM(a2.p_01) AS p_01_4, SUM(a2.p_02) AS p_02_4, SUM(if (p_obs = '',0,1)) AS p_obs_4 
from t_dig_documento a1 
left join t_det_dig_documento a2 on a1.id=a2.cabecera_id 
where etapa=4 
GROUP BY a1.etapa,a1.operativo,a1.persona_id,a1.fecha_registro) D ON D.persona_id=Z.id AND X.fecha_registro=D.fecha_registro and X.operativo=D.operativo;




CREATE VIEW detalle_productividad_operativo_sede_persona as 
SELECT t1.operativo,
CASE WHEN operativo=1 THEN 'ECE' ELSE 'EM' END AS operativo_nombre,
t3.id as sede_id,t3.cod_sede_regional,t3.sede_regional,t3.cod_sede_provincial,t3.sede_provincial,t3.cod_sede_distrital,t3.sede_distrital,t2.id as persona_id,t2.nombres,t1.etapa,
CASE WHEN t1.etapa=1 THEN 'Inventario y Recepción' WHEN t1.etapa=2 THEN 'Revisión y Consistencia' WHEN t1.etapa=3 THEN 'Control de Calidad' ELSE 'Embalado' END AS etapa_nombre,
t1.fecha_registro,
t4.p_01,t4.p_02,t4.p_obs 
FROM t_dig_documento t1
LEFT JOIN t_dig_persona t2 on t1.persona_id=t2.id 
LEFT JOIN t_cod_sede t3 on t1.sede_id=t3.id 
LEFT JOIN t_det_dig_documento t4 on t1.id=t4.cabecera_id;







--borrado
/*-Borrado de registros en t_dig_documento, borra en cascada*/
delete from t_dig_documento where id>156 and fecha_registro<>'2019-11-30' and created_at is not null;
/*-Borrado de registros en t_dig_persona, solo quedan los 62 y 1 que es omision*/
delete from t_dig_persona where id>63;
/*-Borrado de registros en t_dig_marco, solo quedan los 1196 del excel*/
delete from t_dig_marco where id>1196;

New Menú
==========

INSERT INTO `t_menus` (`id`, `id_padre`, `titulo`, `icono`, `link`, `descripcion`, `orden`, `estado`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
(1, NULL, 'Inicio', NULL, '/', NULL, 1, 1, '2019-11-29 14:38:59', '2019-11-29 14:38:59', NULL, NULL, NULL, NULL),
(2, NULL, 'Registros', NULL, '/', NULL, 2, 1, '2019-11-29 14:40:28', '2019-11-29 14:40:28', NULL, NULL, NULL, NULL),
(3, NULL, 'Mantenimiento', NULL, '/', NULL, 9, 1, '2019-11-29 14:40:40', '2019-11-29 14:40:40', NULL, NULL, NULL, NULL),
(4, 1, 'Inicio', 'fa fa-home', '/inicio', NULL, 1, 1, '2019-11-29 14:42:09', '2019-11-29 14:42:09', NULL, NULL, NULL, NULL),
(5, 3, 'Usuarios', 'fa fa-users', '/usuarios', NULL, 1, 1, '2019-11-29 14:44:44', '2019-11-29 14:44:44', NULL, NULL, NULL, NULL),
(6, 3, 'Roles', 'fa fa-grav', '/roles', NULL, 2, 1, '2019-11-29 14:45:58', '2019-11-29 14:45:58', NULL, NULL, NULL, NULL),
(7, 3, 'Menus', 'fa fa-bars', '/menus', NULL, 3, 1, '2019-11-29 14:46:38', '2019-11-29 14:46:38', NULL, NULL, NULL, NULL),
(8, 2, 'Registro de Avance Diario', 'fa fa-paper-plane-o', '/cantidades', NULL, 1, 1, '2019-11-29 22:12:51', '2019-11-29 22:12:51', NULL, NULL, NULL, NULL),
(9, NULL, 'Reportes', NULL, '/', NULL, 3, 1, '2019-12-02 19:36:39', '2019-12-02 19:36:39', NULL, NULL, NULL, NULL),
(10, 9, 'Gestión Documental x Sede Provincial / Distrital', 'fa fa-tasks', '/reporte_avance', NULL, 1, 1, '2019-12-02 19:57:16', '2019-12-02 19:57:16', NULL, NULL, NULL, NULL),
(11, 9, 'Directorio Personal', 'fa fa-users', '/personas', NULL, 8, 1, '2019-12-02 21:55:03', '2019-12-02 21:55:03', NULL, NULL, NULL, NULL),
(12, 9, 'Producción de Personal', 'fa fa-male', '/reporte_productividad', NULL, 4, 1, '2019-12-02 21:55:03', '2019-12-02 21:55:03', NULL, NULL, NULL, NULL),
(13, 9, 'Reporte de Registro Diario', 'fa fa-tasks', '/reporte_diario', NULL, 4, 1, '2019-12-02 21:55:03', '2019-12-02 21:55:03', NULL, NULL, NULL, NULL);
(14, 9, 'Gestión Documental x Sede Regional', 'fa fa-university', '/reporte_avance_nacional', NULL, 0, 1, '2019-12-02 21:55:03', '2019-12-02 21:55:03', NULL, NULL, NULL, NULL);
(15, 9, 'Avance por Documento', 'fa fa-book', '/reporte_avance_documento', NULL, 3, 1, '2019-12-02 21:55:03', '2019-12-02 21:55:03', NULL, NULL, NULL, NULL);