<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpVerAvancePorDocumentoSedregOperativoTipoDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
                CREATE PROCEDURE `sp_ver_avance_doc_reg_operativo_tipdoc`(IN sp_operativo INT, IN sp_tipo_documento INT)
                NO SQL
                BEGIN
                SELECT
                        NULL AS row_num,
                    'TOTAL' AS sede_regional,
                        NULL as doc_id,
                        NULL as tipo_nombre,
                    NULL AS etiqueta,
                    FORMAT(SUM(IFNULL(t0.p0_01_tot,0)),'Currency') AS p0_01_tot_doc,
                    FORMAT(SUM(IFNULL(t0.p0_02_tot,0)),'Currency') AS p0_01_tot_obs,
                    CONCAT(FORMAT(if(IFNULL(SUM(t0.p0_02_tot),0)=0,0,(SUM(t0.p0_02_tot)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p0_01_porc_obs,
					FORMAT(SUM(IFNULL(t2.p1_01,0)),'Currency') AS p1_01_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(t2.p1_01),0)=0,0,(SUM(t2.p1_01)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p1_01_porc_doc,
                    FORMAT(SUM(IFNULL(t2.p1_02,0)),'Currency') AS p1_02_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(t2.p1_02),0)=0,0,(SUM(t2.p1_02)/SUM(t0.p0_01_tot))*100.0),2),'%') AS p1_02_porc_obs,
                    FORMAT(SUM(IFNULL(t4.p2_01,0)),'Currency') AS p2_01_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(t4.p2_01),0)=0,0,(SUM(t4.p2_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p2_01_porc_doc, 
                    FORMAT(SUM(IFNULL(t4.p2_02,0)),'Currency') AS p2_02_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(t4.p2_02),0)=0,0,(SUM(t4.p2_02)/SUM(t0.p0_01_tot))*100.0),2),'%') as p2_02_porc_obs, 
                    FORMAT(SUM(IFNULL(t5.p3_01,0)),'Currency') AS p3_01_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(t5.p3_01),0)=0,0,(SUM(t5.p3_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p3_01_porc_doc, 
                    FORMAT(SUM(IFNULL(t5.p3_02,0)),'Currency') AS p3_02_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(t5.p3_02),0)=0,0,(SUM(t5.p3_02)/SUM(t0.p0_01_tot))*100.0),2),'%') as p3_02_porc_obs, 
                    FORMAT(SUM(IFNULL(t6.p4_01,0)),'Currency') AS p4_01_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(t6.p4_01),0)=0,0,(SUM(t6.p4_01)/SUM(t0.p0_01_tot))*100.0),2),'%') as p4_01_porc_doc, 
                    FORMAT(SUM(IFNULL(t6.p4_02,0)),'Currency') AS p4_02_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(t6.p4_02),0)=0,0,(SUM(t6.p4_02)/SUM(t0.p0_01_tot))*100.0),2),'%') as p4_02_porc_obs
                    FROM t_cod_sede t1 
                    LEFT JOIN (
                        SELECT m1.sede_id,m1.documento_id as doc_id,m2.etiqueta,m1.operativo_marco, SUM(m1.p_01_marco) AS p0_01_tot, 0 AS p0_02_tot 
                        from t_dig_marco m1
                        left join t_cod_documento m2 on m2.id=m1.documento_id
                        left join t_cod_sede m3 on m3.id=m1.sede_id
                        WHERE if(sp_operativo=0,true,m1.operativo_marco=sp_operativo)
                        GROUP BY m1.sede_id,m2.id
                        order by m1.sede_id,m2.id
                    ) t0 on t0.sede_id=t1.id
                left join (
                        select a2.sede_id, a2.operativo, a3.tipo, a1.documento_id, sum(a1.p_01) AS p1_01, sum(a1.p_02) AS p1_02 
                    from t_det_dig_documento a1
                    left join t_dig_documento a2 on a2.id=a1.cabecera_id
                    left join t_cod_documento a3 on a3.id=a1.documento_id
                    WHERE a2.etapa=1 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                    GROUP BY a2.sede_id,a1.documento_id
                        order by a2.sede_id,a1.documento_id
                    ) t2 on t2.sede_id=t0.sede_id and t2.documento_id=t0.doc_id
                left join (
                        select a2.sede_id, a2.operativo, a3.tipo, a1.documento_id, SUM(a1.p_01) AS p2_01, SUM(a1.p_02) AS p2_02 
                    from t_det_dig_documento a1
                    left join t_dig_documento a2 on a2.id=a1.cabecera_id
                    left join t_cod_documento a3 on a3.id=a1.documento_id
                    WHERE a2.etapa=2 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                    GROUP BY a2.sede_id,a1.documento_id
                        order by a2.sede_id,a1.documento_id
                    ) t4 on t4.sede_id=t0.sede_id and t4.documento_id=t0.doc_id
                left join (
                        select a2.sede_id, a2.operativo, a3.tipo, a1.documento_id, SUM(a1.p_01) AS p3_01, SUM(a1.p_02) AS p3_02 
                    from t_det_dig_documento a1
                    left join t_dig_documento a2 on a2.id=a1.cabecera_id
                    left join t_cod_documento a3 on a3.id=a1.documento_id
                    WHERE a2.etapa=3 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                    GROUP BY a2.sede_id,a1.documento_id
                        order by a2.sede_id,a1.documento_id
                    ) t5 on t5.sede_id=t0.sede_id and t5.documento_id=t0.doc_id
                left join (
                        select a2.sede_id, a2.operativo, a3.tipo, a1.documento_id, SUM(a1.p_01) AS p4_01, SUM(a1.p_02) AS p4_02 
                    from t_det_dig_documento a1
                    left join t_dig_documento a2 on a2.id=a1.cabecera_id
                    left join t_cod_documento a3 on a3.id=a1.documento_id
                    WHERE a2.etapa=4 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                    GROUP BY a2.sede_id,a1.documento_id
                        order by a2.sede_id,a1.documento_id
                    ) t6 on t6.sede_id=t0.sede_id and t6.documento_id=t0.doc_id
                UNION
                SELECT row_number() over (order by f.id, f.doc_id) AS row_num, f.sede_regional,f.doc_id,f.tipo_nombre,f.etiqueta,
                p0_01_tot_doc,p0_01_tot_obs,p0_01_porc_obs,p1_01_tot_doc,p1_01_porc_doc,p1_02_tot_doc,p1_02_porc_obs,p2_01_tot_doc,p2_01_porc_doc,p2_02_tot_doc,p2_02_porc_obs,p3_01_tot_doc,p3_01_porc_doc,p3_02_tot_doc,p3_02_porc_obs,p4_01_tot_doc,p4_01_porc_doc,p4_02_tot_doc,p4_02_porc_obs
                FROM (
                    SELECT
                        t1.id,
                        t1.sede_regional,
                        t0.doc_id,
                        CASE WHEN t0.tipo = 1 THEN 'MINEDU' WHEN t0.tipo = 2 THEN 'INEI' ELSE NULL	END AS tipo_nombre,
                        t0.etiqueta,
                        FORMAT(IFNULL(t0.p0_01_tot,0),'Currency') AS p0_01_tot_doc,
                        FORMAT(IFNULL(t0.p0_02_tot,0),'Currency') AS p0_01_tot_obs,
                        CONCAT(FORMAT(if((IFNULL(t0.p0_02_tot,0))=0,0,((t0.p0_02_tot)/(t0.p0_01_tot))*100.0),2),'%') AS p0_01_porc_obs,
                        FORMAT(IFNULL(t2.p1_01,0),'Currency') AS p1_01_tot_doc, 
                        CONCAT(FORMAT(if((IFNULL(t2.p1_01,0))=0,0,((t2.p1_01)/(t0.p0_01_tot))*100.0),2),'%') AS p1_01_porc_doc, 
                        FORMAT(IFNULL(t2.p1_02,0),'Currency') AS p1_02_tot_doc, 
                        CONCAT(FORMAT(if((IFNULL(t2.p1_02,0))=0,0,((t2.p1_02)/(t0.p0_01_tot))*100.0),2),'%') AS p1_02_porc_obs, 
                        FORMAT(IFNULL(t4.p2_01,0),'Currency') AS p2_01_tot_doc, 
                        CONCAT(FORMAT(if((IFNULL(t4.p2_01,0))=0,0,((t4.p2_01)/(t0.p0_01_tot))*100.0),2),'%') as p2_01_porc_doc, 
                        FORMAT(IFNULL(t4.p2_02,0),'Currency') AS p2_02_tot_doc, 
                        CONCAT(FORMAT(if((IFNULL(t4.p2_02,0))=0,0,((t4.p2_02)/(t0.p0_01_tot))*100.0),2),'%') as p2_02_porc_obs, 
                        FORMAT(IFNULL(t5.p3_01,0),'Currency') AS p3_01_tot_doc, 
                        CONCAT(FORMAT(if((IFNULL(t5.p3_01,0))=0,0,((t5.p3_01)/(t0.p0_01_tot))*100.0),2),'%') as p3_01_porc_doc, 
                        FORMAT(IFNULL(t5.p3_02,0),'Currency') AS p3_02_tot_doc, 
                        CONCAT(FORMAT(if((IFNULL(t5.p3_02,0))=0,0,((t5.p3_02)/(t0.p0_01_tot))*100.0),2),'%') as p3_02_porc_obs,  
                        FORMAT(IFNULL(t6.p4_01,0),'Currency') AS p4_01_tot_doc, 
                        CONCAT(FORMAT(if((IFNULL(t6.p4_01,0))=0,0,((t6.p4_01)/(t0.p0_01_tot))*100.0),2),'%') as p4_01_porc_doc,  
                        FORMAT(IFNULL(t6.p4_02,0),'Currency') AS p4_02_tot_doc, 
                        CONCAT(FORMAT(if((IFNULL(t6.p4_02,0))=0,0,((t6.p4_02)/(t0.p0_01_tot))*100.0),2),'%') as p4_02_porc_obs 
                    FROM t_cod_sede t1 
                    LEFT JOIN (
                        SELECT m3.cod_sede_regional,m1.documento_id as doc_id,m2.tipo,m2.etiqueta,m1.operativo_marco, SUM(m1.p_01_marco) AS p0_01_tot, 0 AS p0_02_tot 
                        from t_dig_marco m1
                        left join t_cod_documento m2 on m2.id=m1.documento_id
                        left join t_cod_sede m3 on m3.id=m1.sede_id
                        WHERE if(sp_operativo=0,true,m1.operativo_marco=sp_operativo)
                        GROUP BY m3.cod_sede_regional,m2.id
                        order by m1.sede_id asc, m1.documento_id asc 
                    ) t0 on t0.cod_sede_regional=t1.cod_sede_regional
                left join (
                        select a4.cod_sede_regional, a2.operativo, a3.tipo, a1.documento_id, sum(a1.p_01) AS p1_01, sum(a1.p_02) AS p1_02 
                    from t_det_dig_documento a1
                    left join t_dig_documento a2 on a2.id=a1.cabecera_id
                    left join t_cod_documento a3 on a3.id=a1.documento_id
                        left join t_cod_sede a4 on a4.id=a2.sede_id
                    WHERE a2.etapa=1 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                    GROUP BY a4.cod_sede_regional, a3.id
                        order by a2.sede_id asc, a1.documento_id
                    ) t2 on t2.cod_sede_regional=t1.cod_sede_regional and t2.documento_id=t0.doc_id
                left join (
                        select a4.cod_sede_regional, a2.operativo, a3.tipo, a1.documento_id, SUM(a1.p_01) AS p2_01, SUM(a1.p_02) AS p2_02 
                    from t_det_dig_documento a1
                    left join t_dig_documento a2 on a2.id=a1.cabecera_id
                    left join t_cod_documento a3 on a3.id=a1.documento_id
                        left join t_cod_sede a4 on a4.id=a2.sede_id
                    WHERE a2.etapa=2 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                    GROUP BY a4.cod_sede_regional, a3.id
                        order by a2.sede_id asc, a1.documento_id
                    ) t4 on t4.cod_sede_regional=t1.cod_sede_regional and t4.documento_id=t0.doc_id
                left join (
                        select a4.cod_sede_regional, a2.operativo, a3.tipo, a1.documento_id, SUM(a1.p_01) AS p3_01, SUM(a1.p_02) AS p3_02 
                    from t_det_dig_documento a1
                    left join t_dig_documento a2 on a2.id=a1.cabecera_id
                    left join t_cod_documento a3 on a3.id=a1.documento_id
                        left join t_cod_sede a4 on a4.id=a2.sede_id
                    WHERE a2.etapa=3 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                    GROUP BY a4.cod_sede_regional, a3.id
                        order by a2.sede_id asc, a1.documento_id
                    ) t5 on t5.cod_sede_regional=t1.cod_sede_regional and t5.documento_id=t0.doc_id
                left join (
                        select a4.cod_sede_regional, a2.operativo, a3.tipo, a1.documento_id, SUM(a1.p_01) AS p4_01, SUM(a1.p_02) AS p4_02 
                    from t_det_dig_documento a1
                    left join t_dig_documento a2 on a2.id=a1.cabecera_id
                    left join t_cod_documento a3 on a3.id=a1.documento_id
                        left join t_cod_sede a4 on a4.id=a2.sede_id
                    WHERE a2.etapa=4 and if(sp_operativo=0,true,a2.operativo=sp_operativo) and if(sp_tipo_documento=0,true,a3.tipo=sp_tipo_documento)
                    GROUP BY a4.cod_sede_regional, a3.id
                        order by a2.sede_id asc, a1.documento_id
                    ) t6 on t6.cod_sede_regional=t1.cod_sede_regional and t6.documento_id=t0.doc_id
                    group by t1.cod_sede_regional,t0.doc_id
                    order by t1.id,t0.doc_id
                ) f 
                order by row_num,doc_id;
                END
            ");
        }else{

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP PROCEDURE sp_ver_avance_doc_reg_operativo_tipdoc;");
    }
}
