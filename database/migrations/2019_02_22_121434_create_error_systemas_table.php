<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErrorSystemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_error_systems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_empresa')->nullable();
            $table->integer('id_persona')->nullable();
            $table->string('tabla',50)->nullable();
            $table->char('accion',1)->nullable();
            $table->string('accion_det')->nullable();
            $table->string('descripcion',200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_error_systems');
    }
}
