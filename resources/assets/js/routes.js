import VueRouter from 'vue-router'
import helper from './services/helper'

let routes = [
    {       
        path: '/',
        component: require('./layouts/default-page'),
        meta: { requiresAuth: true },
        children: [
            {
                path: '/',
                //component: require('./views/pages/inicio')
                component: require('./views/reportes/avanceRegional')
            },          
            {
                path: '/inicio',
                //component: require('./views/pages/inicio')
                component: require('./views/reportes/avanceRegional')
            },
            {
                path: '/blank',
                component: require('./views/pages/blank')
            },
            {
                path: '/configuration',
                component: require('./views/configuration/configuration')
            },
            {
                path: '/profile',
                component: require('./views/user/profile')
            },
           
            {
                path: '/user',
                component: require('./views/user/index')
            },                   
            {
                path: '/roles',
                component: require('./views/admin/roles')
            },
            {
                path: '/menus',
                component: require('./views/admin/menus')
            },
            {
                path: '/usuarios',
                component: require('./views/admin/usuarios')
            },
            {
                path: '/usuarios_:parametros',
                component: require('./views/admin/usuarios')
            },

            //cantidades
            {
                path: '/cantidades',
                component: require('./views/registros/gestionDocumental')
            },

            //reportes
            
            {
                path: '/reporte_avance_nacional',
                component: require('./views/reportes/avanceRegional')
            },
            {
                path: '/reporte_avance',
                component: require('./views/reportes/avanceProvincial')
            },
            {
                path: '/reporte_productividad',
                component: require('./views/reportes/produccionPersonal')
            },
            {
                path: '/reporte_diario',
                component: require('./views/reportes/diario')
            },
            {
                path: '/reporte_avance_documento',
                component: require('./views/reportes/avanceDocumento')
            },
            {
                path: '/reporte_avance_ranking_persona',
                component: require('./views/reportes/avanceRankingPersona')
            },

            //mantenimiento
            {
                path: '/personas',
                component: require('./views/mantenimiento/persona')
            },

            

        ]
    },
    {
        path: '/',
        component: require('./layouts/guest-page'),
        meta: { requiresGuest: true },
        children: [
            {
                path: '/login',
                component: require('./views/auth/login')
            },
            {
                path: '/password',
                component: require('./views/auth/password')
            },
            {
                path: '/register',
                component: require('./views/auth/register')
            },
            {
                path: '/auth/:token/activate',
                component: require('./views/auth/activate')
            },
            {
                path: '/password_reset_:token',
                component: require('./views/auth/reset')
            },
            {
                path: '/auth/social',
                component: require('./views/auth/social-auth')
            },

            {
                path: '/consulta_cpe',
                component: require('./views/auth/consulta_cpe')
            },
        ]
    },
    {
        path: '*',
        component : require('./layouts/error-page'),
        children: [
            {
                path: '*',
                component: require('./views/errors/page-not-found')
            }
        ]
    }
];

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
	routes,
    linkActiveClass: 'active',
    
    
});

router.beforeEach((to, from, next) => {

    if (to.matched.some(m => m.meta.requiresAuth)){
        return helper.check().then(response => {
            if(!response){
                return next({ path : '/login'})
            }

            return next()
        })
    }

    if (to.matched.some(m => m.meta.requiresGuest)){
        return helper.check().then(response => {
            if(response){
                return next({ path : '/'})
            }

            return next()
        })
    }

    return next()
});

export default router;
