<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SpVerAvancePorDocumentoSedeFOperativoTipoDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
        
                CREATE PROCEDURE `sp_ver_avance_documentos_operativo_tdocumento_sede`(IN sp_operativo INT, IN sp_tipo_documento INT)
                NO SQL
                BEGIN 
                    
                    set @queryOperativoMarco = ('');
                    set @queryOperativo = ('');
                    set @queryTipoDocumento = ('');
                    
                    IF sp_operativo <> 0 THEN
                    BEGIN
                        set @queryOperativo = CONCAT(' and t0.operativo = ', sp_operativo );
                        set @queryOperativoMarco =  CONCAT(' WHERE M.operativo_marco = ', sp_operativo );
                    END;
                    END IF;
                    
                    IF sp_tipo_documento <> 0 THEN
                        set @queryTipoDocumento = CONCAT(' WHERE  D.tipo = ', sp_tipo_documento );
                    END IF;
                    
                    set @queryString = CONCAT('
                    
                    -- INICO GENERAL
                    -- INICIO SELECT
                    (select 
                    
                    NULL AS row_num,	
                    ''TOTAL'' AS cod_sede_regional,
                    NULL AS cod_sede_provincial,
                    NULL AS cod_sede_distrital,
                    NULL AS sede_regional,
                    NULL AS sede_provincial,
                    NULL AS sede_distrital,
                    NULL AS documento_id, 
                    NULL AS tipo, 
                    NULL AS tipo_nombre,
                    NULL AS subtipo, 
                    NULL AS orden, 
                    NULL AS etiqueta,
                    SUM(M.p0_01_tot_doc) AS p0_01_tot_doc,
                    NULL AS p0_01_tot_obs,
                    NULL AS p0_01_porc_obs,
                    SUM(E1.p1_01_tot_doc) as p1_01_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(E1.p1_01_tot_doc),0)=0,0,(SUM(E1.p1_01_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p1_01_porc_doc, 
                    SUM(E1.p1_02_tot_doc) AS p1_02_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(E1.p1_02_tot_doc),0)=0,0,(SUM(E1.p1_02_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p1_02_porc_obs, 
                    SUM(E2.p2_01_tot_doc) AS p2_01_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(E2.p2_01_tot_doc),0)=0,0,(SUM(E2.p2_01_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p2_01_porc_doc, 
                    SUM(E2.p2_02_tot_doc) AS p2_02_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(E2.p2_02_tot_doc),0)=0,0,(SUM(E2.p2_02_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p2_02_porc_obs, 
                    SUM(E3.p3_01_tot_doc) AS p3_01_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(E3.p3_01_tot_doc),0)=0,0,(SUM(E3.p3_01_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p3_01_porc_doc, 
                    SUM(E3.p3_02_tot_doc) AS p3_02_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(E3.p3_02_tot_doc),0)=0,0,(SUM(E3.p3_02_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p3_02_porc_obs, 
                    SUM(E4.p4_01_tot_doc) AS p4_01_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(E4.p4_01_tot_doc),0)=0,0,(SUM(E4.p4_01_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p4_01_porc_doc, 
                    SUM(E4.p4_02_tot_doc) AS p4_02_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(SUM(E4.p4_02_tot_doc),0)=0,0,(SUM(E4.p4_02_tot_doc)/SUM(M.p0_01_tot_doc))*100.0),2),''%'') AS p4_02_porc_obs
                    -- INICO MATCH
                    FROM ( 
                    --  marco
                    select 
                    M.sede_id as sede_id,
                    M.documento_id as documento_id,
                    SUM(M.p_01_marco) AS p0_01_tot_doc,
                    SUM(M.p_02_marco) AS p0_02_tot_doc
                    from t_dig_marco M 
                    ', @queryOperativoMarco ,' -- remplazar
                    group by M.sede_id,M.documento_id ORDER BY M.sede_id, M.documento_id   asc) M 
                    -- mostando etapa 1
                    LEFT JOIN
                    (select  
                    t0.etapa,
                    t0.sede_id as sede_id,
                    t1.documento_id as documento_id,
                    SUM(t1.p_01) AS p1_01_tot_doc,
                    SUM(t1.p_02) AS p1_02_tot_doc
                    from t_dig_documento t0 
                    LEFT JOIN t_det_dig_documento t1 on t1.cabecera_id = t0.id
                    where t0.etapa = 1 ', @queryOperativo ,' -- remplazar
                    group by t0.sede_id,t1.documento_id) E1 ON M.sede_id = E1.sede_id and M.documento_id = E1.documento_id
                    -- mostando etapa 2
                    LEFT JOIN
                    (select  
                    t0.etapa,
                    t0.sede_id as sede_id,
                    t1.documento_id as documento_id,
                    SUM(t1.p_01) AS p2_01_tot_doc,
                    SUM(t1.p_02) AS p2_02_tot_doc
                    from t_dig_documento t0 
                    LEFT JOIN t_det_dig_documento t1 on t1.cabecera_id = t0.id
                    where t0.etapa = 2 ', @queryOperativo ,' -- remplazar
                    group by t0.sede_id,t1.documento_id) E2 ON M.sede_id = E2.sede_id and M.documento_id = E2.documento_id
                    -- mostando etapa 3
                    LEFT JOIN
                    (select  
                    t0.etapa,
                    t0.sede_id as sede_id,
                    t1.documento_id as documento_id,
                    SUM(t1.p_01) AS p3_01_tot_doc,
                    SUM(t1.p_02) AS p3_02_tot_doc
                    from t_dig_documento t0 
                    LEFT JOIN t_det_dig_documento t1 on t1.cabecera_id = t0.id
                    where t0.etapa = 3 ', @queryOperativo ,' -- remplazar
                    group by t0.sede_id,t1.documento_id) E3 ON M.sede_id = E3.sede_id and M.documento_id = E3.documento_id
                    -- mostando etapa 4
                    LEFT JOIN
                    (select  
                    t0.etapa,
                    t0.sede_id as sede_id,
                    t1.documento_id as documento_id,
                    SUM(t1.p_01) AS p4_01_tot_doc,
                    SUM(t1.p_02) AS p4_02_tot_doc
                    from t_dig_documento t0 
                    LEFT JOIN t_det_dig_documento t1 on t1.cabecera_id = t0.id
                    where t0.etapa = 4 ', @queryOperativo ,' -- remplazar
                    group by t0.sede_id,t1.documento_id) E4 ON M.sede_id = E4.sede_id and M.documento_id = E4.documento_id
                    -- documento
                    LEFT JOIN
                    t_cod_documento D ON D.id = M.documento_id 
                    -- sede
                    LEFT JOIN t_cod_sede S ON  S.id = M.sede_id
                    ', @queryTipoDocumento ,')
                    
                    UNION ALL
                    
                    (select 
                    
                    row_number() over (order by S.id,D.id) AS row_num,	
                    S.cod_sede_regional,
                    S.cod_sede_provincial,
                    S.cod_sede_distrital,
                    S.sede_regional,
                    S.sede_provincial,
                    S.sede_distrital,
                    D.id as documento_id, 
                    D.tipo, 
                    CASE 
                    WHEN tipo = 1 THEN ''MINEDU'' WHEN tipo = 2 THEN ''INEI'' ELSE NULL	END AS tipo_nombre,
                    D.subtipo, 
                    D.orden, 
                    D.etiqueta,
                    M.p0_01_tot_doc,
                    NULL AS p0_01_tot_obs,
                    NULL AS p0_01_porc_obs,
                    E1.p1_01_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(E1.p1_01_tot_doc,0)=0,0,(E1.p1_01_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p1_01_porc_doc, 
                    E1.p1_02_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(E1.p1_02_tot_doc,0)=0,0,(E1.p1_02_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p1_02_porc_obs, 
                    E2.p2_01_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(E2.p2_01_tot_doc,0)=0,0,(E2.p2_01_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p2_01_porc_doc, 
                    E2.p2_02_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(E2.p2_02_tot_doc,0)=0,0,(E2.p2_02_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p2_02_porc_obs, 
                    E3.p3_01_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(E3.p3_01_tot_doc,0)=0,0,(E3.p3_01_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p3_01_porc_doc, 
                    E3.p3_02_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(E3.p3_02_tot_doc,0)=0,0,(E3.p3_02_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p3_02_porc_obs, 
                    E4.p4_01_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(E4.p4_01_tot_doc,0)=0,0,(E4.p4_01_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p4_01_porc_doc, 
                    E4.p4_02_tot_doc,
                    CONCAT(FORMAT(if(IFNULL(E4.p4_02_tot_doc,0)=0,0,(E4.p4_02_tot_doc/M.p0_01_tot_doc)*100.0),2),''%'') AS p4_02_porc_obs
                    -- INICO MATCH
                    FROM ( 
                    --  marco
                    select 
                    M.sede_id as sede_id,
                    M.documento_id as documento_id,
                    SUM(M.p_01_marco) AS p0_01_tot_doc,
                    SUM(M.p_02_marco) AS p0_02_tot_doc
                    from t_dig_marco M 
                    ', @queryOperativoMarco ,' -- remplazar
                    group by M.sede_id,M.documento_id ORDER BY M.sede_id, M.documento_id   asc) M 
                    -- mostando etapa 1
                    LEFT JOIN
                    (select  
                    t0.etapa,
                    t0.sede_id as sede_id,
                    t1.documento_id as documento_id,
                    SUM(t1.p_01) AS p1_01_tot_doc,
                    SUM(t1.p_02) AS p1_02_tot_doc
                    from t_dig_documento t0 
                    LEFT JOIN t_det_dig_documento t1 on t1.cabecera_id = t0.id
                    where t0.etapa = 1 ', @queryOperativo ,' -- remplazar
                    group by t0.sede_id,t1.documento_id) E1 ON M.sede_id = E1.sede_id and M.documento_id = E1.documento_id
                    -- mostando etapa 2
                    LEFT JOIN
                    (select  
                    t0.etapa,
                    t0.sede_id as sede_id,
                    t1.documento_id as documento_id,
                    SUM(t1.p_01) AS p2_01_tot_doc,
                    SUM(t1.p_02) AS p2_02_tot_doc
                    from t_dig_documento t0 
                    LEFT JOIN t_det_dig_documento t1 on t1.cabecera_id = t0.id
                    where t0.etapa = 2 ', @queryOperativo ,' -- remplazar
                    group by t0.sede_id,t1.documento_id) E2 ON M.sede_id = E2.sede_id and M.documento_id = E2.documento_id
                    -- mostando etapa 3
                    LEFT JOIN
                    (select  
                    t0.etapa,
                    t0.sede_id as sede_id,
                    t1.documento_id as documento_id,
                    SUM(t1.p_01) AS p3_01_tot_doc,
                    SUM(t1.p_02) AS p3_02_tot_doc
                    from t_dig_documento t0 
                    LEFT JOIN t_det_dig_documento t1 on t1.cabecera_id = t0.id
                    where t0.etapa = 3 ', @queryOperativo ,' -- remplazar
                    group by t0.sede_id,t1.documento_id) E3 ON M.sede_id = E3.sede_id and M.documento_id = E3.documento_id
                    -- mostando etapa 4
                    LEFT JOIN
                    (select  
                    t0.etapa,
                    t0.sede_id as sede_id,
                    t1.documento_id as documento_id,
                    SUM(t1.p_01) AS p4_01_tot_doc,
                    SUM(t1.p_02) AS p4_02_tot_doc
                    from t_dig_documento t0 
                    LEFT JOIN t_det_dig_documento t1 on t1.cabecera_id = t0.id
                    where t0.etapa = 4 ', @queryOperativo ,' -- remplazar
                    group by t0.sede_id,t1.documento_id) E4 ON M.sede_id = E4.sede_id and M.documento_id = E4.documento_id
                    -- documento
                    LEFT JOIN
                    t_cod_documento D ON D.id = M.documento_id 
                    -- sede
                    LEFT JOIN t_cod_sede S ON  S.id = M.sede_id
                    ', @queryTipoDocumento ,') ORDER BY row_num;
                    ');
                    
                            -- SELECT @queryString;
                    PREPARE myquery FROM @queryString;
                    EXECUTE myquery;
                    
                    END
            ");
        }else{

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP PROCEDURE sp_ver_avance_documentos_operativo_tdocumento_sede;");
    }
}
