<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Util\Service;

use App\Documentos;
use App\CantidadDocumentos;
use App\CantidadDocumentosDetalle;

class CantidadDocumentosController extends Controller
{
    public function obtener(Request $request){      
        $cab_id = 0;
        //IN sp_etapa INT, IN sp_operativo INT, IN sp_sede INT, IN sp_persona INT, IN sp_fecha DATE

       
        $cab_cant_documento = CantidadDocumentos::where('etapa',$request->etapa)
                            ->where('operativo',$request->operativo)
                            ->where('sede_id',$request->sede)
                            ->where('persona_id',$request->persona)
                            ->where('fecha_registro',$request->fecha)->first();
        
      
        if($cab_cant_documento){
            $cab_id = $cab_cant_documento->id;
        }   
        else{
            $cab_cant_documento=array();
        }
       
        $parametros = array($cab_id);
        $det_cant_documento = Service::getStoreGeneral('sp_obtener_registro_etapa_persona',$parametros);
        
        return compact('det_cant_documento','cab_id','cab_cant_documento');
    }

    public function guardar(Request $request){
        try {
            DB::beginTransaction();
            $cant_documentos =  new CantidadDocumentos($request->all());   
            $cant_documentos->save();

            $items = $request->detalle;

            for($i = 0; $i < count($items); $i++){
                $detalle = new CantidadDocumentosDetalle;
                $detalle->cabecera_id = $cant_documentos->id;
                $detalle->documento_id = $items[$i]['documento_id'];
                $detalle->p_01 = $items[$i]['p_01'];
                $detalle->p_02 = $items[$i]['p_02'];
                $detalle->p_obs = $items[$i]['p_obs'];
                $detalle->save();
            }
                  
            DB::commit();
            return response()->json(['message' => 'El Registro ha sido registrado correctamente','cant_documento_id'=>$cant_documentos->id]);
        } catch (Exception $e) {
            
            DB::rollback();
            return response()->json(['message' => 'Tuvimos inconvenientes inténtelo denuevo más tarde'],422);
        }

    }
    public function actualizar($id,Request $request){
       // return $request;
        try {
            DB::beginTransaction();
            $cant_documentos = CantidadDocumentos::find($id)->update(['observacion_etapa'=>$request->observacion_etapa]);
        
            $items = $request->detalle;

            for($i = 0; $i < count($items); $i++){
                $detalle = CantidadDocumentosDetalle::find($items[$i]['id']);               
                $detalle->p_01 = $items[$i]['p_01'];
                $detalle->p_02 = $items[$i]['p_02'];
                $detalle->p_obs = $items[$i]['p_obs'];
                $detalle->save();
            }
                  
            DB::commit();
            return response()->json(['message' => 'Los registros han sido actualizados correctamente']);
        } catch (Exception $e) {
            
            DB::rollback();
            return response()->json(['message' => 'Tuvimos inconvenientes inténtelo denuevo más tarde'],422);
        }

    }

    //store

    public function reporteAvanceNacional(Request $request){
        $parametros = array("'" . $request->operativo ."'","'" . $request->tipo_documento ."'" );
        $det_cant_documento = Service::getStoreGeneral('sp_ver_avance_sede_regional_operativo_tipdoc',$parametros);
        return response()->json(['resp_array'=>$det_cant_documento]);
    }
    
    public function reporteAvanceDocumento(Request $request){
        $parametros = array("'" . $request->operativo ."'" , "'" . $request->tipo_documento ."'"  );
        $det_cant_documento = Service::getStoreGeneral('sp_ver_avance_documentos_operativo',$parametros);       
        return response()->json(['resp_array'=>$det_cant_documento]);
    }
    
    public function reporteAvanceDocumentoSedeRegional(Request $request){
        $parametros = array("'" . $request->operativo ."'" , "'" . $request->tipo_documento ."'"  );
        $det_cant_documento = Service::getStoreGeneral('sp_ver_avance_doc_reg_operativo_tipdoc',$parametros);       
        return response()->json(['resp_array'=>$det_cant_documento]);
    }
    public function reporteAvanceDocumentoSedeProvincialDistrital(Request $request){
        $parametros = array("'" . $request->operativo ."'" , "'" . $request->tipo_documento ."'"  );
        $det_cant_documento = Service::getStoreGeneral('sp_ver_avance_documentos_operativo_tdocumento_sede',$parametros);       
        return response()->json(['resp_array'=>$det_cant_documento]);
    }

    public function reporteAvance(Request $request){
        $parametros = array(
            "'" . $request->sede_regional ."'",
            "'" . $request->sede_provincial ."'",
            "'" . $request->sede_distrital ."'",
			"'" . $request->operativo ."'",
			"'" . $request->tipo_documento ."'" 
        );
        $det_cant_documento = Service::getStoreGeneral('sp_ver_avance_sede_operativo_tipodoc',$parametros);        
        return response()->json(['resp_array'=>$det_cant_documento]);
    }

    public function reporteProductividad(Request $request){
        $parametros = array();
        $det_cant_documento = Service::getStoreGeneral('sp_ver_avance_productividad',$parametros);        
        return response()->json(['resp_array'=>$det_cant_documento]);
    }

    public function reporteDiario(Request $request){
                
         $parametros = array(
            "'" . $request->sede_regional ."'",
            "'" . $request->sede_provincial ."'",
            "'" . $request->sede_distrital ."'",           
            ''.$request->operativo.'',
            ''.$request->persona.'',
            ''.$request->etapa.'',
            "'" . $request->fecha_ini ."'",
            "'" . $request->fecha_fin ."'",
        );
        
        //$parametros = array("''","''","''",'0','0','0',"'2019-11-01'","'2019-12-31'");

        $det_cant_documento = Service::getStoreGeneral('repo_detalle_productividad_persona',$parametros);          
        return response()->json(['resp_array'=>$det_cant_documento]);
    }

    public function reporteRankingPersona(Request $request){
        $parametros = array(
            "'" . $request->operativo ."'",
            "'" . $request->tipo_documento ."'",
        );
        $det_cant_documento = Service::getStoreGeneral('ver_ranking_etapa_operativo',$parametros);        
        return response()->json(['resp_array'=>$det_cant_documento]);
    }

    

}
