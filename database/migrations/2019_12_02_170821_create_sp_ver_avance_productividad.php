<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpVerAvanceProductividad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
            CREATE PROCEDURE sp_ver_avance_productividad() NO SQL 
            BEGIN    
            SELECT NULL AS row_num, NULL AS id,NULL AS cargo_id,NULL AS cargo, 'TOTAL' AS nombres, IFNULL(SUM(p_01_1),0) AS p_01_1, IFNULL(SUM(p_02_1),0) AS p_02_1, IFNULL(SUM(p_01_2),0) AS p_01_2, IFNULL(SUM(p_02_2),0) AS p_02_2, IFNULL(SUM(p_01_3),0) AS p_01_3, IFNULL(SUM(p_02_3),0) AS p_02_3, IFNULL(SUM(p_01_4),0) AS p_01_4, IFNULL(SUM(p_02_4),0) AS p_02_4 FROM
            t_dig_persona t0  
						LEFT JOIN t_cod_cargo X ON t0.cargo_id=X.id 
            LEFT JOIN 
            (SELECT persona_id,SUM(p_01) as p_01_1,SUM(p_02) as p_02_1 FROM t_dig_documento t1 left join t_det_dig_documento t2 on t1.id=t2.cabecera_id where etapa=1 group by persona_id) A on A.persona_id=t0.id 
            LEFT JOIN 
            (SELECT persona_id,SUM(p_01) as p_01_2,SUM(p_02) as p_02_2 FROM t_dig_documento t1 left join t_det_dig_documento t2 on t1.id=t2.cabecera_id where etapa=2 group by persona_id) B ON B.persona_id=t0.id 
            LEFT JOIN 
            (SELECT persona_id,SUM(p_01) as p_01_3,SUM(p_02) as p_02_3 FROM t_dig_documento t1 left join t_det_dig_documento t2 on t1.id=t2.cabecera_id where etapa=3 group by persona_id) C ON C.persona_id=t0.id 
            LEFT JOIN 
            (SELECT persona_id,SUM(p_01) as p_01_4,SUM(p_02) as p_02_4 FROM t_dig_documento t1 left join t_det_dig_documento t2 on t1.id=t2.cabecera_id where etapa=4 group by persona_id) D ON D.persona_id=t0.id  
            UNION ALL    
            SELECT * FROM (SELECT row_number() over (order by Y.cargo_id,Y.nombres) AS row_num,
            Y.id,cargo_id,cargo,nombres, 
            IFNULL(p_01_1,0) as p_01_1,
            IFNULL(p_02_1,0) as p_02_1, 
            IFNULL(p_01_2,0) as p_01_2, 
            IFNULL(p_02_2,0) as p_02_2, 
            IFNULL(p_01_3,0) as p_01_3, 
            IFNULL(p_02_3,0) as p_02_3, 
            IFNULL(p_01_4,0) as p_01_4, 
            IFNULL(p_02_4,0) as p_02_4 
            FROM t_dig_persona Y 
            LEFT JOIN t_cod_cargo X ON Y.cargo_id=X.id  
            LEFT JOIN 
            (SELECT persona_id,SUM(p_01) as p_01_1,SUM(p_02) as p_02_1 FROM t_dig_documento t1 left join t_det_dig_documento t2 on t1.id=t2.cabecera_id where etapa=1 group by persona_id) A on A.persona_id=Y.id 
            LEFT JOIN 
            (SELECT persona_id,SUM(p_01) as p_01_2,SUM(p_02) as p_02_2 FROM t_dig_documento t1 left join t_det_dig_documento t2 on t1.id=t2.cabecera_id where etapa=2 group by persona_id) B ON B.persona_id=Y.id 
            LEFT JOIN 
            (SELECT persona_id,SUM(p_01) as p_01_3,SUM(p_02) as p_02_3 FROM t_dig_documento t1 left join t_det_dig_documento t2 on t1.id=t2.cabecera_id where etapa=3 group by persona_id) C ON C.persona_id=Y.id 
            LEFT JOIN 
            (SELECT persona_id,SUM(p_01) as p_01_4,SUM(p_02) as p_02_4 FROM t_dig_documento t1 left join t_det_dig_documento t2 on t1.id=t2.cabecera_id where etapa=4 group by persona_id) D ON D.persona_id=Y.id 
            WHERE Y.estado=1 ORDER BY Y.cargo_id,Y.nombres) A; 
            END;
            ");
        } else {
            DB::unprepared("
            ");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("drop procedure sp_ver_avance_productividad");
    }
}
