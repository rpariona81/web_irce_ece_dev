<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpVerAvanceSedeXOperativo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('database.default') == 'mysql') {
            DB::unprepared("
            CREATE PROCEDURE sp_ver_avance_sede_operativo(IN sp_region CHAR(3), IN sp_provincia CHAR(2), IN sp_distrito CHAR(2), IN sp_operativo INT) NO SQL 
			BEGIN
			IF (sp_operativo = '') THEN 
			BEGIN
						IF (sp_region = '' AND sp_provincia = '' AND sp_distrito = '') THEN
							BEGIN 
							SELECT NULL AS row_num,NULL AS sede_id,NULL AS cod_sede_regional,'TOTAL' AS sede_regional,NULL AS cod_sede_provincial,NULL AS sede_provincial,NULL AS cod_sede_distrital,NULL AS sede_distrital,NULL AS operativo,NULL AS operativo_nombre,
							SUM(p0_01_tot) as p0_01_tot, 
							0 as p0_02_tot,
							'0.00%' AS p0_02_porc, 
							SUM(p1_01) AS p1_01, 
							CONCAT(FORMAT(if(SUM(p1_01)=0,0,(SUM(p1_01)/SUM(p0_01_tot))*100.0),2),'%') as p1_01_avance, 
							SUM(p1_02) AS p1_02, 
							CONCAT(FORMAT(if(SUM(p1_02)=0,0,(SUM(p1_02)/SUM(p0_01_tot))*100.0),2),'%') as p1_02_avance, 
							SUM(p2_01) AS p2_01, 
							CONCAT(FORMAT(if(SUM(p2_01)=0,0,(SUM(p2_01)/SUM(p0_01_tot))*100.0),2),'%') as p2_01_avance, 
							SUM(p2_02) AS p2_02, 
							CONCAT(FORMAT(if(SUM(p2_02)=0,0,(SUM(p2_02)/SUM(p0_01_tot))*100.0),2),'%') as p2_02_avance,
							SUM(p3_01) AS p3_01, 
							CONCAT(FORMAT(if(SUM(p3_01)=0,0,(SUM(p3_01)/SUM(p0_01_tot))*100.0),2),'%') as p3_01_avance, 
							SUM(p3_02) AS p3_02, 
							CONCAT(FORMAT(if(SUM(p3_02)=0,0,(SUM(p3_02)/SUM(p0_01_tot))*100.0),2),'%') as p3_02_avance,
							SUM(p4_01) AS p4_01, 
							CONCAT(FORMAT(if(SUM(p4_01)=0,0,(SUM(p4_01)/SUM(p0_01_tot))*100.0),2),'%') as p4_01_avance, 
							SUM(p4_02) AS p4_02, 
							CONCAT(FORMAT(if(SUM(p4_02)=0,0,(SUM(p4_02)/SUM(p0_01_tot))*100.0),2),'%') as p4_02_avance 
							from resum_sede_operativo_etapa_1234 
							union all
							SELECT row_number() over (order by A.sede_id) AS row_num, 
							sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							FROM 
							(select sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							from resum_sede_operativo_etapa_1234) A;
							END;
						ELSEIF (sp_region IS NOT NULL AND sp_provincia = '' AND sp_distrito = '') THEN
							BEGIN 
							SELECT NULL AS row_num,NULL AS sede_id,NULL AS cod_sede_regional,'TOTAL' AS sede_regional,NULL AS cod_sede_provincial,NULL AS sede_provincial,NULL AS cod_sede_distrital,NULL AS sede_distrital,NULL AS operativo,NULL AS operativo_nombre,
							SUM(p0_01_tot) as p0_01_tot, 
							0 as p0_02_tot,
							'0.00%' AS p0_02_porc, 
							SUM(p1_01) AS p1_01, 
							CONCAT(FORMAT(if(SUM(p1_01)=0,0,(SUM(p1_01)/SUM(p0_01_tot))*100.0),2),'%') as p1_01_avance, 
							SUM(p1_02) AS p1_02, 
							CONCAT(FORMAT(if(SUM(p1_02)=0,0,(SUM(p1_02)/SUM(p0_01_tot))*100.0),2),'%') as p1_02_avance, 
							SUM(p2_01) AS p2_01, 
							CONCAT(FORMAT(if(SUM(p2_01)=0,0,(SUM(p2_01)/SUM(p0_01_tot))*100.0),2),'%') as p2_01_avance, 
							SUM(p2_02) AS p2_02, 
							CONCAT(FORMAT(if(SUM(p2_02)=0,0,(SUM(p2_02)/SUM(p0_01_tot))*100.0),2),'%') as p2_02_avance,
							SUM(p3_01) AS p3_01, 
							CONCAT(FORMAT(if(SUM(p3_01)=0,0,(SUM(p3_01)/SUM(p0_01_tot))*100.0),2),'%') as p3_01_avance, 
							SUM(p3_02) AS p3_02, 
							CONCAT(FORMAT(if(SUM(p3_02)=0,0,(SUM(p3_02)/SUM(p0_01_tot))*100.0),2),'%') as p3_02_avance,
							SUM(p4_01) AS p4_01, 
							CONCAT(FORMAT(if(SUM(p4_01)=0,0,(SUM(p4_01)/SUM(p0_01_tot))*100.0),2),'%') as p4_01_avance, 
							SUM(p4_02) AS p4_02, 
							CONCAT(FORMAT(if(SUM(p4_02)=0,0,(SUM(p4_02)/SUM(p0_01_tot))*100.0),2),'%') as p4_02_avance 
							from resum_sede_operativo_etapa_1234  WHERE cod_sede_regional = sp_region
							union all
							SELECT row_number() over (order by A.sede_id) AS row_num, 
							sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							FROM 
							(select sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							from resum_sede_operativo_etapa_1234 WHERE cod_sede_regional = sp_region) A;
							END;
						ELSEIF (sp_region IS NOT NULL AND sp_provincia IS NOT NULL AND sp_distrito = '') THEN
							BEGIN 
							SELECT NULL AS row_num,NULL AS sede_id,NULL AS cod_sede_regional,'TOTAL' AS sede_regional,NULL AS cod_sede_provincial,NULL AS sede_provincial,NULL AS cod_sede_distrital,NULL AS sede_distrital,NULL AS operativo,NULL AS operativo_nombre,
							SUM(p0_01_tot) as p0_01_tot, 
							0 as p0_02_tot,
							'0.00%' AS p0_02_porc, 
							SUM(p1_01) AS p1_01, 
							CONCAT(FORMAT(if(SUM(p1_01)=0,0,(SUM(p1_01)/SUM(p0_01_tot))*100.0),2),'%') as p1_01_avance, 
							SUM(p1_02) AS p1_02, 
							CONCAT(FORMAT(if(SUM(p1_02)=0,0,(SUM(p1_02)/SUM(p0_01_tot))*100.0),2),'%') as p1_02_avance, 
							SUM(p2_01) AS p2_01, 
							CONCAT(FORMAT(if(SUM(p2_01)=0,0,(SUM(p2_01)/SUM(p0_01_tot))*100.0),2),'%') as p2_01_avance, 
							SUM(p2_02) AS p2_02, 
							CONCAT(FORMAT(if(SUM(p2_02)=0,0,(SUM(p2_02)/SUM(p0_01_tot))*100.0),2),'%') as p2_02_avance,
							SUM(p3_01) AS p3_01, 
							CONCAT(FORMAT(if(SUM(p3_01)=0,0,(SUM(p3_01)/SUM(p0_01_tot))*100.0),2),'%') as p3_01_avance, 
							SUM(p3_02) AS p3_02, 
							CONCAT(FORMAT(if(SUM(p3_02)=0,0,(SUM(p3_02)/SUM(p0_01_tot))*100.0),2),'%') as p3_02_avance,
							SUM(p4_01) AS p4_01, 
							CONCAT(FORMAT(if(SUM(p4_01)=0,0,(SUM(p4_01)/SUM(p0_01_tot))*100.0),2),'%') as p4_01_avance, 
							SUM(p4_02) AS p4_02, 
							CONCAT(FORMAT(if(SUM(p4_02)=0,0,(SUM(p4_02)/SUM(p0_01_tot))*100.0),2),'%') as p4_02_avance 
							from resum_sede_operativo_etapa_1234 WHERE cod_sede_regional = sp_region and cod_sede_provincial = sp_provincia 
							union all
							SELECT row_number() over (order by A.sede_id) AS row_num, 
							sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							FROM 
							(select sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							from resum_sede_operativo_etapa_1234 WHERE cod_sede_regional = sp_region and cod_sede_provincial = sp_provincia) A;
							END;
						ELSEIF (sp_region IS NOT NULL AND sp_provincia IS NOT NULL AND sp_distrito IS NOT NULL) THEN
							BEGIN 
							SELECT NULL AS row_num,NULL AS sede_id,NULL AS cod_sede_regional,'TOTAL' AS sede_regional,NULL AS cod_sede_provincial,NULL AS sede_provincial,NULL AS cod_sede_distrital,NULL AS sede_distrital,NULL AS operativo,NULL AS operativo_nombre,
							SUM(p0_01_tot) as p0_01_tot, 
							0 as p0_02_tot,
							'0.00%' AS p0_02_porc, 
							SUM(p1_01) AS p1_01, 
							CONCAT(FORMAT(if(SUM(p1_01)=0,0,(SUM(p1_01)/SUM(p0_01_tot))*100.0),2),'%') as p1_01_avance, 
							SUM(p1_02) AS p1_02, 
							CONCAT(FORMAT(if(SUM(p1_02)=0,0,(SUM(p1_02)/SUM(p0_01_tot))*100.0),2),'%') as p1_02_avance, 
							SUM(p2_01) AS p2_01, 
							CONCAT(FORMAT(if(SUM(p2_01)=0,0,(SUM(p2_01)/SUM(p0_01_tot))*100.0),2),'%') as p2_01_avance, 
							SUM(p2_02) AS p2_02, 
							CONCAT(FORMAT(if(SUM(p2_02)=0,0,(SUM(p2_02)/SUM(p0_01_tot))*100.0),2),'%') as p2_02_avance,
							SUM(p3_01) AS p3_01, 
							CONCAT(FORMAT(if(SUM(p3_01)=0,0,(SUM(p3_01)/SUM(p0_01_tot))*100.0),2),'%') as p3_01_avance, 
							SUM(p3_02) AS p3_02, 
							CONCAT(FORMAT(if(SUM(p3_02)=0,0,(SUM(p3_02)/SUM(p0_01_tot))*100.0),2),'%') as p3_02_avance,
							SUM(p4_01) AS p4_01, 
							CONCAT(FORMAT(if(SUM(p4_01)=0,0,(SUM(p4_01)/SUM(p0_01_tot))*100.0),2),'%') as p4_01_avance, 
							SUM(p4_02) AS p4_02, 
							CONCAT(FORMAT(if(SUM(p4_02)=0,0,(SUM(p4_02)/SUM(p0_01_tot))*100.0),2),'%') as p4_02_avance 
							from resum_sede_operativo_etapa_1234 WHERE cod_sede_regional = sp_region and cod_sede_provincial = sp_provincia and cod_sede_distrital = sp_distrito
							union all
							SELECT row_number() over (order by A.sede_id) AS row_num, 
							sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							FROM 
							(select sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							from resum_sede_operativo_etapa_1234 WHERE cod_sede_regional = sp_region and cod_sede_provincial = sp_provincia and cod_sede_distrital = sp_distrito) A;
							END;
						END IF;
					END;
			ELSEIF (sp_operativo IS NOT NULL) THEN 
						IF (sp_region = '' AND sp_provincia = '' AND sp_distrito = '') THEN
							BEGIN 
							SELECT NULL AS row_num,NULL AS sede_id,NULL AS cod_sede_regional,'TOTAL' AS sede_regional,NULL AS cod_sede_provincial,NULL AS sede_provincial,NULL AS cod_sede_distrital,NULL AS sede_distrital,NULL AS operativo,NULL AS operativo_nombre,
							SUM(p0_01_tot) as p0_01_tot, 
							0 as p0_02_tot,
							'0.00%' AS p0_02_porc, 
							SUM(p1_01) AS p1_01, 
							CONCAT(FORMAT(if(SUM(p1_01)=0,0,(SUM(p1_01)/SUM(p0_01_tot))*100.0),2),'%') as p1_01_avance, 
							SUM(p1_02) AS p1_02, 
							CONCAT(FORMAT(if(SUM(p1_02)=0,0,(SUM(p1_02)/SUM(p0_01_tot))*100.0),2),'%') as p1_02_avance, 
							SUM(p2_01) AS p2_01, 
							CONCAT(FORMAT(if(SUM(p2_01)=0,0,(SUM(p2_01)/SUM(p0_01_tot))*100.0),2),'%') as p2_01_avance, 
							SUM(p2_02) AS p2_02, 
							CONCAT(FORMAT(if(SUM(p2_02)=0,0,(SUM(p2_02)/SUM(p0_01_tot))*100.0),2),'%') as p2_02_avance,
							SUM(p3_01) AS p3_01, 
							CONCAT(FORMAT(if(SUM(p3_01)=0,0,(SUM(p3_01)/SUM(p0_01_tot))*100.0),2),'%') as p3_01_avance, 
							SUM(p3_02) AS p3_02, 
							CONCAT(FORMAT(if(SUM(p3_02)=0,0,(SUM(p3_02)/SUM(p0_01_tot))*100.0),2),'%') as p3_02_avance,
							SUM(p4_01) AS p4_01, 
							CONCAT(FORMAT(if(SUM(p4_01)=0,0,(SUM(p4_01)/SUM(p0_01_tot))*100.0),2),'%') as p4_01_avance, 
							SUM(p4_02) AS p4_02, 
							CONCAT(FORMAT(if(SUM(p4_02)=0,0,(SUM(p4_02)/SUM(p0_01_tot))*100.0),2),'%') as p4_02_avance 
							from resum_sede_operativo_etapa_1234 WHERE operativo = sp_operativo
							union all
							SELECT row_number() over (order by A.sede_id) AS row_num, 
							sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							FROM 
							(select sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							from resum_sede_operativo_etapa_1234 WHERE operativo = sp_operativo) A;
							END;
						ELSEIF (sp_region IS NOT NULL AND sp_provincia = '' AND sp_distrito = '') THEN
							BEGIN 
							SELECT NULL AS row_num,NULL AS sede_id,NULL AS cod_sede_regional,'TOTAL' AS sede_regional,NULL AS cod_sede_provincial,NULL AS sede_provincial,NULL AS cod_sede_distrital,NULL AS sede_distrital,NULL AS operativo,NULL AS operativo_nombre,
							SUM(p0_01_tot) as p0_01_tot, 
							0 as p0_02_tot,
							'0.00%' AS p0_02_porc, 
							SUM(p1_01) AS p1_01, 
							CONCAT(FORMAT(if(SUM(p1_01)=0,0,(SUM(p1_01)/SUM(p0_01_tot))*100.0),2),'%') as p1_01_avance, 
							SUM(p1_02) AS p1_02, 
							CONCAT(FORMAT(if(SUM(p1_02)=0,0,(SUM(p1_02)/SUM(p0_01_tot))*100.0),2),'%') as p1_02_avance, 
							SUM(p2_01) AS p2_01, 
							CONCAT(FORMAT(if(SUM(p2_01)=0,0,(SUM(p2_01)/SUM(p0_01_tot))*100.0),2),'%') as p2_01_avance, 
							SUM(p2_02) AS p2_02, 
							CONCAT(FORMAT(if(SUM(p2_02)=0,0,(SUM(p2_02)/SUM(p0_01_tot))*100.0),2),'%') as p2_02_avance,
							SUM(p3_01) AS p3_01, 
							CONCAT(FORMAT(if(SUM(p3_01)=0,0,(SUM(p3_01)/SUM(p0_01_tot))*100.0),2),'%') as p3_01_avance, 
							SUM(p3_02) AS p3_02, 
							CONCAT(FORMAT(if(SUM(p3_02)=0,0,(SUM(p3_02)/SUM(p0_01_tot))*100.0),2),'%') as p3_02_avance,
							SUM(p4_01) AS p4_01, 
							CONCAT(FORMAT(if(SUM(p4_01)=0,0,(SUM(p4_01)/SUM(p0_01_tot))*100.0),2),'%') as p4_01_avance, 
							SUM(p4_02) AS p4_02, 
							CONCAT(FORMAT(if(SUM(p4_02)=0,0,(SUM(p4_02)/SUM(p0_01_tot))*100.0),2),'%') as p4_02_avance 
							from resum_sede_operativo_etapa_1234 WHERE operativo = sp_operativo and cod_sede_regional = sp_region 
							union all
							SELECT row_number() over (order by A.sede_id) AS row_num, 
							sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							FROM 
							(select sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							from resum_sede_operativo_etapa_1234 WHERE operativo = sp_operativo and cod_sede_regional = sp_region) A;
							END;
						ELSEIF (sp_region IS NOT NULL AND sp_provincia IS NOT NULL AND sp_distrito = '') THEN
							BEGIN 
							SELECT NULL AS row_num,NULL AS sede_id,NULL AS cod_sede_regional,'TOTAL' AS sede_regional,NULL AS cod_sede_provincial,NULL AS sede_provincial,NULL AS cod_sede_distrital,NULL AS sede_distrital,NULL AS operativo,NULL AS operativo_nombre,
							SUM(p0_01_tot) as p0_01_tot, 
							0 as p0_02_tot,
							'0.00%' AS p0_02_porc, 
							SUM(p1_01) AS p1_01, 
							CONCAT(FORMAT(if(SUM(p1_01)=0,0,(SUM(p1_01)/SUM(p0_01_tot))*100.0),2),'%') as p1_01_avance, 
							SUM(p1_02) AS p1_02, 
							CONCAT(FORMAT(if(SUM(p1_02)=0,0,(SUM(p1_02)/SUM(p0_01_tot))*100.0),2),'%') as p1_02_avance, 
							SUM(p2_01) AS p2_01, 
							CONCAT(FORMAT(if(SUM(p2_01)=0,0,(SUM(p2_01)/SUM(p0_01_tot))*100.0),2),'%') as p2_01_avance, 
							SUM(p2_02) AS p2_02, 
							CONCAT(FORMAT(if(SUM(p2_02)=0,0,(SUM(p2_02)/SUM(p0_01_tot))*100.0),2),'%') as p2_02_avance,
							SUM(p3_01) AS p3_01, 
							CONCAT(FORMAT(if(SUM(p3_01)=0,0,(SUM(p3_01)/SUM(p0_01_tot))*100.0),2),'%') as p3_01_avance, 
							SUM(p3_02) AS p3_02, 
							CONCAT(FORMAT(if(SUM(p3_02)=0,0,(SUM(p3_02)/SUM(p0_01_tot))*100.0),2),'%') as p3_02_avance,
							SUM(p4_01) AS p4_01, 
							CONCAT(FORMAT(if(SUM(p4_01)=0,0,(SUM(p4_01)/SUM(p0_01_tot))*100.0),2),'%') as p4_01_avance, 
							SUM(p4_02) AS p4_02, 
							CONCAT(FORMAT(if(SUM(p4_02)=0,0,(SUM(p4_02)/SUM(p0_01_tot))*100.0),2),'%') as p4_02_avance 
							from resum_sede_operativo_etapa_1234 WHERE operativo = sp_operativo and cod_sede_regional = sp_region and cod_sede_provincial = sp_provincia 
							union all
							SELECT row_number() over (order by A.sede_id) AS row_num, 
							sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							FROM 
							(select sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							from resum_sede_operativo_etapa_1234 WHERE operativo = sp_operativo and cod_sede_regional = sp_region and cod_sede_provincial = sp_provincia) A;
							END;
						ELSEIF (sp_region IS NOT NULL AND sp_provincia IS NOT NULL AND sp_distrito IS NOT NULL) THEN
							BEGIN 
							SELECT NULL AS row_num,NULL AS sede_id,NULL AS cod_sede_regional,'TOTAL' AS sede_regional,NULL AS cod_sede_provincial,NULL AS sede_provincial,NULL AS cod_sede_distrital,NULL AS sede_distrital,NULL AS operativo,NULL AS operativo_nombre,
							SUM(p0_01_tot) as p0_01_tot, 
							0 as p0_02_tot,
							'0.00%' AS p0_02_porc, 
							SUM(p1_01) AS p1_01, 
							CONCAT(FORMAT(if(SUM(p1_01)=0,0,(SUM(p1_01)/SUM(p0_01_tot))*100.0),2),'%') as p1_01_avance, 
							SUM(p1_02) AS p1_02, 
							CONCAT(FORMAT(if(SUM(p1_02)=0,0,(SUM(p1_02)/SUM(p0_01_tot))*100.0),2),'%') as p1_02_avance, 
							SUM(p2_01) AS p2_01, 
							CONCAT(FORMAT(if(SUM(p2_01)=0,0,(SUM(p2_01)/SUM(p0_01_tot))*100.0),2),'%') as p2_01_avance, 
							SUM(p2_02) AS p2_02, 
							CONCAT(FORMAT(if(SUM(p2_02)=0,0,(SUM(p2_02)/SUM(p0_01_tot))*100.0),2),'%') as p2_02_avance,
							SUM(p3_01) AS p3_01, 
							CONCAT(FORMAT(if(SUM(p3_01)=0,0,(SUM(p3_01)/SUM(p0_01_tot))*100.0),2),'%') as p3_01_avance, 
							SUM(p3_02) AS p3_02, 
							CONCAT(FORMAT(if(SUM(p3_02)=0,0,(SUM(p3_02)/SUM(p0_01_tot))*100.0),2),'%') as p3_02_avance,
							SUM(p4_01) AS p4_01, 
							CONCAT(FORMAT(if(SUM(p4_01)=0,0,(SUM(p4_01)/SUM(p0_01_tot))*100.0),2),'%') as p4_01_avance, 
							SUM(p4_02) AS p4_02, 
							CONCAT(FORMAT(if(SUM(p4_02)=0,0,(SUM(p4_02)/SUM(p0_01_tot))*100.0),2),'%') as p4_02_avance  
							from resum_sede_operativo_etapa_1234 WHERE operativo = sp_operativo and cod_sede_regional = sp_region and cod_sede_provincial = sp_provincia and cod_sede_distrital = sp_distrito 
							union all
							SELECT row_number() over (order by A.sede_id) AS row_num, 
							sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							FROM 
							(select sede_id,cod_sede_regional,sede_regional,cod_sede_provincial,sede_provincial,cod_sede_distrital,sede_distrital,operativo,operativo_nombre,
							p0_01_tot, p0_02_tot, p0_02_porc,
							p1_01, p1_01_avance, p1_02, p1_02_avance,
							p2_01, p2_01_avance, p2_02, p2_02_avance,
							p3_01, p3_01_avance, p3_02, p3_02_avance,
							p4_01, p4_01_avance, p4_02, p4_02_avance 
							from resum_sede_operativo_etapa_1234 WHERE operativo = sp_operativo and cod_sede_regional = sp_region and cod_sede_provincial = sp_provincia and cod_sede_distrital = sp_distrito) A;
							END;
						END IF;
						END IF;
						END
            ");
        } else {
            DB::unprepared("
            ");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("drop procedure sp_ver_avance_sede_operativo");
    }
}
