<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTDetDigDocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_det_dig_documento', function (Blueprint $table) {
            $table->bigincrements('id');
            $table->biginteger('cabecera_id')->unsigned();
            $table->integer('documento_id')->unsigned();
            $table->integer('p_01')->nullable();
            $table->integer('p_02')->nullable();
            $table->string('p_obs',1000)->nullable();
            $table->timestamps();
            $table->foreign('cabecera_id')->references('id')->on('t_dig_documento')->onDelete('cascade');
            $table->foreign('documento_id')->references('id')->on('t_cod_documento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_det_dig_documento');
    }
}
