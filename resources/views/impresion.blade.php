<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Document</title>
</head>
<body>

<script src="{{asset('js/extras/pdf.min.js')}} "></script>
<div id="area_impresion">


<canvas id="the-canvas"></canvas>
</div>



<input type="hidden" id="asset_work" value="{{asset('js/extras/pdf.worker.js')}}">
<input type="hidden" id="url_documento" value="{{$codigo}}">
<input type="hidden" id="base_url" value="{{$base}}">



<script>
// If absolute URL from the remote server is provided, configure the CORS
// header on that server.
var url = document.getElementById('base_url').value + '/downloads/document/pdf/' + document.getElementById('url_documento').value;

// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = document.getElementById('asset_work').value;

// Asynchronous download of PDF
var loadingTask = pdfjsLib.getDocument(url);
loadingTask.promise.then(function(pdf) {
  console.log('PDF loaded');
  
  // Fetch the first page
  var pageNumber = 1;
  pdf.getPage(pageNumber).then(function(page) {
    console.log('Page loaded');
    
    var scale = 1.5;
    var viewport = page.getViewport({scale: scale});

    // Prepare canvas using PDF page dimensions
    var canvas = document.getElementById('the-canvas');
    var context = canvas.getContext('2d');
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: context,
      viewport: viewport
    };
    var renderTask = page.render(renderContext);
    renderTask.promise.then(function () {
      console.log('Page rendered');
      window.print();   
    });
  });
}, function (reason) {
  // PDF loading error
  console.error(reason);
});

</script>


</body>
</html>